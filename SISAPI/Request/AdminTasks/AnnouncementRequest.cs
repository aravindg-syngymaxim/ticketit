﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Request.AdminTasks
{
    public class AnnouncementRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AnnouncementType { get; set; }
        public string TermId { get; set; }
        public string CourseId { get; set; }
        public string SubjectId { get; set; }
        public string ProgramId { get; set; }
        public string Year { get; set; }
        public string Semister { get; set; }
        public string RoleId { get; set; }
        public string PublishDate { get; set; }
        public string ExpiryDate { get; set; }
    }
}
