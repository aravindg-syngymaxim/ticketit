﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Request.AdminTasks
{
    public class AcademicCalenderRequest
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPublicHoliday { get; set; }
        public string LanguageId { get; set; }
        public string EventDate { get; set; }
        public string ReminderUnit { get; set; }
        public string ReminderUnitLength { get; set; }
        public string EndDate { get; set; }
        public string StartDate { get; set; }
        public string EventType { get; set; }
    }
}
