﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Request.AdminTasks
{
    public class FormsApplicationsRequest
    {
        public string Id { get; set; }
        public string DegreeTypeId { get; set; }
        public string FormTitle { get; set; }
        public string CategoryId { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string LastModifiedById { get; set; }
        public string DownloadURL { get; set; }
    }
}
