﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt
{
    public class SISCommonKeys
    {
        #region Constant Variables 

        public const string FROM_HOME = "FROM_HOME";

        public const string STUDENT_ROLE_ID = "ROL0001";
        public const string REGISTRAR_ROLE_ID = "ROL0002";
        public const string FACULTY_ROLE_ID = "ROL0003";
        public const string ADVISOR_ROLE_ID = "ROL0010";
        public const string HOD_ROLE_ID = "ROL0005";
        public const string DEAN_ROLE_ID = "ROL0006";
        public const string EXAM_CENTER_ROLE_ID = "ROL0007";
        public const string HR_ROLE_ID = "ROL0008";
        public const string FINANCE_ROLE_ID = "ROL0009";
        public const string MIS_ROLE_ID = "ROL0004";
        public const string PARENT_ROLE_ID = "ROL0011";
        public const string RAW_STUDENT_ROLE_ID = "ROL0012";

        public const string ADMIN_ROLE_ID = "ROLikfVWrbC";
        public const string ADMIN2_ROLE_ID = "ROLBlKCm2Xf";

        public const string FIRST_ADMIN_USER_TYPE = "FirstAdmin";
        public const string ORG_ROOT_LEVEL_ID = "L00000";
        public const string ORG_ROOT_LEVEL_UNIT_ID = "U00000";
        public const string TELEPHONE_NO_CUSTOM_FIELD_ID = "CF0001";
        public const string ADMIN_SITE_DEFAULT_LANGUAGE = "en-US";
        public const string CONFIG_BASE_CLIENT_ID = "BaseClientId";
        public static int[] PageSize = { 5, 10, 15, 20, 25, 35, 50, 75, 100 };
        public static int GRID_DEFAULT_PAGE_SIZE = 5;
        public const string RANGE_TOTAL_ROWS = "TotalRows";
        public const string RANGE_TOTAL_COUNT = "TotalCount";
        public const int TEXT_MAX_LENGTH = 250;
        public const int TEXT_DESCRIPTION_MAX_LENGTH = 250;
        public const string CSS_BUTTON_ENABLED = "sgbutton";
        public const string CSS_BUTTON_DISABLED = "sgbuttondisabled";
        public const string DETAILED_CERTIFICATION_RESULTS_BY_USER_FEATURE_ID = "FEA0310";
        public const string INCIDENCES_OF_NON_PREFERRED_ANSWERS_FOR_THE_CERTIFICATION__FEATURE_ID = "FEA0311";
        public const string LU_WEBSERVICE_TOKEN = "LUWebServiceToken";
        public const string GRID_DATE_FORMAT = "{0:dd-MMM-yyyy}";
        public const string SESSION_APPLICATION_IDS_TO_REGISTER = "sessionApplicationIDsToApprove";
        public const string RESULT = "Result";

        public const string COMMONPHOTO = "myphoto.jpg";
        #endregion

        #region Constantant Value

        public const string ENGLISH_CULTURE = "en-US";
        public const string ARABIC_CULTURE = "ar-SA";
        public const string SAUDI = "saudi";
        public const string NON_SAUDI = "nonsaudi";
        public const string SELECTED_TRUE = "true";
        public const string SELECTED_FALSE = "false";
        public const string LOOKUPVALUE_DEGREE_TYPE_BACHELOR = "Bachelor";
        public const string LOOKUPVALUE_DEGREE_TYPE_DIPLOMA = "Diploma";
        public const string SAUDI_COUNTRY_CODE = "CON0001";
        public const string SAUDI_MOBILE_COUNTRY_CODE = "966";
        public const string STUD_PHYSICALDISABILITY_OTHER = "Other";

        public const string EDUCATION_TEST_DETAILS_TOEFL_COMPUTER = "TOEFLComputer";
        public const string EDUCATION_TEST_DETAILS_TOEFL_PAPER = "TOEFLPaper";
        public const string EDUCATION_TEST_DETAILS_ABILITY = "Ability";
        public const string EDUCATION_TEST_DETAILS_MEASUREMENT = "Measurement";
        public const string COURES_PREREQUISITES = "Pre";
        public const string COURES_COREQUISITES = "Co";
        public const string SESSION_GRID_PAGE_SIZE = "GridPageSize";
        public const string COURES_CHECKEDSTATUS = "Approved";
        public const string COURES_UNCHECKEDSTATUS = "Pending";
        public const string MY_DEGREECOMPLIANCE_BACK_URL = "MyDegreeComplianceBackPageUrl";
        public const string TRANSCRIPT_BACK_URL = "CRTranscriptBackUrl";

        //pawan to check for type
        public const string PROGRAM_TYPE = "ProgramMaster";
        public const string CRN_MAPPED = "Mapped";

        public const string APPLY_TRANSFER = "Transfer";
        public const string APPLY_FRESHMAN = "Freshman";

        public const string DECISION_MODE_CUMULATIVE_SCORE = "CumulativeScore";
        public const string DECISION_MODE_AVERAGE_SCORE = "AverageScore";
        public const string DECISION_MODE_MINIMUM_SCORE = "MinimumScore";
        #endregion

        #region DegreeTypeConstant

        public const string DEGREETYPE_BACHELOR = "DT00001";
        public const string DEGREETYPE_DIPLOMA = "DT00002";
        public const string DEGREETYPE_PHARMAD = "DT00003";
        public const string DEGREETYPE_FOUNDATION = "DT00004";

        #endregion

        #region ApplicationCjeckListItems
        public const string NAME = "ACM0001";
        public const string ADDRESS_AND_PHONE = "ACM0002";
        public const string PERSONAL_INFORMATION = "ACM0005";
        public const string PARENTAL_INFORMATION = "ACM0006";
        public const string INTERNATIONAL_INFORMATION = "ACM0007";
        public const string HIGH_SCHOOL = "ACM0008";
        public const string TEST_SCORE = "ACM0009";
        public const string MAJOR_AREA_OF_INTEREST = "ACM0010";
        public const string ACTIVITY_AND_HOBBIES = "ACM0011";
        public const string ADDITIONAL_INFORMATION = "ACM0012";

        public const int ORDER_NAME = 1;
        public const int ORDER_ADDRESS_AND_PHONE = 2;
        public const int ORDER_PERSONAL_INFORMATION = 3;
        public const int ORDER_PARENTAL_INFORMATION = 4;
        public const int ORDER_INTERNATIONAL_INFORMATION = 5;
        public const int ORDER_HIGH_SCHOOL = 6;
        public const int ORDER_TEST_SCORE = 7;
        public const int ORDER_MAJOR_AREA_OF_INTEREST = 8;
        public const int ORDER_ACTIVITY_AND_HOBBIES = 9;
        public const int ORDER_ADDITIONAL_INFORMATION = 10;

        #endregion

        #region Physical Disability

        public const string PHYSICAL_DISABILITY_OTHER = "Other";

        #endregion

        #region ViewState Value

        public const string VIEWSTATE_VALIDATEDATE = "ValidateDate";
        public const string VIEWSTATE_CURRENTTABLE = "CurrentTable";
        public const string VIEWSTATE_SPONSORTABLE = "SponsorCurrentTable";
        public const string VIEWSTATE_CLASSMEETING_DETAILS = "CSMeetingDetails";
        public const string VIEWSTATE_ADDITIONAL_INFO = "AdditionalInfo";
        public const string VIEWSTATE_UPLOAD_PHOTO_CLICK = "UploadPhoto";
        public const string VIEWSTATE_SECONDTABLE = "SecondTable";
        public const string VIEWSTATE_UPLOAD_PROGRAM_FILES = "UploadProgFile";
        public const string VIEWSTATE_MAPPED_CLASSROOMS = "MAPPEDCLASS";
        public const string VIEWSTATE_UPLOAD_ASSIGNMENT_CLICK = "UploadAssignment";
        public const string VIEWSTATE_ALL_ASSIGNMENT = "AllAssignment";
        public const string VIEWSTATE_ADMISSION_DECISION = "AdmissionDecision";
        public const string VIEWSTATE_CURRENTTABLE_SCHOOL = "SchoolOrCollegeTable";
        public const string VIEWSTATE_CURRENTTABLE_GRADE = "GradeTable";
        public const string VIEWSTATE_CURRENTTABLE_TEST = "TestTable";
        public const string VIEWSTATE_CURRENTTABLE_COURSE = "CourseTable";
        #endregion

        #region Lookup Constant Value

        public const string LOOKUP_CONTRACT_TYPE = "ContractType";
        public const string LOOKUP_TYPE_DAY = "Days";
        public const string LOOKUP_TYPE_MONTH = "Months";
        public const string LOOKUP_TYPE_YEAR = "Years";
        public const string LOOKUP_DEGREE_TYPE = "DegreeType";
        public const string LOOKUP_PREFIX = "Prefix";
        public const string LOOKUP_APPLICATION_TYPE = "ApplicationType";
        public const string LOOKUP_SEMESTERAPPLIED = "SemesterApplied";
        public const string LOOKUP_IDENTIFICATION = "Identification";
        public const string LOOKUP_RELIGION = "Religion";
        public const string LOOKUP_GENDER = "Gender";
        public const string LOOKUP_REGISTRATIONTYPE = "RegistrationType";
        public const string LOOKUP_REGISTRATION_STATUS = "RegistrationStatus";
        public const string LOOKUP_REGISTRATIONMODE = "RegistrationMode";
        public const string LOOKUP_WITHDRAWALMODE = "WithdrawalRegistrationMode";
        public const string LOOKUP_MARITAL_STATUS = "MaritalStatus";
        public const string LOOKUP_PHYSICAL_DISABILITY = "PhysicalDisability";
        public const string LOOKUP_COURSEPREFERENCE = "CoursePreference";
        public const string LOOKUP_CERTIFICATETYPE = "CertificateType";
        public const string LOOKUP_RELATIONSHIP = "RelationShip";
        public const string LOOKUP_DEGREE_TYPE_BACHELOR = "TestTypeforBachelor";
        public const string LOOKUP_DEGREE_TYPE_DIPLOMA = "TestTypeforDiploma";
        public const string LOOKUP_ReminderType = "ReminderType";
        public const string LOOKUP_YES_NO = "Yes_No";
        public const string LOOKUP_ABOUT_MACHS = "SourceofKnowlegeAboutMACHS";
        public const string LOOKUP_REFERENCE_MAT_CAT = "ReferenceMaterialCategory";
        public const string LOOKUP_REFERENCE_MAT_TYPE = "ReferenceMaterialType";
        public const string LOOKUP_ANNOUNCEMENT_TYPE = "AnnouncmentType";
        public const string LOOKUP_PROGRAM_CATEGORY = "ProgramCategory";
        public const string LOOKUP_DURATION_IN_YEARS = "DurationInYears";
        public const string LOOKUP_REQUISITESCONDITION = "RequisitesCondition";
        public const string LOOKUP_EXAM_TYPE = "ExamType";
        public const string LOOKUP_ASSIGNMENT_ALLOTMENT_STATUS = "AssignmentAllotmentStatus";
        public const string LOOKUP_LEAVE_REASONS = "LeaveReasons";
        public const string LOOKUP_DEGREE_CERTIFICATE = "Degree_Certificate";
        public const string LOOKUP_SCORE_TYPE = "ScoreTypes";
        public const string LOOKUP_SCORE_TYPE_MINIMUM_SCORE = "MinimumScore";
        public const string LOOKUP_COURSE_TYPE = "CourseType_Minor_Major";

        // sree
        public const string LOOKUP_PAYMENT_MODE = "PaymentMode";
        public const string LOOKUP_REQUESTCATEGORY = "RequestCategory";
        public const string LOOKUP_STUDENTADMISSIONSTATUS = "StudentAdmissionStatus";
        public const string LOOKUP_GPATYPE = "GPAType";
        public const string LOOKUP_DEANPENALTYTYPE = "DeanPenaltyType";
        public const string LOOKUP_PENALTYTYPE = "PenaltyType";
        public const string LOOKUP_TRANSFERREQUESTTYPE = "StudentRequestType";
        public const string LOOKUP_UNIVERSITY_GROUP_TYPE = "UniversityGroupType";
        public const string LOOKUP_STUDENT_ELIGIBILITY_TYPE = "StudentEligibilityType";
        public const string LOOKUP_FUNDING_CATEGORY_TYPE = "FundingCategory";

        //kiran
        public const string LOOKUP_STUDENT_APPROVALSTATUS = "ApprovalStatus";
        public const string LOOKUP_FUNDING_TYPE = "FundingType";
        public const string LOOKUP_CHECKLISTSTATUS_TYPE = "DocumentChecklistStatus";

        public const string LOOKUP_GCRESULTSTATUS_TYPE = "GCResultStatus";
        public const string LOOKUP_GCCLASS_TYPE = "GCClass";
        public const string LOOKUP_ITEM_CLEARANCE_STATUS = "ItemClearanceStatus";
        public const string LOOKUP_QUESTION_TYPE = "QuestionType";
        #endregion

        #region Session Value

        public const string APPLICATIONID = "ApplicationId";
        public const string STUD_APPLICATIONID = "StudentApplicationId";
        public const string MY_CULTURE = "MyCulture";
        public const string MY_UICULTURE = "MyUICulture";
        public const string SESSION_VIRTUAL = "virtual";
        public const string SESSION_SELECTED_CLIENT_ID = "SelectedClientId";
        public const string SESSION_EDIT_CLIENT_ID = "EditClientId";
        public const string SESSION_SITE_PAGE_UPLOADED_IMAGE_NAME = "SitePageUploadedImageName";
        public const string SESSION_MENU_ITEMS = "MenuItems";
        public const string SESSION_LOGEDUSERNAME = "LogedUsername";
        public const string SESSION_TIMEOUT = "SessionTimeout";
        public const string SESSION_EMAIL_SCHEDULE_DELIVERY = "SesionEmailScheduleDeliveryDashboard";
        public const string SESSION_SCHEDULE_SEARCH = "SessionScheduleSearch";
        public const string SESSION_SEARCH_OBJECT = "SessionSearchObject";
        public const string SESSION_APPLYFOR = "ApplyFor";
        public const string SESSION_DEGREE = "Degree";
        public const string SESSION_SEMESTERAPPLY = "SemesterApply";
        public const string SESSION_USERNAME = "Username";
        public const string SESSION_COURSE_GROUP_ID = "CourseGroupID";
        public const string SESSION_COURSE_ID = "CourseId";
        public const string SESSION_PROGRAM_ID = "ProgramId";
        public const string SESSION_DEGREETYPEID = "DegreeTypeId";
        public const string SESSION_SEMESTERAPPLY_FOR_YEAR = "SemesterApplyForYear";
        public const string SESSION_DEGREE_NAME = "DegreeName";
        public const string SESSION_SEMESTERAPPLY_ID = "SemesterApplyID";
        public const string SESSION_PROGRAM_NAME = "ApplyProgramName";
        public const string SESSION_TRANSFER_SEMESTER_ID = "TransferSemesterID";
        public const string SESSION_RAW_STUD_USER = "RawStudUser";
        public const string SESSION_TERM_ID = "TermId";
        public const string SESSION_IDENTIFICATION = "Identfication";
        public const string SESSION_GROUPBY = "GroupBy";
        public const string SESSION_STUDENTID = "SessionStudentId";

        //ForReport Params
        public const string SESSION_STUDENTAPPLREPORT_TERM_ID = "StudApplReport_TermId";
        public const string SESSION_STUDENTAPPLREPORT_APPLICATION_TYPE = "StudApplReport_ApplicationType";
        public const string SESSION_STUDENTAPPLREPORT_DEGREETYPE_ID = "StudApplReport_DegreeTypeId";
        public const string SESSION_STUDENTAPPLREPORT_APPLICATION_LIST_FOR = "StudApplReport_ApplicationListFor";

        public const string SESSION_PENDINGAPPLREPORT_TERM_ID = "PendingApplReport_TermId";
        public const string SESSION_PENDINGAPPLREPORT_APPLICATION_TYPE = "PendingApplReport_ApplicationType";
        public const string SESSION_PENDINGAPPLREPORT_DEGREETYPE_ID = "PendingApplReport_DegreeTypeId";
        public const string SESSION_PENDINGAPPLDTLSREPORT_GROUP_BY = "PendingApplDtlsReport_GroupBy";
        public const string SESSION_PENDINGAPPLDTLSREPORT_STATUS = "PendingApplDtlsReport_Status";

        public const string SESSION_STUDENTFUNDINGDETAILS_FUNDINGTYPE = "StudentFundingDetails_FundType";
        public const string SESSION_STUDENTFUNDINGDETAILS_BATCH = "StudentFundingDetails_Batch";
        public const string SESSION_STUDENTFUNDINGDETAILS_START_DATE = "StudentFundingDetails_StartDate";
        public const string SESSION_STUDENTFUNDINGDETAILS_END_DATE = "StudentFundingDetails_EndDate";

        public const string SESSION_STUDENTSTATUS_TERMSEM = "StudentStatus_TermSem";
        public const string SESSION_STUDENTSTATUS_TERMYEAR = "StudentStatus_TermYear";
        public const string SESSION_STUDENTSTATUS_STATUS = "StudentStatus_Status";

        public const string SESSION_RECEIVABLEBYSTUDENT_TERMSEM = "ReceivableByStudent_TermSem";
        public const string SESSION_RECEIVABLEBYSTUDENT_TERMYEAR = "ReceivableByStudent_TermYear";
        public const string SESSION_RECEIVABLEBYSTUDENT_STUDENTID = "ReceivableByStudent_StudentID";

        public const string SESSION_FEESUMMARY_TERMSEM = "FeeSummary_TermSem";
        public const string SESSION_FEESUMMARY_TERMYEAR = "FeeSummary_TermYear";

        public const string SESSION_RECEIVABLEFUNDINGS_TERMSEM = "ReceivableFundings_TermSem";
        public const string SESSION_RECEIVABLEFUNDINGS_TERMYEAR = "ReceivableFundings_TermYear";
        public const string SESSION_RECEIVABLEFUNDINGS_PROGRAM = "ReceivableFundings_Program";
        public const string SESSION_RECEIVABLEFUNDINGS_FINDING_TYPE = "ReceivableFundings_FundingType";

        public const string SESSION_FUNDINGSUMMARY_FUNDINGTYPE = "FunsSummary_FundType";
        public const string SESSION_FUNDINGSUMMARY_BATCH = "FunsSummary_Batch";
        public const string SESSION_FUNDINGSUMMARY_START_DATE = "FunsSummary_StartDate";
        public const string SESSION_FUNDINGSUMMARY_END_DATE = "FunsSummary_EndDate";

        public const string SESSION_RECEIVABLEBYTERM_TERMSEM = "ReceivableByTerm_TermSem";
        public const string SESSION_RECEIVABLEBYTERM_TERMYEAR = "ReceivableByTerm_TermYear";


        public const string SESSION_STUDENTPROFILE_TERMSEM = "StudentProfile_TermSem";
        public const string SESSION_STUDENTPROFILE_TERMYEAR = "StudentProfile_TermYear";
        public const string SESSION_STUDENTPROFILE_STUDENTID = "StudentProfile_StudentID";
        public const string SESSION_STUDENTPROFILE_STUDENTNAME = "StudentProfile_StudentName";
        public const string SESSION_STUDENTPROFILE_PROGRAMID = "StudentProfile_ProgramID";
        public const string SESSION_STUDENTPROFILE_JOININGYEAR = "StudentProfile_JoiningYear";
        public const string SESSION_STUDENTPROFILE_STUDENTSTATUS = "StudentProfile_StudentStatus";

        public const string SESSION_STUDENTSEARCH_TERMSEM = "StudentSearch_TermSem";
        public const string SESSION_STUDENTSEARCH_TERMYEAR = "StudentSearch_TermYear";
        public const string SESSION_STUDENTSEARCH_STUDENTID = "StudentSearch_StudentID";
        public const string SESSION_STUDENTSEARCH_STUDENTNAME = "StudentSearch_StudentName";
        public const string SESSION_STUDENTSEARCH_PROGRAMID = "StudentSearch_ProgramID";
        public const string SESSION_STUDENTSEARCH_JOININGYEAR = "StudentSearch_JoiningYear";
        public const string SESSION_STUDENTSEARCH_STUDENTSTATUS = "StudentSearch_StudentStatus";

        public const string SESSION_ADDMISSIONSTATUS_TERMID = "AdmissionStatus_TermID";
        public const string SESSION_ADDMISSOINSTATUS_APPLICATIONTYPE = "AdmissionStatus_ApplcationType";
        public const string SESSION_ADDMISSOINSTATUS_DEGREETYPE = "AdmissionStatus_DegreeType";
        public const string SESSION_ADDMISSOINSTATUS_DEGREETYPE_ID = "AdmissionStatus_DegreeTypeID";
        public const string SESSION_ADDMISSOINSTATUS_PROGRAM_ID = "AdmissionStatus_ProgramID";

        public const string SESSION_TRANSCRIPT_TERMYEAR = "Transcript_TermYear";
        public const string SESSION_TRANSCRIPT_TERMSEM = "Transcript_TermSem";
        public const string SESSION_REPORTTYPE = "Transcript_Type";
        public const string SESSION_REPORTPath = "";
        public const string SESSION_PROGRAMID = "Transcript_ProgramID";

        public const string SESSION_DHL_TERMID = "DHL_TermID";
        public const string SESSION_DHL_CLASSLEVEL = "Class_Level";
        public const string SESSION_DHL_MAJOR = "Program_ID";

        public const string SESSION_PROBATION_TERMID = "Probation_TermID";
        public const string SESSION_PROBATION_CLASSLEVEL = "Probation_Class_Level";

        public const string SESSION_STUD_CREDIT_SUMMARY_STUD_ID = "Stud_Credit_Summary_StudentId";
        public const string SESSION_STUD_CREDIT_SUMMARY_STUD_NAME = "Stud_Credit_Summary_StudentName";
        public const string SESSION_STUD_CREDIT_SUMMARY_MAJOR = "Stud_Credit_Summary_Major";
        public const string SESSION_STUD_CREDIT_SUMMARY_TERM = "Stud_Credit_Summary_Term";
        public const string SESSION_STUD_CREDIT_SUMMARY_CLASS_LEVEL = "Stud_Credit_Summary_ClassLevel";
        public const string SESSION_REQUESTED_ID = "RequestedById";
        public const string SESSION_STUD_GRADE_SUMMARY_GROUPBY = "Stud_Grade_Summary_GroupBy";

        //kiran
        public const string SESSION_ACADEMICYEAR = "AcademicYear";
        public const string SESSION_MAJOR = "Major";

        public const string SESSION_STUD_ATTENDANCE_STUD_ID = "Stud_Attendance_StudentId";
        public const string SESSION_STUD_ATTENDANCE_TERM = "Stud_Attendance_Term";
        public const string SESSION_STUD_ATTENDANCE_STUD_NAME = "Stud_Attendance_Student_Name";
        public const string SESSION_STUD_ATTENDANCE_STUD_CLASS_LEVEL = "Stud_Attendance_ClassLevel";
        public const string SESSION_STUD_ATTENDANCE_MAJOR = "Stud_Attendance_Major";
        public const string SESSION_STUD_ATTENDANCE_GENDER = "Stud_Attendance_Gender";
        public const string SESSION_STUD_ATTENDANCE_COURSE = "Stud_Attendance_Course";
        public const string SESSION_STUD_ATTENDANCE_CRN = "Stud_Attendance_CRN";

        public const string SESSION_CLASSSCHEDULE_TERMID = "ClassSchedule_TermID";
        public const string SESSION_CLASSSCHEDULE_GROUPBY = "ClassSchedule_GroupBy";
        public const string SESSION_CURRENTTERM_ID = "Session_CurrentTerm_ID";
        public const string SESSION_CURRENTACADEMICYEAR_ID = "Session_CurrentAY_ID";

        public const string SESSION_ACADEMICHOLDS_STUD_ID = "Academic_Holds_StudentId";
        public const string SESSION_ACADEMICHOLDS_STUD_NAME = "Academic_Holds_StudentName";
        public const string SESSION_ACADEMICHOLDS_PROGRAM_ID = "Academic_Holds_ProgramId";
        public const string SESSION_ACADEMICHOLDS_TERM_ID = "Academic_Holds_TermId";
        public const string SESSION_ACADEMICHOLDS_CLASSLEVEL = "Academic_Holds_ClassLevel";

        public const string SESSION_STUDPENALTIES_STUD_ID = "StudPenalties_StudentId";
        public const string SESSION_STUDPENALTIES_STUD_NAME = "StudPenalties_StudentName";
        public const string SESSION_STUDPENALTIES_PROGRAM_ID = "StudPenalties_ProgramId";
        public const string SESSION_STUDPENALTIES_TERM_ID = "StudPenalties_TermId";
        public const string SESSION_STUDPENALTIES_CLASSLEVEL = "StudPenalties_ClassLevel";

        #endregion

        #region Program
        public const string FOUNDATION_YEAR = "PRMF0004";
        #endregion
    }
}
