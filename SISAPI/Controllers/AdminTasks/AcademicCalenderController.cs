﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using TicketIt.Request.AdminTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketIt.Services;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;
using System.Data;
//using SISAPI.Response;
using TicketIt.Services.AdminTasks;
using Microsoft.AspNetCore.Hosting;

namespace TicketIt.Controllers.AdminTasks
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class StudentInfoController : ControllerBase
    {
        IAcademicCalenderService _acdemiCalService;
        public StudentInfoController(IAcademicCalenderService academicCalenderService)
        {
            _acdemiCalService = academicCalenderService;
        }
        /* [HttpGet]
         public IActionResult GetList(string EventType)
         {
             try
             {
                 //var List = _acdemiCalService.GetList(EventType);
                 return Ok("");
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }
         }
         [HttpGet]
         public IActionResult GenerateBillingStatement(string StudentId)
         {
             try
             {
                 var List = _acdemiCalService.GenerateBillingStatement(StudentId);
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }
         [HttpGet]
         public IActionResult GetNafethPaymentLogs(string StudentId)
         {
             try
             {
                 var List = _acdemiCalService.GetNafethPaymentLogs(StudentId);
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }
         [HttpGet]
         public IActionResult GetPaymentsData(string StudentId)
         {
             try
             {
                 var List = _acdemiCalService.GetPaymentsData(StudentId);
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }
         [HttpPost]
         public IActionResult GetStudentDetails([FromBody] StdudentDto stdudentDto)
         {
             try
             {
                 var List = _acdemiCalService.GetStudentDetails(stdudentDto);
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }
         [HttpPost]
         public IActionResult Login([FromBody] LoginModel stdudentDto)
         {
             try
             {
                 var List = _acdemiCalService.Login(stdudentDto);
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }

         [HttpGet]
         public IActionResult GetAllPrograms()
         {
             try
             {
                 var List = _acdemiCalService.GetAllPrograms();
                 return Ok(List);
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }

         }




         //[HttpPost]
         //public object AddEdit([FromBody] AcademicCalenderRequest request)
         //{
         //    try
         //    {
         //        var resp = _acdemiCalService.AddEditEvent(request);
         //        var response = new { status = resp };
         //        return response;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw new Exception(ex.Message);
         //    }
         //}
         //[HttpDelete]
         //public object Delete(string CalId)
         //{
         //    try
         //    {
         //        var resp = _acdemiCalService.Delete(CalId);
         //        var response = new { status = resp };
         //        return response;
         //    }
         //    catch (Exception ex)
         //    {
         //        throw new Exception(ex.Message);
         //    }
         //}
         //[HttpGet]
         //public IActionResult GetInterviewForm(string DegreeTypeId)
         //{
         //    try
         //    {
         //        var List = _acdemiCalService.GetInterviewForm(DegreeTypeId);
         //        return Ok(List);
         //    }
         //    catch (Exception ex)
         //    {
         //        throw new Exception(ex.Message);
         //    }

         //}
     }*/
     public class StdudentDto
     {
         public  string studentId { get; set; }
         public string name { get; set; }
         //public int program { get; set; }
         public string classLevel { get; set; }
     }
 
       
    }
}