﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using TicketIt.Services.landingpage;
using System.IO;

namespace TicketIt.Controllers.landingpagecontroler
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class LandingpageController
    {


        Ilandingpage _landingpageservice;
        private string _pdfdirectory;

        public LandingpageController(Ilandingpage landingpageservice)
        {
            _landingpageservice = landingpageservice;
        }

        [HttpGet]
        public dynamic GetCountries()
        {
            try
            {
                var List = _landingpageservice.GetCountries();
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        [HttpGet]
        public dynamic GetTopics()
        {
            try
            {
                var List = _landingpageservice.GetTopics();
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]
        public dynamic GetTopicscontent()
        {
            try
            {
                var List = _landingpageservice.GetTopicscontent();
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]
        public dynamic GetAdvertisements()
        {
            try
            {
                var List = _landingpageservice.GetAdvertisements();
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

       



        [HttpPost]
        public dynamic GetAdvertisementsById(string countryid)
        {
            try
            {
                var List = _landingpageservice.GetAdvertisementsById(countryid);
                return List;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}





