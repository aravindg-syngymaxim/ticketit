﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Utility
{
    public class ColumnSchema
    {
        #region Language
        public class Language
        {
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_LANG_ENG_NAME = "LanguageEnglishName";
            public const string COL_LANG_NAME = "LanguageName";
            public const string COL_CHAR_SET_TYPE = "CharacterSetType";
            public const string COL_TEXT_DIRECTION = "TextDirection";
            public const string COL_LITERAL_XML = "BaseLiteralXML";
        }
        #endregion

        #region Common
        public class Common
        {
            //Columns
            public const string COL_CREATED_DATE = "DateCreated";

            //VALUES
            public const string VAL_UPDATE_MODE = "ED";
            public const string VAL_INSERT_MODE = "IN";
            public const string VAL_DELETE_MODE = "DL";

            //VALUES FOR ANNOUNCEMENT TYPE OPTIONS
            public const string VAL_ANNOUNCEMENTTYPE_GLOBAL = "global";
            public const string VAL_ANNOUNCEMENTTYPE_PROGRAM = "programspecific";
            public const string VAL_ANNOUNCEMENTTYPE_COURSE = "coursespecific";
            public const string VAL_ANNOUNCEMENTTYPE_GROUP = "groupspecific";

            public const string COL_NUMBER_OF_STUDENT = "NumberOfStudent";
            public const string COL_ROW_INDEX = "rowindex";
        }
        #endregion

        #region SystemMessage
        // testing comments
        internal class SystemMessage
        {
            public const string COL_MESSAGE_ID = "MessageId";
            public const string COL_MESSAGE_TEXT = "MessageText";
            public const string COL_MESSAGE_DESC = "MessageDescription";
            public const string COL_MESSAGE_TYPE = "MessageType";
            public const string COL_MESSAGE_FOR = "MessageFor";
        }
        #endregion

        #region UserPage
        public class UserPage
        {
            //FIELDS
            public const string COL_PAGE_ID = "PageId";
            public const string COL_PAGE_ENGLISH_NAME = "PageEnglishName";
            public const string COL_PAGE_FILE_URL = "PageFileURL";
            public const string COL_DISPLAY_ORDER = "DisplayOrder";

            public const string COL_CONFIG_TYPE = "ConfigType";
            public const string COL_ELEMENT_TYPE = "ElementType";
            public const string COL_VALIDATION_ID = "ValidationId";
            public const string COL_ELEMENT_DISPLAY_NAME = "ElementDisplayName";
            public const string COL_IS_MANDATORY = "IsMandatory";
            public const string COL_IS_READ_ONLY = "IsReadOnly";

            public const string COL_PAGE_ELEMENT_ID = "PageElementId";
            public const string COL_ELEMENT_NAME = "ElementName";
            public const string COL_ELEMENT_TEXT = "ElementText";
            public const string COL_IS_IMAGE_AVAIL = "IsImageAvailable";
            public const string COL_ELEMENT_IMAGE_FILE_NAME = "ElementImageFileName";
        }
        #endregion

        #region TempStudentMaster
        public class TempStudentMaster
        {

            //FIELDS
            public const string COL_APPLICATION_ID = "ApplicationID";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_COURSE_TYPE = "CourseType";
            public const string COL_APPLICATIONFORSEM = "ApplicationForSem";
            public const string COL_STUDENT_APPLICATION_ID = "StudentApplicationID";
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_EMAIL_ID = "EmailId";
            public const string COL_ALTERNATEEMAIL = "AlternateEmail";
            public const string COL_CHAT_MESSAGER = "ChatMessager";
            public const string COL_NATIONALITY = "Nationality";
            public const string COL_PHOTOGRAPH = "Photograph";
            public const string COL_RELIGION = "Religion";
            public const string COL_DATEOFBIRTH = "DateOfBirth";
            public const string COL_PLACEOFBIRTH = "PlaceOfBirth";
            public const string COL_IDENTIFICATION = "Identification";
            public const string COL_NATIONALID = "NationalId";
            public const string COL_PASSPORT_IQAMANO = "Passport_IqamaNo";
            public const string COL_PASSPORT_IQAMAEXPIRYDATE = "Passport_IqamaExpiryDate";
            public const string COL_PASSPORT_IQAMAISSUEDATE = "Passport_IqamaIssueDate";
            public const string COL_PASSPORT_IQAMAISSUEPLACE = "Passport_IqamaIssuePlace";
            public const string COL_IQAMATYPE = "IqamaType";
            public const string COL_SPONSORMOBILENO = "SponsorMobileNo";
            public const string COL_SPONSORCOUNTRYCODE = "SponsorCountryCode";
            public const string COL_FAMILYID = "FamilyID";
            public const string COL_GENDER = "Gender";
            public const string COL_MARITALSTATUS = "MaritalStatus";
            public const string COL_POBOX = "POBox";
            public const string COL_CITY = "CityId";
            public const string COL_POSTALCODE = "PostalCode";
            public const string COL_PROVINCE = "ProvinceId";
            public const string COL_COUNTRY = "CountryId";
            public const string COL_HOME_TELEPHONE_NO = "HomeTelephoneNo";
            public const string COL_OFFICE_TELEPHONE_NO = "OfficeTelephoneNo";
            public const string COL_FAX_NO = "FAXNo";
            public const string COL_MOBILE_NO = "MobileNo";
            public const string COL_COUNTRYCODE = "MobileCountryCode";
            public const string COL_ISPHYSICALDISABILITY = "IsPhysicalDisability";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";
            public const string COL_FIRST_PREFERENCE = "FirstPreference";
            public const string COL_SECOND_PREFERENCE = "SecondPreference";
            public const string COL_THIRD_PREFERENCE = "ThirdPreference";
            public const string COL_FORTH_PREFERENCE = "ForthPreference";
            public const string COL_FIFTH_PREFERENCE = "FifthPreference";
            public const string COL_ACCEPTED_PREFERENCE = "AcceptedPrefrences";
            public const string COL_ISCERTIFY = "IsCertify";
            public const string COL_ISREAD_INSTRUCTION = "IsReadInstruction";
            public const string COL_USERNAME = "UserName";
            public const string COL_PASSWORD = "Password";
            public const string COL_PREFIX = "Prefix";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_GRAND_FATHER_NAME = "GrandFatherName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_SPONSOR_NAME = "SponsorName";
            public const string COL_SPONSOR_ADDRESS = "SponsorAddress";
            public const string COL_ADDRESS = "Address";
            public const string COL_LISTOFHOBBIES = "ListOfHobbies";
            public const string COL_REASON_FOR_EDUCATIONALGAP_IFANY = "ReasonForEducationalGap_Ifany";
            public const string COL_ARFIRSTNAME = "ARFirstName";
            public const string COL_ARMIDDLENAME = "ARMiddleName";
            public const string COL_ARGRANDFATHER_NAME = "ARGrandFatherName";
            public const string COL_ARLASTNAME = "ARLastName";
            public const string COL_APPLICATION_FOR_YEAR = "ApplicationForYear";
            public const string COL_SYSTEM_USER_GUID = "SystemUserGUID";
            public const string COL_NATIONALITY_NAME = "Nationality1";
            public const string COL_RELIGION_NAME = "ReligionName";
            public const string COL_FIRST_PREFERENCE_NAME = "FirstPreferenceName";
            public const string COL_SECOND_PREFERENCE_NAME = "SecondPreference1";
            public const string COL_THIRD_PREFERENCE_NAME = "ThirdPreference1";
            public const string COL_DEGREE_TYPE_NAME = "DegreeTypeName";
            public const string COL_DEGREE_TYPE_ID = "DegreeTypeId";
            public const string COL_APPLICATION_FOR_SEM_NAME = "ApplicationForSemName";
            public const string COL_SEMESTER_APPLY_FOR = "SemesterApplyFor";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_LAST_SECTION_UPDATED = "LastSectionUpdated";
            public const string COL_TERM_ID = "TermId";
            public const string COL_TERM_NAME = "TermName";

            public const string COL_FIRST_PREFERENCENAME = "FirstPreferenceName";
            public const string COL_SECOND_PREFERENCENAME = "SecondPreferenceName";
            public const string COL_THIRD_PREFERENCENAME = "ThirdPreferenceName";
            public const string COL_FORTH_PREFERENCENAME = "ForthPreferenceName";
            public const string COL_FIFTH_PREFERENCENAME = "FifthPreferenceName";

            public const string COL_FIRST_PREF = "FirstPreferenceName";
            public const string COL_SECOND_PREF = "SecondPreferenceName";
            public const string COL_THIRD_PREF = "ThirdPreferenceName";
            public const string COL_FORTH_PREF = "ForthPreferenceName";
            public const string COL_FIFTH_PREF = "FifthPreferenceName";

            public const string COL_FIRST_PREFERENCETITLE = "FirstPrefrenceTitle";
            public const string COL_SECOND_PREFERENCETITLE = "SecondPrefrenceTitle";
            public const string COL_THIRD_PREFERENCETITLE = "ThirdPrefrenceTitle";
            public const string COL_FORTH_PREFERENCETITLE = "ForthPrefrenceTitle";
            public const string COL_FIFTH_PREFERENCETITLE = "FifthPrefrenceTitle";

            public const string COL_FIRST_ACCEPTEDPREFERENCENAME = "FirstAcceptedPreference";
            public const string COL_SECOND_ACCEPTEDPREFERENCENAME = "SecondAcceptedPreference";
            public const string COL_THIRD_ACCEPTEDPREFERENCENAME = "ThirdAcceptedPreference";
            public const string COL_FORTH_ACCEPTEDPREFERENCENAME = "ForthAcceptedPreference";
            public const string COL_FIFTH_ACCEPTEDPREFERENCENAME = "FifthAcceptedPreference";

            public const string COL_DATESUBMITTED = "DateSubmitted";
            public const string COL_DUE_DATE_CONFIRMATION = "DueDate";
            public const string COL_IS_STUDENT_SENT_CONFIRMATION = "IsStudentSentConfirmation";
            public const string COL_CONFIRM_PROGRAM_TITLE = "ConfirmProgramTitle";
            public const string COL_STUDENT_CONFIRM_COMMENT = "StudentConfirmComments";
            public const string COL_STUDENT_CONFIRM_PROGRAMID = "StudentConfirmProgramId";
            public const string COL_ISOVERDUE = "ISOverDue";
            public const string COL_CONFIRMATION_TYPE = "ConfirmationType";
            public const string COL_CONFIRMATION_DATE = "ConfirmationDate";
            public const string COL_CONFIRMATION_FOR_TERMID = "ConfirmationForTermId";

            public const string COL_APPLICATION_DETAILS = "ApplicationDetails";
            public const string COL_APPLICATION_CHECKlIST_STATUS = "ApplicationCheckListStatus";
            public const string COL_CONFIRMATION_TERM_NAME = "ConfirmationTermName";
            public const string COL_CONFIRMATION_PROGRAM_NAME = "ConfirmationProgramName";
            public const string COL_STUDENT_ID = "StudentId";
            public const string COL_CHECKLIST_STATUS = "ApplicationChecklistStatus";
            public const string COL_PREFERENCE = "Prefernces";
            public const string COL_APPLICATIONSTATUS = "ApprovalStatus";
            public const string COL_LASTMODIFIEDBY = "LastModifiedByName";
            public const string COL_DEGREE_NAME = "DegreeName";

            public const string COL_STUDENT_INFO = "StudentInfo";
        }
        #endregion

        #region Temp Student Language
        public class TempStudentLanguage
        {
            //FIELDS

        }
        #endregion

        #region Temp Student Parental Information
        public class TempStudentParentalInformation
        {
            //FIELDS
            public const string COL_PARENTALINFO_ID = "ParentalInfoID";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_RELATIONSHIP = "Relationship";
            public const string COL_PREFIX = "Prefix";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_GRAND_FATHERNAME = "GrandFatherName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_EMAILID = "EmailId";
            public const string COL_ADDRESS = "Address";
            public const string COL_POBOX = "POBox";
            public const string COL_CITY = "CityId";
            public const string COL_POSTALCODE = "PostalCode";
            public const string COL_PROVINCE = "ProvinceId";
            public const string COL_COUNTRY_ID = "CountryId";
            public const string COL_HOME_TELEPHONE_NO = "HomeTelephoneNo";
            public const string COL_OFFICE_TELEPHONE_NO = "OfficeTelephoneNo";
            public const string COL_FAXNO = "FAXNo";
            public const string COL_MOBILENO = "MobileNo";
            public const string COL_MOBILE_COUNTRY_CODE = "MobileCountryCode";
            public const string COL_EMPLOYER_DETAILS = "EmployerDetails";
        }
        #endregion

        #region Tempstud Educational Details
        public class TempStudentEducationalDetails
        {

            //FIELDS
            public const string COL_EDUCATIONALDETAIL_ID = "EducationalDetailId";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_COURSE_OR_TESTNAME = "Course_TestName";
            public const string COL_SCHOOLNAME = "SchoolOrUniversityName";
            public const string COL_SCHOOL_CODE = "SchoolCode";
            public const string COL_SC_CODE = "StudyCenterCode";
            public const string COL_SCHOOL_LOCATION = "Location";
            public const string COL_FROM_MONTH = "FromMonth";
            public const string COL_TO_MONTH = "ToMonth";
            public const string COL_FROM_YEAR = "FromYear";
            public const string COL_TO_YEAR = "ToYear";
            public const string COL_PERCENTAGE = "Percentage";
            public const string COL_GPATYPE = "GPAType";
            public const string COL_SCORE_OR_GRADE = "Score_Grade";
            public const string COL_CERTIFICATION_TYPE = "CertificationType";
            public const string COL_EDUCATION_TYPE = "EducationType";
            public const string COL_MARKSHEET_PATH = "MarksheetPath";
            public const string COL_CERTIFICATION_TYPE_VALUE = "CertificationTypeValue";
        }
        #endregion

        #region Tempstud Pursuing Education
        public class TempStudentPursuingEducation
        {

            //FIELDS
            public const string COL_PEDUCATION_ID = "PEducationID";
            public const string COL_APPLICATION_ID = "ApplicationID";
            public const string COL_COLLEGENAME = "CollegeName";
            public const string COL_COLLEGE_ADDRESS = "CollegeAddress";
            public const string COL_COLLEGEID = "CollegeId";
            public const string COL_GPATYPE = "GPAType";
            public const string COL_CREDITS = "Credits";
            public const string COL_PROGRAMNAME = "ProgramName";
            public const string COL_SEMESTERNAME = "SemesterName";
            public const string COL_TERMNAME = "TermName";
            public const string COL_CREATEDBYID = "CreatedById";
            public const string COL_NOOFWARNINGS = "NoOfWarning";
            public const string COL_TYPEOFWARNINGS = "TypeOfWarning";
            public const string COL_HASACADEMICWARNINGS = "HadAcademicWarnings";

        }
        #endregion

        #region Temp transfer stud course details
        public class TempTransferStudentCoursesDetails
        {

            //FIELDS
            public const string COL_APPLICATION_COURSES_DETAILSID = "ApplicationCoursesDetailsId";
            public const string COL_APPLICATION_ID = "ApplicationID";
            public const string COL_COURSENAME = "CourseName";
            public const string COL_CREDITS = "Credits";
            public const string COL_SUBJECT = "Subject";
            public const string COL_GRADESCORE = "GradeScore";
            public const string COL_SEQUENCEORDER = "SequenceOrder";
            public const string COL_MAPCOURSETITLE = "MapCourseTitle";
        }
        #endregion

        #region TempStudent Sponsors Details
        public class TempStudentSponsors
        {
            //FIELDS
            public const string COL_SPONSOR_GUID = "SponsorGUID";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_SPONSOR_NAME = "SponsorName";
            public const string COL_SPONSOR_TELEPHONE = "SponsorTelephoneNo";
            public const string COL_SPONSOR_WORKING_ADD = "SponsorWorkingAddress";
            public const string COL_SPONSOR_EMAIL = "SponsorEmailAddress";
        }
        #endregion

        #region TempStudent Physical Disability
        public class TempStudentPhysicalDisability
        {
            //FIELDS
            public const string COL_PHYSICALDISABILITY_ID = "PhysicalDisabilityId";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_TYPEOFPHYSICALDISABILITY = "TypeOfPhysicalDisability";
            public const string COL_DISABILITY_DESCRIPTION = "DisabilityDescription";
        }
        #endregion

        #region LookUp
        internal class Lookup
        {
            //FIELDS
            public const string COL_LOOKUP_ID = "LookupId";

            public const string COL_LOOKUP_TEXT = "LookupText";
            public const string COL_LOOKUP_VALUE = "LookupValue";
            public const string COL_LOOKUP_TYPE_ID = "LookupTypeId";
            public const string COL_LOOKUP_TYPE = "LookupType";
            public const string COL_LOOKUP_LANGUAGEID = "LanguageId";
            public const string COL_IS_DEFAULT = "IsDefault";

            public const string COL_ENGLISHLOOKUPTEXT = "EnglishLookupText";
            public const string COL_ARLOOKUP_TEXT = "ARLookupText";
            public const string COL_LOOKUP_CODE = "LookupValue";
            public const string COL_LOOKUP_STATUS = "Status";
        }
        #endregion

        #region CountryMaster
        public class CountryMaster
        {
            //FIELDS
            public const string COL_COUNTRY_ID = "CountryID";
            public const string COL_CONTINENT_ID = "ContinentId";
            public const string COL_SUBCONTINENT_ID = "SubContinentId";
            public const string COL_CODE = "Code";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_COUNTRYNAME = "CountryName";
            public const string COL_NATIONALITY = "Nationality";
            public const string COL_ARNAIONALITY = "ARNationality";
            public const string COL_ARCountryName = "ARCountryName";
            public const string COL_STATUS = "Status";

        }
        #endregion

        #region CountryProvinceMaster
        public class CountryProvinceMaster
        {
            //FIELDS
            public const string COL_PROVINCE_ID = "ProvinceId";
            public const string COL_COUNTRY_ID = "CountryID";
            public const string COL_CODE = "Code";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_PROVINCENAME = "ProvinceName";
            public const string COL_ARPROVINCENAME = "ARProvinceName";
            public const string COL_COUNTRYNAME = "CountryName";
            public const string COL_STATUS = "Status";
        }
        #endregion

        #region TempStudent Course Preferences
        public class TempStudentCoursePreferences
        {
            //FIELDS
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_COURSE_ID = "ProgramId";
            public const string COL_COURSEPREFERENCE = "ProgramPreference";
        }
        #endregion

        #region TempStudent Additional Information
        public class TempStudentAdditionalInformation
        {
            //FIELDS
            public const string COL_ADDITIONALINFO_ID = "AdditionalInformationID";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_LANGUAGEID = "LanguageId";
            public const string COL_ADDITIONINFORMATIONVALUE = "AdditionInformationValue";
            public const string COL_STATUS = "Status";
            public const string COL_DETAILS = "Details";
        }
        #endregion

        #region StudyCenterMaster
        public class StudyCenterMaster
        {
            public const string COL_STUDYCENTER_ID = "StudyCenterId";
            public const string COL_CITY_ID = "CityId";
            public const string COL_STUDY_CENTERCODE = "StudyCenterCode";
            public const string COL_STUDYCENTER_CODE = "Code";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_STUDYCENTER_NAME = "StudyCenterName";
            public const string COL_DESCRIPTION = "Description";
        }
        #endregion

        #region ProgramMaster

        public class ProgramMaster
        {
            //FIELDS
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_PROGRAM_TITLE = "ProgramTitle";
            public const string COL_OTHERDESCRIPTION = "OtherDescription";
            public const string COL_PROGRAM_DISPLAY_TEXT = "ProgramDisplayText";
            public const string COL_PROGRAM_PATH = "ProgramPath";
            public const string COL_PROGRAM_CODE = "ProgramCode";
            public const string COL_DURATION_IN_YEARS = "DurationInYears";
            public const string COL_DURATION_IN_REBAT_YEARS = "DurationInRebatYears";
            public const string COL_DURATION_IN_SEMESTERS = "DurationInSemesters";
            public const string COL_DEGREE_TYPE = "DegreeType";
            public const string COL_CATEGORY = "Category";
            public const string COL_APPROVAL_STATUS = "ApprovalStatus";
            public const string COL_CATEGORY_NAME = "CategoryName";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_PROGRAM_NAME = "ProgramName";
        }
        #endregion

        #region CourseMaster

        public class CourseMaster
        {
            //FIELDS
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_COURSE_NAME = "CourseName";
            public const string COL_COURSE_CODE = "CourseCode";
            public const string COL_COURSE_GROUPID = "CourseGroupId";
            public const string COL_PREREQUISITES = "PreRequisites";
            public const string COL_COREQUISITES = "CoRequisites";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";
            public const string COL_TOTALCREDITS = "TotalCredits";
            public const string COL_NO_OF_COURSES = "NoOfCourses";
            public const string COL_COURSE_GROUPNAME = "CourseGroupName";
            public const string COL_ARABIC_COURSE_TITLE = "ArabicCourseTitle";
            public const string COL_COURSE_DESCRIPTION = "CourseDescription";
            public const string COL_COURSE_DETAILS = "CourseDetails";
            public const string COL_COURSE_GROUP_CODE = "Code";
            public const string COL_COURSE_SUBJECT = "Subject";
            public const string COL_COURSE_PREREQUISITES = "CoursePreRequisites";
            public const string COL_COURSE_COREQUISITES = "CourseCoRequisites";
            public const string COL_IS_SCHEDLED = "IsScheduled";
            public const string COL_IS_MET = "IsMet";
            public const string COL_COURSE = "Course";
            public const string COL_CREDITS = "Credit";
            public const string COL_POINTS = "Points";
            public const string COL_SCORE = "Score";
            public const string COL_ELIGIBLE_SEMESTER = "EligibleSemester";
            public const string COL_SUBJECT = "Subject";
            public const string COL_CONTACTHRS = "ContactHours";
            public const string COL_BILLING = "BillingAmount";
            public const string COL_COURSETYPE = "CourseType";
            public const string COL_CREDITHRS = "CreditHours";
            public const string COL_GRADE_TYPE = "GradeType";
        }
        #endregion

        #region ProgramYears
        public class ProgramYears
        {
            public const string COL_PROGRAMYEAR_ID = "ProgramYearId";
            public const string COL_YEAR = "Year";

        }
        #endregion

        #region SemesterMaster
        public class SemesterMaster
        {
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_TERM = "Term";
        }
        #endregion

        #region ProgramCoursesMaster
        public class ProgramCoursesMaster
        {
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_COURSE_TITLE = "CourseTitle";

        }
        #endregion

        #region DegreeTypeMaster

        public class DegreeTypeMaster
        {
            public const string COL_DEGREETYPE_ID = "DegreeTypeId";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_DEGREE_DESCRIPTION = "DegreeDescription";
            public const string COL_PRE_REQUISITES = "PreRequisites";
            public const string COL_DEGREETYPE = "DegreeType";
        }
        #endregion

        #region FundingTypeMaster

        public class FundingTypeMaster
        {
            public const string COL_FUNDING_TYPE = "FundingType";

        }
        #endregion

        #region SponsorsAllocation

        public class SponsorsAllocation
        {
            public const string COL_SPONALLOC_ID = "SponsorAllocationId";
            public const string COL_TERM_SEM = "TermSem";
            public const string COL_TERM_YEAR = "TermYear";
            public const string COL_TERM_ID = "TermId";
            public const string COL_STUDENT_NO = "NoOFStudent";
            public const string COL_TOTALAMOUNT = "TotalAmount";
            public const string COL_TERMSEMNAME = "TermSemName";
        }

        #endregion

        #region FeeSourceMaster

        public class FeeSourceMaster
        {
            public const string COL_FEE_SOURCE_ID = "FeeSourceId";
            public const string COL_FEE_SOURCE_CODE = "FeeSourceCode";
            public const string COL_FEE_SOURCE_DESCRIPTION = "FeeSourceDescription";
        }
        #endregion

        #region StudentInvoiceDetails

        public class StudentInvoiceDetails
        {
            public const string COL_TOTAL_FEE = "totalfee";


        }
        #endregion

        #region StudentFeePaymentDetails

        public class StudentFeePaymentDetails
        {
            public const string COL_TOTAL_AMT_RECEIVED = "TotalAmountReceived";
            public const string COL_TOTAL_AMT_RECEIVABLE = "TotalAmountReceivable";
        }
        #endregion

        #region  StudentSponsors
        public class StudentSponsors
        {
            public const string COL_SPONSOR_GUID = "SponsorGUID";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";

            public const string COL_COMMENTS = "Comments";

            public const string COL_FUNDING_AMOUNT_PER_MONTH = "FundingAmtPerMonth";
            public const string COL_FUNDING_CONTRACT_NO = "FundingContractNo";
            public const string COL_FUNDING_CONTRACT_DATE = "FundingContractDate";
            public const string COL_FUNDING_CONTRACT_BATCH = "FundingContractBatch";
            public const string COL_FUNDING_PERIOD_IN_MONTHS = "FundingPeriodInMonths";


            public const string COL_STUDENTGUID = "StudentGUId";



        }
        #endregion

        #region AdditionalInformationMaster
        public class AdditionalInformationMaster
        {
            public const string COL_ADDITIONAINFO_GUID = "AdditionalInfoGUID";
            public const string COL_ADDITIONAINFO = "AdditionalInfo";
            public const string COL_INFODATATYPE = "InfoDataType";
            public const string COL_LOOKUP_TYPEID = "LookupTypeId";
            public const string COL_LANGUAGE_ID = "LanguageId";
        }
        #endregion

        #region AnnouncementMaster

        public class AnnouncementMaster
        {
            //FIELDS
            public const string COL_ANNOUNCEMENT_ID = "AnnouncementId";
            public const string COL_ANNOUNCEMENT_TITLE = "DisplayText";
            public const string COL_ANNOUNCEMENT_DESCRIPTION = "Description";
            public const string COL_ANNOUNCEMENT_TYPE = "AnnouncementType";
            public const string COL_ANNOUNCEMENT_DEGREETYPE = "DegreeType";
            public const string COL_ANNOUNCEMENT_PROGRAMIDS = "ProgramIds";
            public const string COL_ANNOUNCEMENT_YEAR = "Year";
            public const string COL_ANNOUNCEMENT_FORSEMISTER = "ForSemester";
            public const string COL_ANNOUNCEMENT_SUBJECTIDS = "SubjectIds";
            public const string COL_ANNOUNCEMENT_PUBLISHDATE = "PublishDate";
            public const string COL_ANNOUNCEMENT_EXPIRYDATE = "ExpiryDate";
            public const string COL_ANNOUNCEMENT_CREATEDBYID = "CreatedById";
            public const string COL_ANNOUNCEMENT_LASTMODIFIEDBYID = "LastModifiedById";
            public const string COL_ANNOUNCEMENT_ISACTIVE = "IsActive";
            public const string COL_ANNOUNCEMENTTITLE = "Title";

        }
        #endregion

        #region AcademicCalendarMaster

        public class AcademicCalendarMaster
        {
            //FIELDS
            public const string COL_ACALENDAR_ID = "AcademicCalId";
            public const string COL_ACALENDAR_TITLE = "Title";
            public const string COL_ACALENDAR_DESCRIPTION = "Description";
            public const string COL_EVENTDATE = "EventDate";
            public const string COL_ISPUBLICHOLIDAY = "IsPublicHoliday";
            public const string COL_ISREMINDERREQUIRED = "IsReminderRequired";
            public const string COL_REMINDERUNIT = "ReminderUnit";
            public const string COL_REMINDERUNITLENGTH = "ReminderUnitLength";
            public const string COL_ACALENDAR_ISACTIVE = "IsActive";
            public const string COL_ACALENDAR_CREATEDBYID = "CreatedById";
            public const string COL_ACALENDAR_LASTMODIFIEDDATE = "LastModifiedDate";
            public const string COL_HIJRI_DAY = "HijriDay";
            public const string COL_HIJRI_MONTH = "HijriMonth";
            public const string COL_HIJRI_YEAR = "HijriYear";


        }
        #endregion

        #region AdmissionSchedule

        public class AdmissionSchedule
        {
            //FIELDS
            public const string COL_ADMISSIONSCHEDULE_ID = "AdmissionScheduleId";
            public const string COL_ACADEMIC_TERM = "AcademicTerm";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_STARTDATE = "StartDate";
            public const string COL_ENDDATE = "EndDate";
            public const string COL_STATUS = "EndStatus";



            public const string COL_REMARKS = "Remarks";
            public const string COL_APPLYFOR = "ApplyFor";

            public const string COL_PROGRAM_NAME = "ProgramName";
            public const string COL_ACADEMIC_TERM_NAME = "AcademicTermName";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_DEGREE_TYPE = "DegreeType";
            public const string COL_ADMISSION_FOR = "AdmissionFor";


        }
        #endregion

        #region Graph

        public class Graph
        {
            public const string COL_DISPLAY_TEXT = "DisplayText";
            public const string COL_COUNT = "Count";
            public const string COL_MAX_COUNT = "MaxCount";
            public const string COL_TOTAL_APPLICATIONS = "TotalApplications";
            public const string COL_TOTAL_NOT_COMPLETED_APPLICATIONS = "TotalNotCompletedApplications";
            public const string COL_TOTAL_COMPLETED_APPLICATIONS = "TotalCompletedApplications";
            public const string COL_TOTAL_ACCEPTED_APPLICATIONS = "TotalAcceptedApplications";
            public const string COL_TOTAL_REJECTED_APPLICATIONS = "TotalRejectedApplications";
            public const string COL_TOTAL_APPROVED_APPLICATIONS = "TotalApprovedApplications";

            public const string COL_ATTENDENCE_P = "P";
            public const string COL_ATTENDENCE_P1 = "P1";
            public const string COL_ATTENDENCE_P2 = "P2";
            public const string COL_ATTENDENCE_UX = "UX";
            public const string COL_ATTENDENCE_X = "X";
            public const string COL_MONTH_NAME = "MonthName";
            public const string COL_MONTH = "Month";
            public const string COL_LEGEND_TEXT = "LegendText";

            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_SCORE = "Score";
            public const string COL_TERM_GPA = "TermGPA";
            public const string COL_TERM_GPA_TILLTERM = "TermGPATillterm";
            public const string COL_TERM_CA = "TermCA";

            public const string COL_STUDENT_DETAILS = "StudentDetails";
            public const string COL_STUDENT_GUID = "StudentGUID";

        }

        #endregion

        #region StudentMaster
        public class StudentMaster
        {

            //FIELDS
            public const string COL_STUD_ID = "StudentID";
            public const string COL_STUD_GUIID = "StudentGUID";
            public const string COL_APPLICATION_ID = "StudentApplicationID";
            public const string COL_STUDENT_APPLICATION_ID = "StudentApplicationID";
            public const string COL_FIRST_NAME = "FirstName";
            public const string COL_MIDDLE_NAME = "MiddleName";
            public const string COL_GRANDFATHER_NAME = "GrandFatherName";
            public const string COL_LAST_NAME = "LastName";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_TERMREGISTRATIONS = "TermRegistrations";
            public const string COL_TERMPERFORMANCE = "TermPerformance";
            public const string COL_ACADEMICADVISORS = "AcademicAdvisors";
            public const string COL_TERMPAYMENTS = "TermPayments";
            public const string COL_PRG_DISPLAY_TEXT = "Programdisplaytext";
            public const string COL_ARFIRST_NAME = "ARFirstName";
            public const string COL_ARMIDDLE_NAME = "ARMiddleName";
            public const string COL_ARGRANDFATHER_NAME = "ARGrandFatherName";
            public const string COL_ARLAST_NAME = "ARLastName";
            public const string COL_DEGREENAME = "DegreeName";
            public const string COL_GENDER = "Gender";
            public const string COL_GENDERTEXT = "GenderText";
            public const string COL_GENDERNAME = "GenderName";
            public const string COL_GENDERRPT = "Gender";
            public const string COL_FIRST_PREFERENCE_NAME = "FirstPreferenceName";
            public const string COL_DATE_OF_JOINING = "DateOfJoining";
            public const string COL_NAME = "Name";
            public const string COL_ARABIC_NAME = "ArabicName";
            public const string COL_MAJOR = "programTitle";
            public const string COL_SEMESTERNAME = "semestername";
            public const string COL_PROGID = "ProgramId";
            public const string COL_SEMESTERID = "SemesterId";
            public const string COL_CRNCODE = "CRN";
            public const string COL_PROGRAMNAME = "ProgramName";
            public const string COL_PROGRAMTITLE = "ProgramTitle";
            public const string COL_PROGRAM_DISPLAY_TEXT = "ProgramDisplayText";
            public const string COL_TERMSEMNAME = "Term";
            public const string COL_TERMNAME = "TermName";
            public const string COL_ACADEMICYEAR = "AcademicYear";
            public const string COL_ACADEMICSEMESTER = "AcademicSemester";
            public const string COL_CLASSROOMTITLE = "ClassRoomTitle";
            public const string COL_STATUS = "Status";
            public const string COL_IS_ACTIVE = "IsActive";
            public const string COL_USER_NAME = "UserName";
            public const string COL_SYSTEM_USER_GUID = "SystemUserGuiD";
            public const string COL_CLASS_SCHEDULE_REGISTRATION_ID = "csRegistrationId";
            public const string COL_CLASS_SCHEDULE_REGISTRATION_STATUS = "RegistrationStatus";
            public const string COL_CLASS_SCHEDULE_REGISTRATION_DATE = "RegistrationDate";
            public const string COL_CLASS_LEVEL = "ClassLevel";
            public const string COL_STUDENT_STATUS = "StudentStatus";
            public const string COL_APPLICATION_DETAILS = "ApplicationDetails";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_APPLICATION_CHECKlIST_STATUS = "ApplicationCheckListStatus";
            public const string COL_CONFIRM_TERM_NAME = "ConfirmTermName";
            public const string COL_CONFIRMATION_TERM_NAME = "ConfirmationTermName";
            public const string COL_CONFIRMATION_DATE = "ConfirmationDate";
            public const string COL_CONFIRMATION_PROGRAM_NAME = "ConfirmationProgramName";
            public const string COL_CONFIRM_PROGRAM_NAME = "ConfirmProgramName";
            public const string COL_CONFIRMATION_TYPE = "ConfirmationType";
            public const string COL_CONFIRMATION_FOR_TERMID = "ConfirmationForTermId";
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_STUDENT_ARABIC_NAME = "StudentArabicName";
            public const string COL_FIRST_PREFERENCE = "FirstPreference";
            public const string COL_SECOND_PREFERENCE = "SecondPreference";
            public const string COL_THIRD_PREFERENCE = "ThirdPreference";
            public const string COL_FORTH_PREFERENCE = "ForthPreference";
            public const string COL_FIFTH_PREFERENCE = "FifthPreference";
            public const string COL_CONFIRMED_TERM_NAME = "StudentConfirmedTerm";
            public const string COL_CONFIRMED_PROGRAM_NAME = "StudentConfirmedProgramName";

            public const string COL_FIRST_PREFERENCENAME = "FirstPreferenceName";
            public const string COL_SECOND_PREFERENCENAME = "SecondPreferenceName";
            public const string COL_THIRD_PREFERENCENAME = "ThirdPreferenceName";
            public const string COL_FORTH_PREFERENCENAME = "ForthPreferenceName";
            public const string COL_FIFTH_PREFERENCENAME = "FifthPreferenceName";
            public const string COL_EMAIL_ID = "EmailId";
            public const string COL_MACHS_EMAIL_ID = "MachsEmailId";
            public const string COL_STUDENTPROGRAM_REGISTRATION_ID = "StudentProgramRegistrationId";
            public const string COL_STUDENT_APPLICATION_TERM_NAME = "StudentAcademicTerm";
            public const string COL_NATIONALITY = "Nationality";
            public const string COL_NATIONAL_ID = "NationalId";
            public const string COL_LAST_MODIFIED_BY_ID = "LastModifiedById";
            public const string COL_LAST_MODIFIED_DATE = "LastModifiedDate";
            public const string COL_COURSE_TYPE = "CourseType";
            public const string COL_PASSWORD = "Password";
            public const string COL_CUMULATIVE_GPA = "CummulativeGPA";
            public const string COL_CUMULATIVE_AVG = "CummulativeAverage";
            public const string COL_GRADE_CLASS = "GradeClass";
            public const string COL_RESULT_STATUS = "ResultStatus";
            public const string COL_COMPLETION_DATE = "CompletionDate";
            public const string COL_DISTINCTIONCLASS = "DistinctionClass";
            public const string COL_DISTINCTIONCLASSTEXT = "DistinctionClassText";

            public const string COL_RANK_LEVEL_SECURED = "DistinctionClassText";
            public const string COL_SIS_LOGIN_STATUS = "Status";
            public const string COL_PROMOTEDPROGRAM_NAME = "promotedProgramName";
            public const string COL_PROMOTEDTERM_NAME = "PromotedTerm";
            public const string COL_ISDEGREECOMPLETED = "IsdegreeCompleted";
            public const string COL_ADMISSIONDATE = "AdmissionDate";
            public const string COL_JOININGDATE = "JoiningDate";
            public const string COL_OTHERDETAILS = "OtherDetails";
            public const string COL_GRADUATED_DATE = "GraduatedDate";

            public const string COL_NUMBER_OF_REGISTERED_COURSES = "NoOfRegisteredCourses";

            //kiran
            public const string COL_OTHER_DETAILS = "Otherdetails";
            public const string COL_ELIGIBLESEMESTERID = "EligibleSemesterId";

            public const string COL_ISCURRENTTERM = "IsCurrentTerm";
        }
        #endregion

        #region FormApplicationMaster

        public class FormApplicationMaster
        {
            //FIELDS
            public const string COL_FORM_ID = "FormId";
            public const string COL_DEGREENAME = "DegreeName";
            public const string COL_CATEGORYID = "CategoryId";
            public const string COL_FORMTITLE = "FormTitle";
            public const string COL_FILE_NAME = "FileName";
            public const string COL_CATEGORYNAME = "FormCategoryName";
            public const string COL_FILE_EXT = "FileExtension";

        }
        #endregion

        #region FormCategory

        public class FormCategory
        {
            //FIELDS
            public const string COL_FORM_APPCATEGORYID = "FormAppCategoryId";

            public const string COL_CATEGORYTITLE = "CategoryTitle";



        }
        #endregion

        #region CapacityPlanningDetails
        public class CapacityPlanningDetails
        {
            //FIELDS
            public const string COL_CAPACITYPLANNINGDETAILS_ID = "CapacityPlanningDetailsId";
            public const string COL_CAPACITYPLANNING_ID = "CapacityPlanningId";
            public const string COL_SECTIONNAME = "SectionName";
            public const string COL_CLASSROOM_ID = "ClassRoomId";
            public const string COL_CAPACITY = "Capacity";
            public const string COL_CRN = "CRN";

        }
        #endregion

        #region CapacityPlanningMaster
        public class CapacityPlanningMaster
        {
            //FIELDS
            public const string COL_CAPACITYPLANNING_ID = "CapacityPlanningId";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_ACADEMIC_SEM = "AcademicSem";
            public const string COL_TERM_YEAR = "TermYear";
            public const string COL_TERM_SEM = "TermSem";
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_PROGRAM_TITLE = "ProgramTitle";
            public const string COL_SEM_ID = "SemesterId";
            public const string COL_CRN_ID = "CRNId";
            public const string COL_TOTAL_SEATS = "TotalSeats";
            public const string COL_TOTAL_ALLOTMENT = "TotalAllotment";
            public const string COL_CRN = "CRN";
        }
        #endregion

        #region ClassRoomMaster
        public class ClassRoomMaster
        {
            //FIELDS
            public const string COL_CLASSROOM_ID = "ClassRoomId";
            public const string COL_CLASSROOM_TITLE = "ClassRoomTitle";
            public const string COL_CLASSROOM_DESCRIPTION = "ClassRoomDescription";
            public const string COL_CLASSROOM_CAPACITY = "Capacity";
            public const string COL_CREATEDBY = "CreatedById";
            public const string COL_DATECREATED = "DateCreated";
            public const string COL_LASTMODIFIEDID = "LastModifiedById";
            public const string COL_LASTMODIFIEDDATE = "LastModifiedDate";

        }
        #endregion

        #region StudentProgramRegistrationDetails

        public class StudentProgramRegistrationDetails
        {
            /// COLUMNS
            public const string COL_STUDENT_PROGRAM_REGISTRATION_ID = "StudentProgramRegistrationId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_PROGRAM_YEAR_ID = "ProgramYearId";
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_ACADEMIC_SEMESTER = "AcademicSemester";
            public const string COL_IS_CURRENT_DETAILS = "IsCurrentDetails";
            public const string COL_ENROLLMENTS_SEQUENCE = "EnrollmentsSequence";
            public const string COL_CRN_ID = "CRNId";
            public const string COL_STUDENT_COUNT = "StudentCount";
            public const string COL_GRADUATIONCOMPLETION_ID = "GraduationCompletionId";

        }
        #endregion

        #region CRNMaster
        public class CRNMaster
        {
            //FIELDS
            public const string COL_CRN_ID = "CRNId";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_ACADEMIC_SEM = "AcademicSem";
            public const string COL_TERM_YEAR = "TermYear";
            public const string COL_TERM_SEM = "TermSem";
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_SECTION_ID = "SectionId";
            public const string COL_CRN = "CRN";
            public const string COL_EMPLOYEE_ID = "EmployeeId";
            public const string COL_ROLE_ID = "RoleId";
            public const string COL_PROGRAM_DISPLAY_TEXT = "ProgramDisplayText";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_SECTION_NAME = "SectionName";
            public const string COL_CLASSROOM = "ClassRoom";
            public const string COL_CAPACITY = "capacity";
            public const string COL_STATUS = "Status";
            public const string COL_TERM_SEM_NAME = "TermSemName";

        }
        #endregion

        #region TermMaster
        public class TermMaster
        {
            //FIELDS
            public const string COL_TERM_ID = "TermId";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_ACADEMIC_SEM = "AcademicSem";
            public const string COL_TERM_YEAR = "TermYear";
            public const string COL_TERM_SEM = "TermSem";
            public const string COL_TERM_SEM_NAME = "TermSemName";
            public const string COL_STARTDATE = "StartDate";
            public const string COL_ENDDATE = "EndDate";
            public const string COL_TERMDISPLAY_TEXT = "TermDisplayText";
            public const string COL_TERM_CODE = "TermCode";
            public const string COL_ISACTIVE = "IsActive";
            public const string COL_STATUS = "TermStatus";
            public const string COL_TERM_NAME = "TermName";

            public const string COL_TERM_REGISTRATION_STATUS_FOR_REGISTER = "IsRegisterScheduleOpen";
            public const string COL_TERM_REGISTRATION_STATUS_FOR_ADDDROP = "IsAddDropScheduleOpen";
            public const string COL_TERM_REGISTRATION_STATUS_WITHDRAW = "IsWithdrawScheduleOpen";

            public const string COL_TERM = "Term";
            public const string COL_DURATION = "Duration";
            public const string COL_LASTMODIFIEDBY_ID = "LastModifiedById";
            public const string COL_LASTMODIFIED_DATE = "LastModifiedDate";
            public const string COL_LASTMODIFIED_NAME = "LastModifiedName";
            public const string COL_ACADEMIC_START_YEAR = "AcademicStartYear";
            public const string COL_LASTMODIFIED_BY_NAME = "LastModifiedByName";

            public const string COL_ACTIVESTATUS = "ActiveStatus";
        }
        #endregion

        #region AssignmentMaster
        public class AssignmentMaster
        {
            //FIELDS
            public const string COL_ASSIGNMENT_ID = "AssignmentID";
            public const string COL_ASSIGNMENT_TITLE = "AssignmentTitle";
            public const string COL_ASSIGNMENT_DESCRIPTION = "AssignmentDescription";
            public const string COL_UPLOADED_FILE_PATH = "UploadedFilePath";
            public const string COL_VALID_FROM_SEMESTER = "ValidFromSemester";
            public const string COL_VALID_FROM_YEAR = "ValidFromYear";
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_STATUS = "Status";
            public const string COL_APPROVAL_STATUS = "ApprovalStatus";
            public const string COL_CREATED_BY_ID = "CreatedById";
            public const string COL_ALLOTMENT_COUNT = "Allotmentcount";
            public const string COL_ASSIGNMENT_MODE = "AssignmentMode";
            public const string COL_ASSESSMENT_ID = "AssessmentId";
            public const string COL_SCORE = "Score";
            public const string COL_TOTALSCORE = "TotalScore";
            public const string COL_CREATEDDATE = "DateCreated";
            public const string COL_COMPLETEDDATE = "CompletatedDate";
        }
        #endregion

        #region AssignmentAllotmentMaster
        public class AssignmentAllotmentMaster
        {
            //FIELDS
            public const string COL_ALLOTMENT_ID = "AllotmentId";
            public const string COL_ASSIGNMENT_ID = "AssignmentId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_STUDENT_ID = "StudentID";
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_STARTDATE = "StartDate";
            public const string COL_DUEDATE = "DueDate";
            public const string COL_EXPIRYDATE = "ExpiryDate";
            public const string COL_ISREMINDER_REQUIRED = "IsReminderRequired";
            public const string COL_REMINDERUNIT = "ReminderUnit";
            public const string COL_SURVEY_ALLOTMENT_ID = "SurveyAllotmentId";

            public const string COL_REMINDER_UNIT_LENGTH = "ReminderUnitLength";
            public const string COL_REMINDER_RELATIVE_DATESET = "ReminderRelativeDateset";
            public const string COL_COMPLETION_STATUS = "CompletionStatus";
            public const string COL_ASSIGNMENT_TITLE = "AssignmentTitle";
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_COURSE_NAME = "CourseTitle";

            public const string COL_OVERDUE = "IsOverDue";
            public const string COL_EXPIRED = "IsExpired";
            public const string COL_ASSIGNMENTSTATUS = "AssignmentStatus";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_EMAILID = "EmailId";

        }
        #endregion

        #region ProgramSeatsMaster

        public class ProgramSeatsMaster
        {
            //COLUMNS
            public const string COL_PROGRAM_SEATAS_PROGRAMSEATSID = "ProgramSeatsId";
            public const string COL_PROGRAM_SEATAS_PROGRAMID = "ProgramId";
            public const string COL_PROGRAM_SEATAS_ACADEMICTERM = "AcademicTerm";
            public const string COL_PROGRAM_SEATAS_ACADEMICYEAR = "AcademicYear";
            public const string COL_PROGRAM_SEATAS_SEATS = "Seats";
            public const string COL_PROGRAM_SEATAS_STATUS = "Status";
        }
        #endregion

        #region ExamTimeTable
        public class ExamTimeTable
        {
            //COLUMNS
            public const string COL_EXAM_SCHEDULE_ID = "ExamScheduleId";
            public const string COL_EXAM_SCHEDULE_CRN_ID = "CRNId";
            public const string COL_EXAM_SCHEDULE_COURSE_ID = "CourseID";
            public const string COL_EXAM_SCHEDULE_EXAM_TYPE = "ExamType";
            public const string COL_EXAM_SCHEDULE_CLASS_ROOM_ID = "ClassRoomId";
            public const string COL_EXAM_SCHEDULE_EXAM_DATE = "ExamDate";
            public const string COL_EXAM_SCHEDULE_EXAM_DATE_WITHFORMAT = "ExamDate_WithFormat";
            public const string COL_EXAM_SCHEDULE_START_TIME = "StartTime";
            public const string COL_EXAM_SCHEDULE_END_TIME = "EndTime";
            public const string COL_EXAM_SCHEDULE_PROCTORS_ID = "ProctorsId";
            public const string COL_EXAM_SCHEDULE_IS_REMINDER_REQUIRED = "IsReminderRequired";
            public const string COL_EXAM_SCHEDULE_REMINDER_UNIT = "ReminderUnit";
            public const string COL_EXAM_SCHEDULE_REMINDER_UNIT_LENGTH = "ReminderUnitLength";
            public const string COL_EXAM_TYPE_TITLE = "ExamTypeTitle";
            public const string COL_EXAM_TYPE_DISPLAY_START_TIME = "StartTimeDisplay";
            public const string COL_EXAM_TYPE_DISPLAY_END_TIME = "EndTimeDisplay";
            public const string COL_COURSE_CREDITS = "CourseCredits";
            public const string COL_EXAM_STARTTIME = "ExamStartTime";
            public const string COL_EXAM_DAY = "Day";
            public const string COL_EXAM_CLASSROOM = "ClassRoom";
            public const string COL_EXAM_COURSE_TITLE = "CourseTitle";
            public const string COL_EXAM_COURSE_GROUP_NAME = "CourseGroupName";
            public const string COL_EXAM_SCHEDULE_EXAM_TYPE_TITLE = "ExamTypeTitle";
            public const string COL_EXAM_SCHEDULE_EXAM_START_TIME = "ExamStartTime";
            public const string COL_EXAM_CLASSROOM_WITH_STUDENTS = "ClassRoomWithStudents";
            public const string COL_EXAM_SCHEDULE_PROCTOR = "Proctors";
            public const string COL_EXAM_SCHEDULE_SERIAL_NO = "Serial No.";
            public const string COL_EXAM_CLASSROOM_TITLE = "ClassRoomTitle";
            public const string COL_TEACHERNAME = "TeacherName";
            public const string COL_TEACHERID = "TeacherId";
            public const string COL_ALLOCATE = "Allocate";
        }
        #endregion

        #region PlacementTestResult
        public class PlacementTestResult
        {
            //COLUMNS
            public const string COL_PLACEMENTTEST_ID = "PlacementTestResultId";
            public const string COL_STUDENT_ID = "StudentId";
            public const string COL_STUDENT_APPLICATION_ID = "StudentApplicationID";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_GRANDFATHERNAME = "GrandFatherName";
            public const string COL_DOB = "DateOfBirth";
            public const string COL_MOBILE_COUNTRYCODE = "MobileCountryCode";
            public const string COL_MOBILE_NO = "MobileNo";
            public const string COL_PLACEMENT_TESTSCORE = "PlacementTestScore";
            public const string COL_PLACEMENT_TESTRESULT = "PlacementTestResult";
            public const string COL_PROGRAM_TITLE = "ProgramTitle";
            public const string COL_ADMISSION_ON = "AdmissionOn";
            public const string COL_TERMSEMNAME = "AcademicYearName";
            public const string COL_ACADEMICYEAR = "AcademicYear";
            public const string COL_GENDERTEXT = "GenderText";
            public const string COL_TERMID = "TermId";
        }
        #endregion

        #region StudentAttendence
        public class StudentAttendence
        {
            //FIELDS
            public const string COL_STUDENT_ATTENDENCE_ID = "AttendenceId";
            public const string COL_CLASS_TIMETABLE_ID = "ClassTimeTableId";
            public const string COL_CRN_ID = "CRNId";
            public const string COL_SECTION_ID = "SectionId";
            public const string COL_STUDENTGUID = "StudentGUID";
            public const string COL_STUDENTID = "StudentID";
            public const string COL_ISPRESENT = "IsPresent";
            public const string COL_ISABSENT = "IsAbsent";
            public const string COL_ISONLEAVE = "IsOnLeave";
            public const string COL_STUDENTNAME = "FirstName";
            public const string COL_MOBILENUMBER = "MobileNo";
            public const string COL_STATUS = "Status";

            public const string COL_COURSEID = "CourseId";
            public const string COL_COURSECODE = "CourseCode";
            public const string COL_COURSETITLE = "CourseTitle";
            public const string COL_CREDITS = "Credits";
            public const string COL_TERMHOURS = "TotalTermHrs";
            public const string COL_COMPLETEDHOURS = "TotalTermCompletedHrs";
            public const string COL_ATTENDEDHOURS = "TotalTermAttendedHrs";
            public const string COL_ATTENDANCEPERCENT = "Percentage";



        }
        #endregion

        #region ExamAttendence
        public class ExamAttendence
        {
            public const string COL_EXAM_ATTENDENCE_ID = "ExamAttendenceId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_CLASSROOM_ID = "ClassRoomId";
            public const string COL_EXAMDATE = "ExamDate";
            public const string COL_STARTTIME = "StartTime";
            public const string COL_ENDTIME = "EndTime";
            public const string COL_STATUS = "Status";
            public const string COL_EXAMSCHEDULE_ID = "ExamScheduleId";
            public const string COL_ALLOCATEDSTUDENTS = "TotalAllocatedStudents";
            public const string COL_CRS_SCHEDULED = "CoursesScheduled";
            public const string COL_ATTENDEDSTUDENTS = "TotalAttendedStudents";
            public const string COL_ABSENTSTUDENTS = "TotalAbsentStudents";
            public const string COL_STUDENT_ID = "StudentID";
            public const string COL_STUDENT_DETAILS = "FirstName";
            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_TIMESLOT = "TimeSlot";

            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_PRGTEXT = "ProgramDisplayText";
            public const string COL_SEMESTERNAME = "SemesterName";

        }
        #endregion

        #region ProgramCurriculumMaster

        public class ProgramCurriculumMaster
        {
            public const string COL_PROGRAM_CURRICULUM_ID = "CurriculumPlanId";
            public const string COL_ACADEMIC_YEAR = "AcademicYear";
            public const string COL_DEGREE_TYPE = "DegreeType";
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_PROGRAM_YEAR_ID = "ProgramYearId";
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_DURATION_IN_WEEKS = "DurationInWeeks";
            public const string COL_APPROVAL_STATUS = "ApprovalStatus";
            public const string COL_MINIMUM_CREDITS = "MinimumCredits";
            public const string COL_MAXIMUM_CREDITS = "MaximumCredits";

            public const string COL_PROGRAM_NAME = "ProgramDisplayText";
            public const string COL_PROGRAM_TERM = "AcademicYear";
            public const string COL_PROGRAM_STATUS = "ApprovalStatus";
            public const string COL_PROGRAM_ISUSED = "IsUsed";
        }
        #endregion

        #region StudentLeaves
        public class StudentLeaves
        {
            // Fields
            public const string COL_STUDENT_LEAVE_ID = "StudentLeaveId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_REASON = "Reason";
            public const string COL_REASONNAME = "ReasonName";
            public const string COL_REASON_DESCRIPTION = "ReasonDescription";
            public const string COL_FROM_DATE = "FromDate";
            public const string COL_TO_DATE = "ToDate";
            public const string COL_NO_OF_DAYS = "NoOfDays";
            public const string COL_STATUS = "Status";

            public const string COL_STUDENTID = "StudentId";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_FACULTYCHGLEAVESTATUS = "IsFacultyChangeLeaveStatus";
            public const string COL_VIEW = "View";
        }
        #endregion

        #region LibraryItems
        public class LibraryItems
        {
            // Fields
            public const string COL_LIBRARYITEM_ID = "LibraryItemId";
            public const string COL_ITEM_NO = "ItemNo";
            public const string COL_TITLE = "Title";
            public const string COL_AUTHOR = "Author";
            public const string COL_EDITOR = "Editor";
            public const string COL_PUBLISHER = "Publisher";
            public const string COL_SUBJECT = "Subject";
            public const string COL_FORMAT = "OMFormat";
            public const string COL_SRNO = "srno";
        }
        #endregion

        #region StudentLeavesStatus
        public class StudentLeavesStatus
        {
            // Fields
            public const string COL_STUDENT_LEAVESTATUS_ID = "StudentLeaveId";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";
            public const string COL_FACULTYNAME = "FacultyName";

        }
        #endregion

        #region EmployeeLeavesStatus
        public class EmployeeLeavesStatus
        {
            // Fields
            public const string COL_EMPLOYEE_LEAVESTATUS_ID = "EmployeeLeaveId";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";
            public const string COL_HODNAME = "HodName";

        }
        #endregion

        #region EmployeeLeaves
        public class EmployeeLeaves
        {
            // Fields
            public const string COL_EMPLOYEE_LEAVE_ID = "EmployeeLeaveId";
            public const string COL_EMPLOYEE_GUID = "EmployeeGUID";
            public const string COL_REASON = "Reason";
            public const string COL_REASONNAME = "ReasonName";
            public const string COL_REASON_DESCRIPTION = "ReasonDescription";
            public const string COL_FROM_DATE = "FromDate";
            public const string COL_TO_DATE = "ToDate";
            public const string COL_NO_OF_DAYS = "NoOfDays";
            public const string COL_STATUS = "Status";

            public const string COL_EMPLOYEEID = "EmployeeId";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_COUNT = "StatusCount";
        }
        #endregion

        #region AdminRole
        public class AdminRole
        {
            //FIELDS
            public const string COL_ROLE_ID = "RoleId";
            public const string COL_ROLE_NAME = "RoleName";
            public const string COL_ROLE_DESC = "RoleDescription";
            public const string COL_IS_ACTIVE = "IsActive";
            public const string COL_IS_DEFAULT = "IsDefault";
            public const string COL_ROLE_TYPE = "RoleType";
            public const string COL_CREATED_BY_ID = "CreatedById";
            public const string COL_CREATED_BY_Name = "CreatedByName";
            public const string COL_ID = "RoleId";
            public const string COL_TOTAL_COUNT = "TotalCount";
            public const string COL_PARENT_ROLE_TITLE = "ParentRoleTitle";
        }
        #endregion

        #region EmployeeMaster

        public class EmployeeMaster
        {
            //COLUMNS
            public const string COL_EMPLOYEE_MASTER_EMPLOYEE_GUID = "EmployeeGUID";
            public const string COL_EMPLOYEE_MASTER_EMPLOYEE_ID = "EmployeeId";
            public const string COL_EMPLOYEE_MASTER_PREFIX = "Prefix";
            public const string COL_EMPLOYEE_MASTER_FIRST_NAME = "FirstName";
            public const string COL_EMPLOYEE_MASTER_MIDDLE_NAME = "MiddleName";
            public const string COL_EMPLOYEE_MASTER_GRANDFATHER_NAME = "GrandFatherName";
            public const string COL_EMPLOYEE_MASTER_LASTNAME = "LastName";
            public const string COL_EMPLOYEE_MASTER_AR_FIRST_NAME = "ARFirstName";
            public const string COL_EMPLOYEE_MASTER_AR_MIDDLE_NAME = "ARMiddleName";
            public const string COL_EMPLOYEE_MASTER_AR_GRANDFATHER_NAME = "ARGrandFatherName";
            public const string COL_EMPLOYEE_MASTER_AR_LAST_NAME = "ARLastName";
            public const string COL_EMPLOYEE_MASTER_SPONSOR_NAME = "SponsorName";
            public const string COL_EMPLOYEE_MASTER_SPONSOR_ADDRESS = "SponsorAddress";
            public const string COL_EMPLOYEE_MASTER_LOCAL_ADDRESS = "LocalAddress";
            public const string COL_EMPLOYEE_MASTER_INTERNATIONAL_ADDRESS = "InternationalAddress";
            public const string COL_EMPLOYEE_MASTER_IDENTIFICATION = "Identification";
            public const string COL_EMPLOYEE_MASTER_NATIONALID = "NationalId";
            public const string COL_EMPLOYEE_MASTER_PASSPORT_IQAMANO = "Passport_IqamaNo";
            public const string COL_EMPLOYEE_MASTER_PASSPORT_IQAMA_EXPIRY_DATE = "Passport_IqamaExpiryDate";
            public const string COL_EMPLOYEE_MASTER_IQAMA_ISSUE_DATE = "Passport_IqamaIssueDate";
            public const string COL_EMPLOYEE_MASTER_IQAMA_ISSUE_PLACE = "Passport_IqamaIssuePlace";
            public const string COL_EMPLOYEE_MASTER_IQAMA_TYPE = "IqamaType";
            public const string COL_EMPLOYEE_MASTER_SPONSOR_MOBILE_NO = "SponsorMobileNo";
            public const string COL_EMPLOYEE_MASTER_SPONSOR_COUNTRY_CODE = "SponsorCountryCode";
            public const string COL_EMPLOYEE_MASTER_NATIONALITY = "Nationality";
            public const string COL_EMPLOYEE_MASTER_PHOTOGRAPH = "Photograph";
            public const string COL_EMPLOYEE_MASTER_GNDER = "Gender";
            public const string COL_EMPLOYEE_MASTER_DATE_OF_BIRTH = "DateOfBirth";
            public const string COL_EMPLOYEE_MASTER_PLACE_OF_BIRTH = "PlaceOfBirth";
            public const string COL_EMPLOYEE_MASTER_MARTIAL_STATUS = "MaritalStatus";
            public const string COL_EMPLOYEE_MASTER_RELIGION = "Religion";
            public const string COL_EMPLOYEE_MASTER_EMAIL_ID = "EmailId";
            public const string COL_EMPLOYEE_MASTER_PERSONAL_EMAIL_ID = "PersonalEmailId";
            public const string COL_EMPLOYEE_MASTER_EMPLOYEE_ID_NUMBER = "EmployeeIDNumber";
            public const string COL_EMPLOYEE_MASTER_IBAN_NUMBER = "IBANNumber";
            public const string COL_EMPLOYEE_MASTER_POSITION = "Position";
            public const string COL_EMPLOYEE_MASTER_CONTRACT_NUMBER = "ContractNumber";
            public const string COL_EMPLOYEE_MASTER_CONTRACT_START_DATE = "ContractStartDate";
            public const string COL_EMPLOYEE_MASTER_CONTRACT_EXPIRY_DATE = "ContractExpiryDate";
            public const string COL_EMPLOYEE_MASTER_DEPARTMENT = "Department";
            public const string COL_EMPLOYEE_MASTER_CONTRACT_TYPE = "ContractType";
            public const string COL_EMPLOYEE_MASTER_JOB_DESCRIPTIONS = "JobDescriptions";
            public const string COL_EMPLOYEE_MASTER_BASIC_SALARY = "BasicSalary";
            public const string COL_EMPLOYEE_MASTER_HOUSING = "Housing";
            public const string COL_EMPLOYEE_MASTER_TRANSPORT = "Transport";
            public const string COL_EMPLOYEE_MASTER_CALL_ALLOWANCE = "CallAllowance";
            public const string COL_EMPLOYEE_MASTER_OTHER_ALLOWANCE = "OtherAllowance";
            public const string COL_EMPLOYEE_MASTER_VACATION = "Vacation";
            public const string COL_EMPLOYEE_MASTER_WARNING = "Warning";
            public const string COL_EMPLOYEE_MASTER_CATEGORY = "Category";
            public const string COL_EMPLOYEE_MASTER_COURSEID = "CourseId";
            public const string COL_EMPLOYEE_MASTER_ROLEID = "RoleId";
            public const string COL_EMPLOYEE_MASTER_USERNAME = "UserName";
            public const string COL_EMPLOYEE_MASTER_PASSWORD = "Password";

            public const string COL_EMPLOYEE_MASTER_ROLE_NAME = "RoleName";
            public const string COL_EMPLOYEE_MASTER_FAMILYID = "FamilyID";
            public const string COL_EMPLOYEE_MASTER_LCOUNTRYID = "LCountryId";
            public const string COL_EMPLOYEE_MASTER_LPROVINCEID = "LProvinceId";
            public const string COL_EMPLOYEE_MASTER_LCITYID = "LCityId";
            public const string COL_EMPLOYEE_MASTER_LZIPCODE = "LZipCode";
            public const string COL_EMPLOYEE_MASTER_LPOBOX = "LPOBox";
            public const string COL_EMPLOYEE_MASTER_LADDRESS = "LAddress";
            public const string COL_EMPLOYEE_MASTER_MOBILECOUNTRYCODE = "MobileCountryCode";
            public const string COL_EMPLOYEE_MASTER_MOBILENO = "MobileNo";
            public const string COL_EMPLOYEE_MASTER_HOMETELEPHONENO = "HomeTelephoneNo";
            public const string COL_EMPLOYEE_MASTER_PROGRAMIDS = "ProgramIDs";
            public const string COL_EMPLOYEE_MASTER_COURSEIDS = "CourseIDs";
            public const string COL_EMPLOYEE_MASTER_CRNID = "crnid";
            public const string COL_EMPLOYEE_MASTER_TITLE = "Title";
            public const string COL_EMPLOYEE_MASTER_DEPARTMENT_NAME = "DepartmentName";
            public const string COL_EMPLOYEEDETAILS = "EmployeeDetail";

        }
        #endregion

        #region  ManageStudentFunding
        public class ManageFundingType
        {
            public const string COL_STUDENTSPONSOR_GUID = "SponsorGUID";
            public const string COL_APPROVALSTATUS = "ApprovalStatus";
            public const string COL_FUNDINGTYPE = "FundingType";
            public const string COL_COMMENTS = "Comments";
            public const string COL_ATTENDENCE = "Attendence";
            public const string COL_INTENDEDAMOUNT = "IntendedAmount";

            public const string COL_STUDENTSPONSORS_FUNDING_AMOUNT_PER_MONTH = "FundingAmtPerMonth";
            public const string COL_STUDENTSPONSORS_FUNDING_CONTRACT_NO = "FundingContractNo";
            public const string COL_STUDENTSPONSORS_FUNDING_CONTRACT_DATE = "FundingContractDate";
            public const string COL_STUDENTSPONSORS_FUNDING_CONTRACT_BATCH = "FundingContractBatch";
            public const string COL_STUDENTSPONSORS_FUNDING_PERIOD_IN_MONTHS = "FundingPeriodInMonths";

            public const string COL_STUDENTSPONSORS_STUDENTID = "StudentId";
            public const string COL_STUDENTSPONSORS_STUDENTGUID = "StudentGUId";

            public const string COL_STUDENTSPONSORS_APPROVALSTATUS = "ApprovedStaus";
            public const string COL_STUDENTSPONSORS_FIRSTNAME = "FirstName";
            public const string COL_STUDENTSPONSORS_LASTNAME = "LastName";
            public const string COL_STUDENTSPONSORS_MIDDLENAME = "MiddleName";
        }
        #endregion

        #region EmployeeContractDetails
        public class EmployeeContractDetails
        {
            //COLUMNS
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_ID = "ContractualDetailID";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_EMP_GUID = "EmployeeGUID";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_CONTRACTTYPE = "ContractType";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_CONTRACTNUMBER = "ContractNumber";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_CONTRACTSTARTDATE = "ContractStartDate";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_CONTRACTEXPIRYDATE = "ContractExpiryDate";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_DEPARTMENT = "Department";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_POSITION = "Position";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_JOBDESCRIPTION = "JobDescription";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_BASICSALARY = "BasicSalary";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_HOUSING = "Housing";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_TRANSPORT = "Transport";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_CALLALLOWANCES = "CallAllowance";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_OTHERALLOWANCES = "OtherAllowance";
            public const string COL_EMPLOYEE_CONTRACT_DETAILS_VACATIONALLOWED = "VacationAllowed";
            public const string COL_CONTRACT_DETAILS_WARNINGALLOWED = "WarningAllowed";
            public const string COL_DETAILS = "Details";
        }
        #endregion

        #region StudentPayments
        public class StudentPayments
        {
            //FIELDS
            public const string COL_STUD_ID = "StudentID";
            public const string COL_PAYMENT_DATE = "PaymentDate";
            public const string COL_INVOICENO = "InvoiceNo";
            public const string COL_PAYMENT_MODE = "PaymentMode";
            public const string COL_TOTALRECEIVED_AMT = "TotalReceivedAmt";
            public const string COL_DETAILS = "Details";
            public const string COL_STUDENT_FEEPAYMENT_ID = "StudentFeePaymentId";
            public const string COL_SPONSOR_FEEPAYMENT_ID = "SponsorFeePaymentId";
            public const string COL_BANK_NAME = "BankName";
            public const string COL_BANK_ADDRESS = "BankAddress";
            public const string COL_IBAN = "IBAN";
            public const string COL_REFERENCE = "Reference";
            public const string COL_ORNUMBER = "ORNumber";
            public const string COL_TERM = "Term";
            public const string COL_PREVIEW = "Preview";
            public const string COL_AMOUNTITEMS = "AmountItems";
            public const string COL_TRAN_DETAILS = "TranDetails";
            public const string COL_RECEIPTNO = "ReceiptNO";
            public const string COL_TOTALSTUDENT = "NoOfStudents";
            public const string COL_LATEFEE = "LateFee";
            public const string COL_DUEDATE = "DueDate";
            public const string COL_PROGRAMID = "ProgramId";
            public const string COL_STUDENT_INVOICE_ID = "StudentInvoiceId";
            public const string COL_APPLICATIONFEE = "ApplicationFee";
            public const string COL_PAYMENTSTATUS = "PaymentStatus";
            public const string COL_STUDENTNAME = "StudentName";
            public const string COL_STUDENT_ACADEMIC_DETAILS = "StudentAcademicDetails";
            public const string COL_STUDENT_GUID = "StudentGUID";
        }
        #endregion

        #region Invoice
        public class Invoice
        {
            //COLUMNS
            public const string COL_PROGRAM_ID = "ProgramId";
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_TERM_SEM = "TermSem";
            public const string COL_TERM_YEAR = "TermYear";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_FIRST_NAME = "FirstName";
            public const string COL_LAST_NAME = "LastName";
            public const string COL_DATE_OF_BIRTH = "DateOfBirth";
            public const string COL_MOBILE_NO = "MobileNo";
            public const string COL_PROGRAM_DISPLAY_TEXT = "ProgramDisplayText";
            public const string COL_STUDENT_ACADEMIC_YEAR = "StudentAcademicYear";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_SECTION_NAME = "SectionName";
            public const string COL_PROGRAM_FEE_ID = "ProgramFeeID";
            public const string COL_FEE_SOURCE_ID = "FeeSourceID";
            public const string COL_FEE_SOURCE_CODE = "FeeSourceCode";
            public const string COL_FEE_SOURCE_NAME = "FeeSourceName";
            public const string COL_AMOUNT = "Amount";
            public const string COL_TOTAL_FEE = "TotalFee";
            public const string COL_LATE_FEE = "LateFee";
            public const string COL_TOTAL_INVOICE_AMT_TEXT = "TotalInvoiceAmtText";
            public const string COL_INVOICE_DATE = "InvoiceDate";
            public const string COL_INVOICE_NO = "InvoiceNo";
            public const string COL_SPONSOR_NAME = "SponsorName";
            public const string COL_SPONSOR_ID = "SponsorId";
            public const string COL_STUDENT_INVOICE_DETAIL_ID = "StudentInvoiceDetailId";
            public const string COL_INVOICE_DUE_DATE = "DueDate";
            public const string COL_IS_REMINDER_REQUIRED = "IsReminderRequired";
            public const string COL_REMINDER_UNIT = "ReminderUnit";
            public const string COL_REMINDER_UNIT_LENGTH = "ReminderUnitLength";
            public const string COL_IS_STUDENT_REMINDER_REQUIRED = "IsStudentReminderRequired";
            public const string COL_IS_HR_REMINDER_REQUIRED = "IsHRReminderRequired";
            public const string COL_PENDING_AMOUNT = "PendingAmount";
            public const string COL_EMAIL_ID = "EmailId";
            public const string COL_SEQUENCEORDER = "SequenceOrder";
            public const string COL_ISTERMEXPIRED = "IsTermExpired";
        }
        #endregion

        #region StudentAdvisorRequests
        public class StudentAdvisorRequests
        {
            //FIELDS
            public const string COL_STUDENTADVISORREQUESTID = "StudentAdvisorRequestId";
            public const string COL_ADVISORID = "AdvisorId";
            public const string COL_PROGRAMID = "ProgramId";
            public const string COL_SEMESTERID = "SemesterId";
            public const string COL_STUDENTGUID = "StudentGUID";
            public const string COL_POSTEDDATEFROM = "DateCreated";
            public const string COL_POSTEDDATETO = "PostedDateTo";
            public const string COL_TITLE = "Title";
            public const string COL_DESCRIPTION = "Description";
            public const string COL_ADVISORCOMMENTS = "AdvisorComments";
            public const string COL_APPROVEDDATE = "ApprovedDate";
            public const string COL_CATEGORY = "Category";
            public const string COL_STATUS = "Status";
            public const string COL_ADVISORNAME = "AdvisorName";
            public const string COL_ADVISOR_ID = "Advisor ID";
            public const string COL_COMMENTS = "Comments";
            public const string COL_STUDENTADVISORS_TERMSEM = "TermSem";
            public const string COL_STUDENTADVISORS_TERMYEAR = "TermYear";
            public const string COL_STUDENTADVISORS_ID = "StudentAdvisorId";
            public const string COL_STUDENTAPPLICATIONID = "StudentApplicationId";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_GRANDFATHER_NAME = "GrandFatherName";
            public const string COL_STUDENT_ID = "StudentID";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_APPLICATIONID = "StudentApplicationID";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_STUDENTNAME = "StudentName";
            public const string COL_ACADEMIC_DETAILS = "AcademicDetails";
            public const string COL_ASSIGNED_TERM = "TermName";
            public const string COL_ASSIGNED_TERM_Code = "TermCode";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_PROGRAM_NAME = "ProgramName";
            public const string COL_REGISTRATION_MODE = "RegistrationMode";
        }
        #endregion

        #region EmployeeEducationDetails

        public class EmployeeEducationDetails
        {
            public const string COL_EMPLOYEEEDUCATIONDETAIL_ID = "EmployeeEducationDetailID";
            public const string COL_EMPLOYEE_GUID = "EmployeeGUID";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_QUALIFICATION = "Qualification";
            public const string COL_GRADUATION_DATE = "DateOfGraduation";
            public const string COL_GRADUATION_PLACE = "PlaceOfGraduation";
            public const string COL_GRADUATION_COUNTRY = "CountryOfGraduation";
            public const string COL_SPECIALIZATION = "Specialization";
            public const string COL_SPECIALIZATION_NAME = "NameOfSpecialization";
            public const string COL_SPECIALIZATION_CODE = "SpecializationCode";
            public const string COL_SPECIFIC_SPECIALIZATION = "SpecificSpecialization";
            public const string COL_GRADE = "Grade";
            public const string COL_SEQUENCE_ORDER = "SequenceOrder";
            public const string COL_CREATEDBY_ID = "CreatedById";
            public const string COL_CREATED_DATE = "DateCreated";
            public const string COL_LASTMODIFIEDBY_ID = "LastModifiedById";
            public const string COL_LASTMODIFIED_DATE = "LastModifiedDate";
        }
        #endregion

        #region StudentPenalty
        public class StudentPenalty
        {

            //FIELDS
            public const string COL_STUDENT_PENALTY_ID = "StudentPenaltyId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_TERMSEM = "TermSem";
            public const string COL_TERMSEMNAME = "TermSemName";
            public const string COL_TERMYEAR = "TermYear";
            public const string COL_PROGRAMID = "ProgramId";
            public const string COL_SEMESTERID = "SemesterId";
            public const string COL_PENALTYTYPE = "PenaltyType";
            public const string COL_PENALTYTITLE = "PenaltyTitle";
            public const string COL_PENALTYCHARGES = "PenaltyCharges";
            public const string COL_COMMENTS = "Comments";
            public const string COL_VIEW = "View";
        }
        #endregion

        #region ClassTimeTable
        public class ClassTimeTable
        {


            #region StudentTransferRequests
            public class StudentTransferRequests
            {
                //FIELDS
                public const string COL_STUDENTTRANSFERREQUESTID = "StudentTransferRequestId";
                public const string COL_PROGRAMID = "ProgramId";
                public const string COL_SEMESTERID = "SemesterId";
                public const string COL_STUDENTGUID = "StudentGUID";
                public const string COL_STATUS = "Status";
                public const string COL_TRANSFERREQUEST_TYPE = "TransferRequestType";
                public const string COL_REQUEST_TITLE = "RequestTitle";
                public const string COL_REQUESTEDDATE = "DateCreated";
                public const string COL_APPROVEDDATE = "ApprovalDate";
                public const string COL_APPROVALDETAILS = "ApprovalDetails";
            }
            #endregion

            //FIELDS
            public const string COL_CLASS_TIME_TABLE_DETAIL_COURSE_TITLE = "CourseTitle";
            public const string COL_CLASS_TIME_TABLE_DETAIL_STUDENT_GUID = "StudentGuId";
            public const string COL_CLASS_TIME_TABLE_DETAIL_START_TIME = "StartTime";
            public const string COL_CLASS_TIME_TABLE_DETAIL_END_TIME = "EndTime";

            public const string COL_CLASS_TIME_TABLE_DETAIL_START_TIMEEXAM = "starttimeExam";
            public const string COL_CLASS_TIME_TABLE_DETAIL_END_TIMEEXAM = "endtimeExam";
            public const string COL_CLASS_TIME_TABLE_DETAIL_EXAMTITLE = "ExamTitle";
        }
        #endregion

        #region StudentTransferRequests
        public class StudentTransferRequests
        {
            //FIELDS
            public const string COL_STUDENTTRANSFERREQUESTID = "StudentTransferRequestId";
            public const string COL_PROGRAMID = "ProgramId";
            public const string COL_SEMESTERID = "SemesterId";
            public const string COL_STUDENTGUID = "StudentGUID";
            public const string COL_STATUS = "Status";
            public const string COL_TRANSFERREQUEST_TYPE = "TransferRequestType";
            public const string COL_REQUEST_TITLE = "RequestTitle";
            public const string COL_REQUESTEDDATE = "RequestedDate";
            public const string COL_APPROVEDDATE = "ApprovalDate";
            public const string COL_APPROVALDETAILS = "ApprovalDetails";
            public const string COL_STUDENTDETAILS = "StudentDetails";
            public const string COL_ACADEMICDETAILS = "AcademicDetails";
            public const string COL_STUDENTID = "StudentID";
        }
        #endregion

        #region Reminders
        public class Reminders
        {
            public const string COL_TITLE = "title";
            public const string COL_DESCRIPTION = "description";
            public const string COL_EVENT_DATE = "eventdate";
            public const string COL_EVENT_Id = "EventId";
            public const string COL_EVENT_ISACTIVE = "IsActive";
            public const string COL_REMINDER_TYPE = "ReminderType";
            public const string COL_EVENTDATE = "EventDate";
            public const string COL_TITLECAP = "Title";
            public const string COL_DESC_CAP = "Description";
            public const string COL_ACADEMICCAL_ID = "AcademicCalId";
        }
        #endregion

        #region TranscriptDetails
        public class TranscriptDetails
        {
            //FIELDS
            public const string COL_COURSE_CODE = "CourseCode";
            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_CREDITHRS = "CreditHrs";
            public const string COL_SCORE = "Score";
            public const string COL_GRADEPOINTS = "GradePoints";
            public const string COL_GRADELETTER = "GradeLetter";
            public const string COL_GPACLASS = "GPAClass";
            public const string COL_GPACLASS_TILLTERM = "GPAClasstillterm";
            public const string COL_FAILEDHRS = "FailedHrs";
            public const string COL_FAILEDHRS_TILLTERM = "FailedHrsTillterm";

        }
        #endregion

        #region Menu
        public class MenuItem
        {
            public const string COL_MENU_NAME = "MenuItemName";
            public const string COL_MENU_ID = "MenuLinkId";
            public const string COL_MENU_IS_ACTIVE = "IsActive";
            public const string COL_FEATURE_ID = "FeatureId";
            public const string COL_CAN_VIEW_STATUS = "CanViewStatus";
        }
        #endregion

        #region FundingType
        public class FundingType
        {
            //FIELDS
            public const string COL_FUNDINGTYPEID = "FundingTypeId";
            public const string COL_CODE = "Code";
            public const string COL_TYPE = "FundingType";
            public const string COL_TYPE_DETAILS = "FundingTypeDetails";
            public const string COL_ELIGIBILITYTYPE = "EligibilityType";
            public const string COL_SCORE = "Score";
            public const string COL_UPLOADPDF = "UploadPDF";
            public const string COL_REFERENCE = "Reference";
            public const string COL_FUNDINGCATEGORY = "FundingCategory";
            public const string COL_DESCRIPTION = "Description";
        }
        #endregion

        #region SponsosrsInvoice

        public class SponsosrsInvoice
        {
            //COLUMNS
            public const string COL_SPONSORS_INVOICE_INVOICE_ID = "SponsorInvoiceID";
            public const string COL_SPONSORS_INVOICE_TERM_SEM = "TermSem";
            public const string COL_SPONSORS_INVOICE_TERM_YEAR = "TermYear";
            public const string COL_SPONSORS_INVOICE_INVOICE_NO = "InvoiceNo";
            public const string COL_SPONSORS_INVOICE_INVOICE_DATE = "InvoiceDate";
            public const string COL_SPONSORS_INVOICE_SPONSOR_ALLOCATION_ID = "SponsorAllocationId";
            public const string COL_SPONSORS_INVOICE_FUNDING_TYPE_ID = "FundingTypeId";
            public const string COL_SPONSORS_INVOICE_DUE_DATE = "DueDate";
            public const string COL_SPONSORS_INVOICE_TOTAL_TUTION_FEE = "TotalTutionFee";
            public const string COL_SPONSORS_INVOICE_TOTAL_AMT_CHARGED = "TotalAmountCharged";
            public const string COL_SPONSORS_INVOICE_BALANCE_AMT = "BalanceAmount";
            public const string COL_SPONSORS_INVOICE_LATE_FEE = "LateFee";
            public const string COL_SPONSORS_INVOICE_IS_REMINDER_REQUIRED = "IsReminderRequired";
            public const string COL_SPONSORS_INVOICE_REMINDER_UNIT = "ReminderUnit";
            public const string COL_SPONSORS_INVOICE_REMINDER_UNIT_LENGTH = "ReminderUnitLength";
            public const string COL_SPONSORS_INVOICE_IS_STUDENT_REMINDER_REQUIRED = "IsStudentReminderRequired";
            public const string COL_SPONSORS_INVOICE_IS_HR_REMINDER_REQUIRED = "IsHRReminderRequired";
            public const string COL_SPONSORS_INVOICE_INVOICE_STATUS = "InvoiceStatus";
            public const string COL_SPONSORS_INVOICE_TOTAL_APPROVED_STUDENTS = "TotalApprovedStudents";
            public const string COL_SPONSORS_INVOICE_APPROVEDSTUDENTLINK = "ApprovedStudentLink";
            public const string COL_SPONSORS_INVOICE_TERMSEMNAME = "TermSemName";
            public const string COL_SPONSORS_AMOUNT = "Amount";
            public const string COL_SPONSORS_INVOICE_TERMNAME = "TermName";
        }
        #endregion

        #region ApplicationChecklistItemsMaster

        public class ApplicationChecklistItemsMaster
        {
            //COLUMNS
            public const string COL_APPLICATION_CHECK_LST_ITEM_ID = "ApplicationChecklistItemId";
            public const string COL_ITEM_NAME = "ItemName";
            public const string COL_ITEM_DESCRIPTION = "ItemDescription";
            public const string COL_PAGE_URL = "PageURL";
            public const string COL_DEGREE_TYPE = "DegreeType";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_DISPLAY_ORDER = "DisplayOrder";
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_SECOND_PAGE_URL = "SecondPageURL";
            public const string COL_CHECKLIST = "Checklist";
        }
        #endregion

        #region StudentApplicationChecklist
        public class StudentApplicationChecklist
        {
            //COLUMNS
            public const string COL_APPLICATION_ID = "ApplicationId";
            public const string COL_APPLICATION_CHECK_LST_ITEM_ID = "ApplicationChecklistItemId";
            public const string COL_IS_COMPLETED = "IsCompleted";
            public const string COL_SEQUENCE_ORDER_ID = "SequenceOrder";
        }
        #endregion

        #region FODMaster
        public class FODMaster
        {

            //COLUMNS                
            public const string COL_FOD_MASTER_ID = "FODId";
            public const string COL_FOD_MASTER_TITLE = "FODTitle";
            public const string COL_FOD_MASTER_DESCRIPTION = "Description";
            public const string COL_FOD_MASTER_PDFPATH = "PDFFilePath";
            public const string COL_FOD_MASTER_DEGREES = "Degrees";
            public const string COL_FOD_MASTER_DEGREE_TYPE_IDS = "DegreeTyepeIds";
            public const string COL_FOD_MASTER_EXCLUDEINCLUDE_STATUS = "ExcludeIncludeStatus";
            public const string COL_FOD_MASTER_FROMTERMYEAR = "FromTermYear";
            public const string COL_FOD_MASTER_FROMTERMSEM = "FromTermSem";
            public const string COL_FOD_MASTER_TOTERMYEAR = "ToTermYear";
            public const string COL_FOD_MASTER_TOTERMSEM = "ToTermSem";
        }
        #endregion

        #region MajorRestrictions
        public class MajorRestrictions
        {

            //COLUMNS         
            public const string COL_MAJORS_RESTRICTIONS_ID = "MajorRestrictionId";
            public const string COL_MAJORS_RESTRICTIONS_PROGRAMID = "ProgramId";
            public const string COL_MAJORS_RESTRICTIONS_PROGRAMDISPLAYTEXT = "ProgramDisplayText";

        }
        #endregion

        #region SpecialApproverConsentRestrictions
        public class SpecialApproverConsentRestrictions
        {
            public const string COL_SPECIALAPPROVERCONSENTRESTRICTIONS_ID = "SpecialApproverConsentId";
        }
        #endregion

        #region InstructorConsentRestrictions
        public class InstructorConsentRestrictions
        {
            public const string COL_INSTRUCTORCONSENTRESTRICTIONS_ID = "InstructorConsentId";
            public const string COL_ISCONCENTFOROVERBOOKING = "IsConsentForOverBooking";
            public const string COL_ISCONCENTFORREGISTRATION = "IsConsentForRegistration";
        }
        #endregion

        #region GenderRestrictions
        public class GenderRestrictions
        {

            //COLUMNS         
            public const string COL_GENDER_RESTRICTIONS_ID = "GenderRestrictionId";
            public const string COL_GENDER_RESTRICTIONS_LOOKUPTYPEID = "Lookuptypeid";
            public const string COL_GENDER_RESTRICTIONS_GENDER = "Gender";
            public const string COL_GENDERNAME = "GenderName";
        }
        #endregion

        #region ClassRestrictions
        public class ClassRestrictions
        {
            //COLUMNS         
            public const string COL_CLASS_RESTRICTIONS_ID = "ClassRestrictionId";
            public const string COL_CLASS_RESTRICTIONS_CLASSTITLE = "ClassTitle";
            public const string COL_CLASS_RESTRICTIONS_CLASSLEVELID = "ClassLevelId";
        }
        #endregion

        #region GradeRestrictions
        public class GradeRestrictions
        {
            //COLUMNS         
            public const string COL_GRADE_RESTRICTIONS_ID = "GradeRestrictionId";
            public const string COL_GRADE_RESTRICTIONS_GRADE = "Grade";
            public const string COL_GRADE_RESTRICTIONS_MINIMUMGPA = "MinimumGPA";
            public const string COL_GRADE_RESTRICTIONS_MAXIMUMGPA = "MaximumGPA";
            public const string COL_GRADE_LOOKUP_TEXT = "LookupText";
        }
        #endregion

        #region ApplicationDecisionCriterias

        public class ApplicationDecisionCriterias
        {
            //COLUMNS                
            public const string COL_APPLICATION_DECISION_ID = "ApplicationDecisionId";
            public const string COL_DEGREE_TYPEID = "DegreeTypeId";
            public const string COL_TERM_ID = "TermId";
            public const string COL_STUDENT_TYPE = "StudentType";
            public const string COL_APPLICATION_DECISION_FOR = "ApplicationDecisionFor";
            public const string COL_DECISION_MODE = "DecisionMode";
            public const string COL_SCORE = "Score";
            public const string COL_CRITERIA_CONDITION = "CriteriaCondition";
            public const string COL_COMPLETION_STATUS = "CompletionStatus";
            public const string COL_SEQUENCE_ORDER = "SequenceOrder";
            public const string COL_TEST_NAMES = "TestNames";
            public const string COL_TEST_SCORES = "TestScores";
            public const string COL_TEST_SCORE = "TestScore";
        }
        #endregion

        #region CitiesMaster
        public class CitiesMaster
        {
            public const string COL_CITY_ID = "CityId";
            public const string COL_CITYNAME = "CityName";
            public const string COL_AR_CITY_NAME = "ARCityName";
            public const string COL_CITY_AR_NAME = "ARCityName";
            public const string COL_PROVINCE_NAME = "ProvinceName";
            public const string COL_COUNTRY = "CountryName";
            public const string COL_MOHE_CODE = "Code";
            public const string COL_STATUS = "Status";
        }
        #endregion

        #region DocumentCheckList
        public class DocumentCheckList
        {
            public const string COL_DOCUMENT_CHECK_LIST_ID = "DocumentCheckListId";
            public const string COL_DEGREE_TYPE_ID = "DegreeTypeId";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_REQUIRED_COPIES = "RequiredCopies";
            public const string COL_DOCUMENT_NAME = "DocumentName";
            public const string COL_AR_DOCUMENT_NAME = "ARDocumentName";
            public const string COL_DESCRIPTION = "Description";
            public const string COL_IS_ACTIVE = "IsActive";
            public const string COL_LANGUAGE_ID = "LanguageId";
            public const string COL_MODIFIED_BY = "LastModifiedName";
            public const string COL_DEGREE_TYPE = "DegreeType";

        }
        #endregion

        #region TempStudentDocCheckList
        public class TempStudentDocCheckList
        {
            public const string COL_STATUS = "Status";
            public const string COL_APPLICATION_CHECKLIST_ID = "ApplicationCheckListId";
            public const string COL_DOCUMNET_CHECKLIST_ID = "DocumentCheckListID";
            public const string COL_NO_OF_COPIES = "NoOfCopies";

            public const string COL_DOCUMENT_NAME = "DocumentName";
            public const string COL_TEMP_STUDENT_DOC_CHECK_ID = "TempStudentDocCheckID";
            public const string COL_LAST_MODIFIED_BY_NAME = "LastModifiedName";
            public const string COL_LAST_MODIFIED_DATE = "LastModifiedDate";
        }
        #endregion

        #region ApplicationCheckListMaster
        public class ApplicationCheckListMaster
        {
            public const string COL_FIRST_NAME = "FirstName";
            public const string COL_MIDDLENAME = "MiddleName";
            public const string COL_LAST_NAME = "LastName";
            public const string COL_EMAIL_ID = "EmailId";
            public const string COL_FISRT_PREFERENCENAME = "FirstPreferenceName";
            public const string COL_RECEIVED = "Received";
            public const string COL_MISSING = "Missing";
            public const string COL_NOT_REQUIRED = "NotRequired";
            public const string COL_LAST_MODIFIED_Date = "LastModifiedDate";
            public const string COL_APPLICATIONSTATUS = "Status";
            public const string COL_APPLICATIONID = "ApplicationID";
            public const string COL_APPLICATIONTYPE = "ApplicationType";
            public const string COL_STUDENT_APPLICATIONID = "StudentApplicationID";

            public const string COL_CHECKLIST_STATUS = "Status";
            public const string COL_CHECKLIST_APPLICATION_CHECKLIST_ID = "ApplicationCheckListId";
            public const string COL_CHECKLIST_DOCUMNET_CHECKLIST_ID = "DocumentCheckListID";
            public const string COL_CHECKLIST_NO_OF_COPIES = "NoOfCopies";

            public const string COL_CHECKLIST_DOCUMENT_NAME = "DocumentName";
            public const string COL_CHECKLIST_TEMP_STUDENT_DOC_CHECK_ID = "TempStudentDocCheckID";
            public const string COL_CHECKLIST_LAST_MODIFIED_BY_NAME = "LastModifiedByName";
        }
        #endregion

        #region ApplicationAcceptance

        public class ApplicationAcceptance
        {
            public const string COL_STUDENT_APPLICATION_ID = "StudentApplicationID";
            public const string COL_APPLICATION_ID = "ApplicationID";
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_APPLICATION_DETAILS = "ApplicationDetails";
            public const string COL_PREFERENCES = "Preferences";
            public const string COL_STATUS = "ApprovalStatus";
            public const string COL_LATEST_GRADE = "Grade";
            public const string COL_ELIGIBILITY = "EligibilityTest";
            public const string COL_ACTION = "Actions";
            public const string COL_ACCEPT = "Accept";
            public const string COL_APPROVE = "Approve";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MIddleName";
            public const string COL_GRANDFATHERNAME = "GrandFatherName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_DEGREE_NAME = "DegreeName";
            public const string COL_APPLICATION_TYPE = "ApplicationType";
            public const string COL_TERM_NAME = "TermName";
            public const string COL_FIRST_PREFERENCE_NAME = "FirstPrefrenceTitle";
            public const string COL_SECOND_PREFERENCE_NAME = "SecondPrefrenceTitle";
            public const string COL_THIRD_PREFERENCE_NAME = "ThirdPrefrenceTitle";
            public const string COL_FORTH_PREFERENCE_NAME = "ForthPrefrenceTitle";
            public const string COL_FIFTH_PREFERENCE_NAME = "FifthPrefrenceTitle";
            public const string COL_PREVIOUS_SCHOOL = "PreviousSchool";
            public const string COL_DEGREE_ID = "DegreeTypeId";
            public const string COL_ADMISSIONFOR = "AdmissionFor";
            public const string COL_SYSTEMUSERGUID = "SystemUserGUID";
            public const string COL_EMAIL = "EmailId";
            public const string COL_CHECKLIST = "Checklist";
            public const string COL_LOOKUPTEXT = "LookupText";
            public const string COL_LOOKUPVALUE = "LookupValue";
            public const string COL_MAJORS = "Majors";
            public const string COL_DOCUMNET_LIST = "DocumentList";
            public const string COL_DUE_DATE = "DueDate";
            public const string COL_REJECTION_POINT = "RejectionPoint";
        }
        #endregion ApplicationAcceptance

        #region CourseCatalgoue

        public class CourseCatalgoue
        {
            //FIELDS
            public const string COL_CATALOGUE_ID = "CatalogueId";
            public const string COL_ACADEMICYEAR_ID = "AcademicYearId";
            public const string COL_PUBLISH_DATE = "PublishDate";
            public const string COL_EXPIRY_DATE = "ExpiryDate";
            public const string COL_STAUS = "Status";
            public const string COL_ACADEMIC_STARTYEAR = "AcademicStartYear";
            public const string COL_ACADEMICYEAR = "AcademicYear";
            public const string COL_TOTAL_COURSES = "TotalCourses";
            public const string COL_TOTAL_COURSE_GROUPS = "TotalCourseGroups";
            public const string COL_TOTAL_CREDITS = "TotalCredits";
            public const string COL_PREREQUISITES = "CoursePreRequisites";
            public const string COL_COREQUISITES = "CourseCoRequisites";
            public const string COL_CONSENTS = "Consents";
            public const string COL_LEVELTITLE = "LevelTitle";
            public const string COL_COURSE_OTHERDESCRIPTION = "OtherDescription";
            public const string COL_ISCLASSSCHEDULEEXISTS = "IsClassScheduleExists";
            public const string COL_CRN = "CRN";
            public const string COL_LASTMODIFIED_NAME = "LastModifiedName";
        }
        #endregion

        #region AcademicYearMaster
        public class AcademicYearMaster
        {
            public const string COL_ACADEMIC_YEAR_ID = "AcademicYearId";
            public const string COL_ACADEMIC_START_YEAR = "AcademicStartYear";
            public const string COL_ACADEMIC_END_YEAR = "AcademicEndYear";
            public const string COL_ACADEMIC_YEAR_START_DATE = "StartDate";
            public const string COL_ACADEMIC_YEAR_END_DATE = "EndDate";
            public const string COL_ACADEMIC_YEAR_STATUS = "Status";
            public const string COL_ACADEMIC_IS_USED = "IsUsed";
        }
        #endregion

        #region ClassScheduleMaster

        public class ClassScheduleMaster
        {

            //COLUMNS
            public const string COL_CLASS_SCHEDULE_CODE_ID = "ClassScheduleCodeId";
            public const string COL_CLASS_SCHEDULE_CODE = "Code";
            public const string COL_CLASS_SCHEDULE_ID = "ClassScheduleId";
            public const string COL_CLASS_SCHEDULE_COURSEGROUPID = "CourseGroupId";
            public const string COL_CLASS_SCHEDULE_COURSEID = "CourseID";
            public const string COL_CLASS_SCHEDULE_STARTTIME = "StartTime";
            public const string COL_CLASS_SCHEDULE_ENDTIME = "EndTime";
            public const string COL_CLASS_SCHEDULE_CLASSROOMID = "ClassRoomId";
            public const string COL_CLASS_SCHEDULE_DEPTID = "DepartmentId";
            public const string COL_CLASS_SCHEDULE_INSTRUCTID = "InstructorId";
            public const string COL_CLASS_SCHEDULE_CRN = "CRN";
            public const string COL_CLASS_SCHEDULE_SECTION = "Section";
            public const string COL_COURSE_ALTERNATE_TITLES = "AlternateTitles";
            public const string COL_CLASS_SCHEDULE_STARTDATE = "StartDate";
            public const string COL_CLASS_SCHEDULE_ENDDATE = "EndDate";
            public const string COL_CLASS_SCHEDULE_SATURDAY = "IsScheduledForSaturday";
            public const string COL_CLASS_SCHEDULE_SUNDAY = "IsScheduledForSunday";
            public const string COL_CLASS_SCHEDULE_MONDAY = "IsScheduledForMonday";
            public const string COL_CLASS_SCHEDULE_TUESDAY = "IsScheduledForTuesday";
            public const string COL_CLASS_SCHEDULE_WEDNESDAY = "IsScheduledForWednesday";
            public const string COL_CLASS_SCHEDULE_THURSDAY = "IsScheduledForThursday";
            public const string COL_CLASS_SCHEDULE_FRIDAY = "IsScheduledForFriday";
            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_COURSE_DECRITPION = "CourseDescription";
            public const string COL_INSTRUCTOR = "Instructor";
            public const string COL_INSTRUCTOR_NAME = "InstructorName";
            public const string COL_AR_INSTRUCTOR_NAME = "ARInstructorName";
            public const string COL_CLASSROOMNAME = "ClassRoomName";
            public const string COL_DYASSCHEDULED = "DaysScheduled";
            public const string COL_DYASSCHEDULED_CUSTOM_CS = "DaysScheduledCustomCS";
            public const string COL_SCHEDULEDDATES_CUSTOM_CS = "ScheduledDatesCustomCS";
            public const string COL_COURSE_GROUP_NAME = "CourseGroupName";
            public const string COL_STATUS = "Status";
            public const string COL_TOTAL_CREDITS = "TotalCredits";
            public const string COL_COURSE_DESCRIPTION = "CourseDescription";
            public const string COL_TERM_NAME = "TermName";
            public const string COL_PRE_REQUISITES = "CoursePreRequisites";
            public const string COL_CO_REQUISITES = "CourseCoRequisites";
            public const string COL_REGISTRATION_STARTDATE = "RegistrationStartDate";
            public const string COL_REGISTRATION_ENDDATE = "RegistrationEndDate";
            public const string COL_RW_REGISTRATION_STARTDATE = "RWRegistrationStartDate";
            public const string COL_RW_REGISTRATION_ENDDATE = "RWRegistrationEndDate";
            public const string COL_ARABIC_COURSE_TITLE = "ArabicCourseTitle";
            public const string COL_RESTRICTION_CLASS_LEVELTITLE = "LevelTitle";
            public const string COL_RESTRICTION_DEGREENAME = "DegreeName";
            public const string COL_RESTRICTION_FODTITLE = "FODTitle";
            public const string COL_RESTRICTION_GENDERNAME = "GenderName";
            public const string COL_RESTRICTION_GRADE = "LookupText";
            public const string COL_RESTRICTION_MAJOR = "ProgramDisplayText";
            public const string COL_DAYSTIME = "DaysNTime";
            public const string COL_BOUND_DATE = "StartToEndDate";
            public const string COL_TOTAL_CAPACITY = "TotalCapacity";
            public const string COL_VACANCY = "Vacancy";
            public const string COL_CONSUMED_CAPACITY = "ConsumedCapacity";
            public const string COL_REG_STATUS_TEXT = "RegistrationStatusText";
            public const string COL_REG_MODIFIED_DATE = "RegistrationModifiedDate";
            public const string COL_REG_DATE = "RegistrationDate";
            public const string COL_IS_USED = "IsUsed";
            public const string COL_REGISTERED_STUDENT_COUNT = "RegistreredStudentsCount";
            public const string COL_REGISTRATION_TYPES_LOOKUP_TEXT = "RegistrationTypesLookupText";
            public const string COL_REGISTRATION_TYPES_LOOKUP_VALUE = "RegistrationTypesLookupValue";
            public const string COL_LASTMODIFIEDDATE = "LastModifiedDate";
            public const string COL_REGISTEREDSEMESTER = "RegisteredSemester";
            public const string COL_REGISTEREDPROGRAM = "RegisteredProgram";
            public const string COL_CLASS_SCHEDULECODE = "ScheduleCode";
            public const string COL_CLASS_SCHEDULE_MEETINGDETAILS_ID = "ID";
            public const string COL_CLASS_MEETINGTYPE = "MeetingType";
        }
        #endregion

        #region StudentPINAllocation

        public class StudentPINAllocation
        {
            public const string COL_STUDENTPINALLOCATION_ID = "StudentPinAllocationId";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_STUDENT_ID = "StudentId";
            public const string COL_FIRST_NAME = "FirstName";
            public const string COL_MIDDLE_NAME = "MiddleName";
            public const string COL_LAST_NAME = "LastName";
            public const string COL_GRANDFATHERNAME = "GrandFatherName";
            public const string COL_STUDENT_DETAILS = "StudentDetails";
            public const string COL_REGISTRATION_PIN = "PIN";
            public const string COL_TERM_REGISTRATION = "TermRegistrations";
            public const string COL_TERM_PERFORMANCE = "TermPerformance";
            public const string COL_TERM_PAYMENTS = "TermPayments";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_PRORAM_DISPLAY_TEST = "programdisplaytext";
            public const string COL_PROGRAM_ID = "ProgramId";
        }
        #endregion

        #region ManagePromotions

        public class ManagePromotions
        {
            //FIELDS
            public const string COL_PROMOTIONAL_ID = "PromotionalId";
            public const string COL_MAJOR_TITLE = "ProgramTitle";
            public const string COL_MAJOR_CODE = "ProgramId";
            public const string COL_TOTAL_SEMESTER = "TotalSemesters";
            public const string COL_TERM_NAME = "TermName";
            public const string COL_LASTMODIFIED_NAME = "LastModifiedName";
            public const string COL_ACTION = "Action";
            public const string COL_PROGRAM_DISPLAY_TEXT = "ProgramDisplayText";
            public const string COL_SEMESTER_NAME = "SemesterName";
            public const string COL_SEMESTER_ID = "SemesterId";
            public const string COL_STATUS = "Status";
            public const string COL_TO_TERM_NAME = "ToTermName";
            public const string COL_STARTTERMID = "StartTermId";
        }
        #endregion

        #region ClassScheduleRegistrationMaster

        public class ClassScheduleRegistrationMaster
        {
            //FIELDS
            public const string COL_MINCREDITS = "MinCredits";
            public const string COL_MAXCREDITS = "MaxCredits";
            public const string COL_REGISTERED_CREDITS = "RegisteredCredits";
            public const string COL_REGISTERED_CREDITS_TILLTERM = "RegisteredCreditsTillterm";
            public const string COL_REQUIRED_CREDITS = "RequiredCredits";
            public const string COL_TERM = "TermName";
            public const string COL_EARNEDCREDITS = "EarnedCredits";
            public const string COL_EARNEDCREDITS_TILLTERM = "EarnedCreditsTillterm";
        }
        #endregion

        #region ClassRoster

        public class ClassRoster
        {
            //FIELDS
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_MEETING_DETAILS = "MeetingDetails";
            public const string COL_ROASTER_MEETING_DETAILS = "RoasterMeetingDetails";
            public const string COL_STUDENTID = "StudentId";
            public const string COL_ACADEMIC_DETAILS = "AcademicDetails";
            public const string COL_REGISTRATION_DETAIL = "RegistrationDetails";
            public const string COL_REG_NO = "RegistrationNo";
            public const string COL_ACTION = "Actions";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MIddleName";
            public const string COL_GRANDFATHERNAME = "GrandFatherName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_CLASSLEVEL = "ClassLevel";
            public const string COL_PROGRAMNAME = "ProgramName";
            public const string COL_REG_STATUS = "RegistrationStatus";
            public const string COL_CREATED_ON = "DateCreated";
            public const string COL_CLASS_TITLE = "ClassTitle";
            public const string COL_TOTAL_CREDIT = "TotalCredits";
            public const string COL_COURSE_PRE = "CoursePreRequisites";
            public const string COL_COURSE_DESCRITPION = "CourseDescription";
            public const string COL_TERM_NAME = "TermName";
            public const string COL_COURSE_CO = "CourseCoRequisites";
            public const string COL_START_DATE = "StartDate";
            public const string COL_END_DATE = "EndDate";
            public const string COL_TOTAL_CAPACITY = "TotalCapacity";
            public const string COL_CONSUME_CAPACITY = "ConsumedCapacity";
            public const string COL_DAYS = "DaysScheduled";
            public const string COL_START_TIME = "StartTime";
            public const string COL_END_TIME = "EndTime";
            public const string COL_REG_START_DATE = "RegistrationStartDate";
            public const string COL_REG_END_DATE = "RegistrationEndDate";
            public const string COL_CLASS_ROOM = "ClassRoomName";
            public const string COL_CLASS_SCHEDULE_ID = "ClassScheduleId";
            public const string COL_CRN = "CRN";
            public const string COL_INST = "Instructor";
            public const string COL_COURSE_TITLE = "CourseTitle";
            public const string COL_COURSE_CON = "ConsentRequired";
            public const string COL_COURSE_AVAILABLE_TERM = "ConsentRequired";
            public const string COL_COURSE_PASS_PERCENTAGE = "PassingPercentage";
            public const string COL_COURSE_RESTRICT_INCLUDE = "RestricationsIncluded";
            public const string COL_CLASS_TERM_ID = "TermId";
        }
        #endregion

        #region AddGrade

        public class AddGrade
        {
            //FIELDS
            public const string COL_GRADE_POINTS = "GradePoints";
            public const string COL_STUDENT_NAME = "StudentName";
            public const string COL_FIRSTNAME = "FirstName";
            public const string COL_MIDDLENAME = "MIddleName";
            public const string COL_GRANDFATHERNAME = "GrandFatherName";
            public const string COL_LASTNAME = "LastName";
            public const string COL_SCORE = "Score";
            public const string COL_ICSTATUS = "ICStatus";
            public const string COL_CREDITHRS = "CreditHrs";
            public const string COL_GRADE_OUT_OF_FIVE = "Points";
            public const string COL_CREDIT = "Credit";
            public const string COL_LASTMODIFIED = "LastModified";
            public const string COL_LASTMODIFIED_NAME = "LastModifiedName";
            public const string COL_LASTMODIFIED_DATE = "LastModifiedDate";
            public const string COL_ISPUBLISHED = "IsPublished";
            public const string COL_PUBLISHEDSTATUS = "PublishedStatus";
            public const string COL_MARKFROM = "MarksFrom";
            public const string COL_MARKTO = "MarksTo";
            public const string COL_STUDENT_GRADE_IDS = "StudentGradeIds";
            public const string COL_ISALL_GRADES_SUBMITTED = "IsAllGradesSubmitted";
            public const string COL_STUDENT_GRADE_ID = "StudentGradeId";
            public const string COL_STUDENT_GRADE_RESULT = "Result";
        }
        #endregion

        #region DHL

        public class DHL
        {
            public const string COL_STUDENT_ID = "StudentID";
            public const string COL_STUDENT_FIRST_NAME = "FirstName";
            public const string COL_STUDENT_LAST_NAME = "LastName";
            public const string COL_SEMESTER_AVERAGE = "SemAverage";
        }

        #endregion

        #region ClassLevels

        public class ClassLevels
        {
            public const string COL_LEVEL_TITLE = "LevelTitle";
        }

        #endregion

        #region DHLDetails

        public class DHLDetails
        {
            public const string COL_DHLID = "DHLId";
            public const string COL_DHLDETAILSID = "DHLDetailsId";
            public const string COL_TERMID = "TermId";
            public const string COL_LEVELTITLE = "LevelTitle";
            public const string COL_PUBLISHDATE = "PublishDate";
            public const string COL_STUDENTGUID = "StudentGUID";
            public const string COL_PROGRAMID = "ProgramId";
            public const string COL_SEMESTERAVG = "SemesterAvg";
            public const string COL_PUBLISHSTATUS = "PublishStatus";
            public const string COL_SEMESTERID = "SemesterId";
            public const string COL_AVERAGESCORE = "AverageScore";
        }

        #endregion

        #region Probation

        public class Probation
        {
            public const string COL_GPA = "GPA";
        }
        #endregion

        #region ClearanceDepartmentMaster

        public class ClearanceDepartmentMaster
        {
            public const string COL_DEPTNAME = "DeptName";
            public const string COL_DEPTDESC = "DeptDesc";
            public const string COL_CLEARANCE_DEPT_ID = "ClearanceDeptId";
            public const string COL_CONTACTS = "Contacts";
            public const string COL_ISSELECTED = "IsSelected";
            public const string COL_PRIMARYCONTACT_EMPLOYEEGUID = "PrimaryContactEmpId";
            public const string COL_SECONDARYCONTACT_EMPLOYEEGUID = "SecondaryContactEmpId";
            public const string COL_STUDENTCLEARANCEFORMID = "StudentClearanceFormId";
            public const string COL_INTTERVIEW_DATE = "InterviewDate";
            public const string COL_PRIMARYCONTACT_EMPLOYEENAME = "PrimaryContactEmpName";
            public const string COL_SECONDARYCONTACT_EMPLOYEENAME = "SecondaryContactEmpName";
            public const string COL_STARTTIME = "StartTime";
            public const string COL_ENDATIME = "EndTime";
            public const string COL_CFDEPTID = "CFDEPTID";
            public const string COL_EMPLOYEECLEARANCEFORMID = "EmployeeClearanceFormId";
        }
        #endregion

        #region ClearanceDeptItems
        public class ClearanceDeptItems
        {
            public const string COL_ITEMNAME = "ItemName";
            public const string COL_ITEMDESC = "ItemDesc";
            public const string COL_CLEARANCE_DEPT_ID = "ClearanceDeptId";
            public const string COL_CLEARANCE_DEPT_ITEMID = "ClearanceDeptItemId";
            public const string COL_STATUS = "Status";
            public const string COL_COMMENTS = "Comments";
            public const string COL_CFDEPTITEMID = "CFDeptItemId";
        }
        #endregion

        #region ClearanceDepartmentMaster

        public class ClearanceFormTemplateMaster
        {
            public const string COL_TEMPLATE_NAME = "TemplateName";
            public const string COL_TEMPLATE_DESC = "TemplateDesc";
            public const string COL_CLEARANCE_FORM_TEMPLATE_ID = "ClearanceFormTemplateId";
            public const string COL_TEMPLATE_ITEMS = "Items";
            public const string COL_CFTEMPLATEDEPT_ID = "CFTemplateDeptId";
        }
        #endregion

        #region AssessmentMaster

        public class AssessmentMaster
        {
            public const string COL_ASSESSMENT_ID = "AssessmentID";
            public const string COL_ASSESSMENT_NAME_EN = "AssessmentNameEnglish";
            public const string COL_ASSESSMENT_NAME_AR = "AssessmentNameAr";
            public const string COL_ASSESSMENT_DESC = "AssessmentDescription";
            public const string COL_ASSESSMENT_IS_ACTIVE = "IsActive";
            public const string COL_ASSESSMENT_STATUS = "Status";
            public const string COL_ASSESSMENT_QUESTION = "QuestionsCount";
            public const string COL_ASSESSMENT_TYPE = "AssessmentType";
            public const string COL_ASSESSMENT_IS_SCORING = "IsScoringRequired";
            public const string COL_ASSESSMENT_IS_ASSIGNED = "IsAssigned";
            public const string COL_ASSESSMENT_ACTION = "Action";
            public const string COL_QUESTION = "Question";
            public const string COL_SHEARINGTYPE = "SharingType";
            public const string COL_SCORETEXT = "ScoreText";

            public const string COL_ASSESSMENT_QUESTIONTITLE_ID = "QuestionID";
            public const string COL_ASSESSMENT_QUESTIONTITLE_EN = "QuestionTitleEnglish";
            public const string COL_ASSESSMENT_QUESTIONTITLE_AR = "QuestionTitleAR";
            public const string COL_ASSESSMENT_QUESTIONCATEGORY_EN = "CategoryNameEnglish";
            public const string COL_ASSESSMENT_QUESTIONCATEGORY_AR = "CategoryNameAR";
            public const string COL_ASSESSMENT_QUESTION_TYPE = "QuestionType";
            public const string COL_ASSESSMENT_QUESTION_SEQUENCE_ORDER = "SequenceOrder";

            public const string COL_ASSESSMENT_QUESTION_CATEGORY_ID = "QuestionCategoryID";
            public const string COL_ASSESSMENT_QUESTION_CATEGORY_NAME_EN = "CategoryNameEnglish";
            public const string COL_ASSESSMENT_QUESTION_CATEGORY_NAME_AR = "CategoryNameAR";
            public const string COL_ASSESSMENT_QUESTION_SCORE = "Score";
            public const string COL_ASSESSMENT_IS_SCORE_REQUIRED = "IsScoreRequired";

        }
        #endregion

        #region QuestionsCategoryMaster

        public class QuestionsCategoryMaster
        {

            //COLUMNS                
            public const string COL_QUESTION_CATEGORY_ID = "QuestionCategoryId";
            public const string COL_CATEGORY_NAME_ENGLISH = "CategoryNameEnglish";
            public const string COL_CATEGORY_NAME_AR = "CategoryNameAR";
            public const string COL_CATEGORY_DESCRIPTION = "CategoryDescription";
            public const string COL_ISACTIVE = "IsActive";
            public const string COL_QUESTIONSCOUNTLINK = "QuestionsCountLink";
            public const string COL_QUESTIONSCOUNT = "QuestionsCount";
            public const string COL_ACTIVE = "Active";
        }
        #endregion

        #region QuestionRepositoryMaster

        public class QuestionRepositoryMaster
        {
            //COLUMNS                
            public const string COL_QUESTION_ID = "QuestionId";
            public const string COL_QUESTION_CATEGORY_ID = "QuestionCategoryId";
            public const string COL_QUESTION_TYPE = "QuestionType";
            public const string COL_QUESTION_TITLE_ENGLISH = "QuestionTitleEnglish";
            public const string COL_QUESTION_TITLE_AR = "QuestionTitleAR";
            public const string COL_QUESTION_DESCRIPTION = "QuestionDescription";
            public const string COL_ISACTIVE = "IsActive";
            public const string COL_ISEXPLANATION_REQUIRED = "IsExplanationRequired";
            public const string COL_SCORE = "Score";
            public const string COL_ISSCORE_REQUIRED = "IsScoreRequired";
            public const string COL_ISUSED = "IsUsed";
            public const string COL_EXPLANATIONTEXT = "ExplanationText";
        }
        #endregion

        #region QuestionOptionsMaster
        public class QuestionOptionsMaster
        {
            //COLUMNS                
            public const string COL_QUESTION_OPTION_ID = "QuestionOptionsID";
            public const string COL_QUESTION_ID = "QuestionId";
            public const string COL_OPTION_TITLE_ENGLISH = "OptionTitleEnglish";
            public const string COL_OPTION_TITLE_AR = "OptionTitleAR";
            public const string COL_ISACTIVE = "IsActive";
            public const string COL_ISEXPLANATION_REQUIRED = "IsExplanationRequired";
            public const string COL_ISCORRECT = "IsCorrect";
            public const string COL_SEQUENCEORDER = "SequenceOrder";
            public const string COL_ISOPTIONSELECTED = "isOptionSelected";
            public const string COL_GOTSCORE = "GetScore";
        }
        #endregion

        #region AssignmentQuestionsTracking
        public class AssignmentQuestionsTracking
        {
            //PARAMETERS               
            public const string PARA_ASSIGNMENT_TRACKING_ID = "@AssignmentTrackingId";
            public const string PARA_QUESTIONID = "@QuestionID";
            public const string PARA_EXPLANATION_TEXT = "@ExplanationText";
            public const string PARA_QUESTION_OPTION_IDs = "@QuestionOptionIds";
            public const string PARA_ISCORRECT = "@IsCorrect";
            public const string PARA_SCORE = "@Score";
            public const string PARA_DATESUBMITTED = "@DateSubmitted";

            //COLUMNS                
            public const string COL_ASSIGNMENT_TRACKING_ID = "AssignmentTrackingId";
            public const string COL_QUESTIONID = "QuestionID";
            public const string COL_EXPLANATION_TEXT = "ExplanationText";
            public const string COL_QUESTION_OPTION_IDs = "QuestionOptionIds";
            public const string COL_ISCORRECT = "IsCorrect";
            public const string COL_SCORE = "Score";
            public const string COL_DATESUBMITTED = "DateSubmitted";

        }
        #endregion

        #region SurveyAllotmentMaster
        public class SurveyAllotmentMaster
        {
            //FIELDS
            public const string COL_ALLOTMENT_ID = "AllotmentId";
            public const string COL_ASSESSMENT_ID = "AssessmentId";
            public const string COL_ASSIGNMENT_TITLE = "AssignmentTitle";
            public const string COL_STUDENT_GUID = "StudentGUID";
            public const string COL_COURSE_ID = "CourseId";
            public const string COL_STARTDATE = "StartDate";
            public const string COL_DUEDATE = "DueDate";
            public const string COL_EXPIRYDATE = "ExpiryDate";
            public const string COL_COMPLETION_STATUS = "CompletionStatus";
            public const string COL_FIRST_NAME = "FirstName";
            public const string COL_MIDDLE_NAME = "MiddleName";
            public const string COL_LAST_NAME = "LastName";
            public const string COL_EMAILID = "EmailId";
            public const string COL_TOTSTUDENT = "TotalStudent";
            public const string COL_TOTEMPLOYEE = "TotalEmployee";
            public const string COL_ISEDITABLE = "IsEditable";
        }
        #endregion

        #region AssessmentTracking
        public class AssessmentTracking
        {
            //COLUMNS                
            public const string COL_ASSESSMENT_TRACKING_ID = "AssessmentTrackingId";
            public const string COL_COMPLETED_DATE = "CompletatedDate";
            public const string COL_NOOFATTEMPTS = "NoOfAttempts";
            public const string COL_REVIEWER_COMMENTS = "ReviewerComments";
            public const string COL_CERTIFICATION_FILENAME = "CerrtificationFileName";
            public const string COL_ATTEMPT_LANG_ID = "AttemptLanguageId";
        }
        #endregion

        #region AssessmentQuestionsTracking
        public class AssessmentQuestionsTracking
        {
            //COLUMNS                
            public const string COL_ASSESSMENT_TRACKING_ID = "AssessmentTrackingId";
            public const string COL_QUESTIONID = "QuestionID";
            public const string COL_EXPLANATION_TEXT = "ExplanationText";
            public const string COL_QUESTION_OPTION_IDs = "QuestionOptionIds";
            public const string COL_ISCORRECT = "IsCorrect";
            public const string COL_SCORE = "Score";
            public const string COL_DATESUBMITTED = "DateSubmitted";

        }
        #endregion
    }
}
