﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Utility
{
    public class Messages
    {
        public class Adaptor
        {
            const string MESSAGE_NOT_FOUND = "Invalid Message Id";

            #region GetMessage(string pMessageId)
            /// <summary>
            /// Get message as per Id in en-US
            /// </summary>
            /// <param name="pMessageId">Messsage Id</param>
            /// <returns>Message</returns>
            //public static string GetMessage(string pMessageId)
            //{
            //    return GetMessage(pMessageId, null);
            //}
            #endregion

            #region GetMessage(string pMessageId, string pLanguageId)  
            /// <summary>
            /// Get message as per Id in specified language
            /// </summary>
            /// <param name="pMessageId">Messsage Id</param>
            /// <param name="pLanguageId">Language Id</param>
            /// <returns>Message</returns>
       
            #endregion
        }

        public class Announcments
        {
            public const string ENTER_ANNOUNCMENT_TITLE = "mENTER_ANNOUNCEMENT_TITLE";
            public const string ENTER_PUBLISH_DATE = "mENTER_PUBLISH_DATE";
            public const string PUBLISH_DATE_NOT_LESSTHAN_TODAY = "mPUBLISH_DATE_NOT_LESSTHAN_TODAY";
            public const string EXPIRY_DATE_NOT_GREATERTHAN_TODAY = "mEXPIRY_DATE_NOT_GREATERTHAN_TODAY";
            public const string EXPIRY_DATE_GREATERTHAN_PUBLISHDATE = "mEXPIRY_DATE_GREATERTHAN_PUBLISHDATE";
        }

        public class Contract
        {
            public const string ENTER_BASIC_SALARY = "mENTER_BASIC_SALARY";
            public const string ENTER_CONTRACT_EXPIRYDATE = "mENTER_CONTRACT_EXPIRYDATE";
            public const string ENTER_CONTRACT_NUMBER = "mENTER_CONTRACT_NUMBER";
            public const string ENTER_CONTRACT_STARTDATE = "mENTER_CONTRACT_STARTDATE";
            public const string ENTER_DEPARTMENT = "mENTER_DEPARTMENT";
            public const string ENTER_HOUSING = "mENTER_HOUSING";
            public const string ENTER_POSITION = "mENTER_POSITION";
            public const string ENTER_TRASPORT = "mENTER_TRANSPORT";
        }

        public class ProgramFee
        {
            public const string PROGRAMFEE_ALREADY_EXISTS = "mPROGRAMFEE_ALREADY_EXISTS";

        }

        public class Courses
        {
            public const string ENTER_TITLE = "mENTER_COURSE_TITLE";
            public const string ENTER_CODE = "mENTER_COURSE_CODE";
            public const string ENTER_TITLE_EXIST = "mENTER_COURSE_TITLE_EXIST";
            public const string ENTER_CODE_EXIST = "mENTER_COURSE_CODE_EXIST";
            public const string NOT_DELETE_COURSE = "mNOT_DELETE_COURSE";
            public const string NOT_ARCHIEVE_COURSE = "mNOT_ARCHIEVE_COURSE";
            public const string ENTER_COURSEGROUP = "mENTER_COURSEGROUP";
            public const string ENTER_COURSE_TITLE_IN_ARABIC = "mENTER_COURSE_TITLE_IN_ARABIC";
            public const string ENTER_COURSE_TITLE_CODE_EXIST = "mENTER_COURSE_TITLE_CODE_EXIST";
        }

        public class Common
        {
            public const string INVALID_PERCENTAGE = "mInvalidPercentage";
            public const string INVALID_KEY = "InvalidKey";
            //Email sent successfully to 
            public const string EMAIL_SUCCESS_TO = "mEMAILMSSUCCESS";
            //Please specify the URL of the RSS feed in webpart properties.
            public const string RSS_URL_ERROR = "mRSSURLERROR";
            //Error occured:
            public const string ERROR = "mERROR";
            public const string EMAIL_ERROR = "mEmailError";
            //Click here to View all items
            public const string CLICK_HERE = "mCLICK";
            //Your session is expired. Please login again.
            public const string SESSION_EXP = "mSESSIONEXP";
            //No records found.
            public const string NO_RECORD = "mNORECORD";
            // Feedback send 
            public const string FEEDBACK_SEND = "mFeedBackSend";
            // Record Added successfully
            public const string RECORD_ADDED = "mRecordAdded";
            // Registration successfully
            public const string REGISTER_SUCCESSFULLY = "mRegisterSuccessfully";
            public const string RECORD_NOT_SAVED = "mRECORD_NOT_Saved";
            //Invalid File
            public const string FILE_XSL_INVALID = "mXLSERROR";
            public const string IMPORT_ERROR = "mImportHistoryERROR";
            public const string APP_LOG = "mAPPLOG";
            //The code you typed has expired after 
            public const string CODE_EXPIRED = "mExpiredCode";
            //Code was typed too quickly. Wait at least 
            public const string WAIT_TO_ENTER_CODE = "mWaitToEnterCode";
            //The text you typed does not match the text in the image.
            public const string TEXT_NOT_MATCH = "mTEXTNotMatch";
            //Please specify the URL of the RSS feed in webpart properties.
            public const string SPECIFY_RSS_URL = "mSPECIFYRssURL";
            //File Save Error
            public const string FILE_ERROR = "mFLIERROR";
            public const string MANIFEST_ERROR = "mMANIFESTREADERROR";
            public const string METADATA_ERROR = "mMETADATAREADERROR";
            // Enter the code shown
            public const string ENTER_CODE_SHOWN = "mENTCodeShown";
            //The code you typed does not match the code in the image
            public const string CODE_NOT_MATCH = "mCodeNotMatch";
            //Password and confirm password does not match
            public const string PWD_NCONF_PWD_NOT_MATCH = "mPWDnCONFPWDNotMatch";
            //Enter password is not valid.
            public const string INVALID_USERNAME_PWD = "mInvalid_USERNAME_PWD";
            //Password is not changed successfully.
            public const string PWD_NOT_CHANGED = "mPWDnotChanged";
            //Error occurred while getting information from Server.
            public const string ERR_TO_GET_INFO_FROM_SERVER = "mErrorInGetInfoFromServer";
            //Error is occurred in binding the current course list in the grid
            public const string ERR_IN_CURR_COURSE_BIND = "mCurrCourseBindingError";
            //Error is occurred in binding the completed course list in the grid.
            public const string ERR_IN_COMP_COURSE_BIND = "mCompCourseBindingError";
            //No any current course found
            public const string NO_CURR_COURSE_FIND = "mNoCurrCourseFind";
            //No any completed course found
            public const string NO_COMP_COURSE_FIND = "mNoCompCourseFind";
            // Register successfully but unable to send a mail to you.
            public const string REGISTERED_BUT_UN_SEND_MAIL = "mRegisteredButUnableToSendMail";
            //You are not authorised to view this content
            public const string INVALID_VIEW = "mInvalidView";
            //Invalid Description
            public const string INVALID_DESC = "mInvalidDesc";
            //Invalid Display Order
            public const string INVALID_DISP_ORDER = "mInvalidDisplayOrder";
            //Invalid Query String Value  
            public const string INVALID_QS = "mInvalidQS";
            //Access Error
            public const string NO_ACCESS = "mNoAccess";
            //This course is expired. Please contact to the Site Administrator.
            public const string COURSE_EXPIRED_CONTACT_SITE_ADMIN = "mCourseExpiredContactSiteAdmin";
            //File path is not valid. Please check the file path
            public const string INVALID_FILE_PATH = "mInvalidFilePath";
            public const string SESSION_EXPIRED_LOGIN_AGAIN = "mSessionExpiredLoginAgain";
            // Page under Development
            public const string PAGE_UNDER_DEVELOPMENT = "mPageUnderDevelopment";
            // File Size Greater than configured max size
            public const string FILE_GREATER_MAX = "mFileGreateThanMax";
            // Please enter valid To Date
            public const string VALID_TO_DATE = "mValidToDate";
            // Please enter To Date
            public const string ENTER_TO_DATE = "mEnterToDate";
            // Please enter From Date
            public const string ENTER_FROM_DATE = "mEnterFromDate";
            // Please enter #
            public const string PLEASE_ENTER = "mPleaseEnter";
            // # Delete Successfully.
            public const string DEL_SUCC = "mDel_Succ";
            // # Added Successfully.
            public const string ADD_SUCC = "mAdd_Succ";
            // # Updated Successfully.
            public const string UPDATE_SUCC = "mUpdate_Succ";
            // Are you sure to delete # ?
            public const string DEL_CONFIRM = "mDel_Confirm";
            // You do not have permission to # ?
            public const string PERMISSION = "mPermission";
            // Insufficient Rights
            public const string COMMON_INSUFFICIENT_RIGHTS = "mCommonInsufficientRights";
            //Page element text is not found in the database. Please contact to your Site Administrator. 
            public const string PAGE_ELEMENT_TEXT_NOT_FOUND = "mPageElementTextNotFound";
            public const string COMMON_ENTER_EMAIL = "mCommonEnterEmail";
            public const string COMMON_INVALID_EMAIL = "mCommonInvalidEmail";
            public const string COMMON_ENTER_SCHEDULE_ST_DATE = "mCommonEnterScheduleStDate";
            public const string COMMON_ENTER_SCHEDULE_END_DATE = "mCommonEnterScheduleEndDate";
            public const string SCH_END_DT_MST_B_GRTR_THN_SCH_ST_DT = "mSchEndDtMstBgrtrThnSchStDt";
            public const string COMMON_ENTER_TASK_TITLE = "mCommonEnterTaskTitle";
            public const string COMMON_ENTER_SCH_START_DATE = "mCommonEnterSchStartDate";
            public const string COMMON_ENTER_SCH_END_DATE = "mCommonEnterSchEndDate";
            public const string SCH_END_DT_MST_BGRTR_THN_SCH_STRT = "mSchEndDtMstBGrtrThnStart";
            //Please select not used record  
            public const string SELECT_NOT_USED_RECORD = "mCommonSelectNotUsedRecord";
            //No records deleted
            public const string NO_RECORDS_DELETED = "mNoRecordsDeleted";
            //Please enter valid numeric field # 
            public const string ENTER_VALID_NUMERIC_RANGE_VALUE = "mEnterValidNumericRangeValue";
            //1. Error is occurred in retrive the RSS url.
            public const string ERROR_IN_RSS_URL_RETRIEVAL = "mErrorInRSSURLRetrieval";
            //Please enter numeric value only. 
            public const string ENTER_NUMERIC_VALUE_ONLY = "mEnterNumericValueOnly";
            //Web Address is not valid. 
            public const string INVALID_WEB_ADDRESS = "mInvalidWebAddress";
            //Upload Failed.
            public const string FILE_UPLOAD_FAILED = "mFileUploadFailed";
            //Configure from email id 
            public const string CONFIGURE_FROM_EMAIL_ID = "mConfigureFromEmailID";
            ///Date range must be in between 1/1/1753 to 12/31/9999
            public const string DATE_OUT_OF_RANGE = "mOutOfDate";
            //Error while executing!  
            public const string EXECUTION_ERROR = "mEXECUTIONERROR";
            //Error while reading file!
            public const string XLS_READ_ERROR = "mXLSReadERROR";
            //Error while downloading!!! 
            public const string DOWNLOAD_ERROR = "mDOWNLOADERROR";
            //Please select field(s) to add. 
            public const string SELECT_FIELD_ADD = "mSELECTFIELDADD";
            //Please select field(s) to remove.
            public const string SELECT_FIELD_REMOVE = "mSELECTFIELDREMOVE";
            //Please select only one field to move up.
            public const string SELECT_FIELD_UP = "mSELECTFIELDUP";
            //Please select only one field to move down.
            public const string SELECT_FIELD_DOWN = "mSELECTFIELDDOWN";
            //Please select field to move up.
            public const string SELECT_FIELD_TO_UP = "mSELECTFIELDTOUP";
            //Please select field to move down.
            public const string SELECT_FIELD_TO_DOWN = "mSELECTFIELDTODOWN";
            //Please select other field to move up.
            public const string SELECT_FIELD_OTHER_UP = "mSELECTFIELDOTHERUP";
            //Please select other field to move down.
            public const string SELECT_FIELD_OTHER_DOWN = "mSELECTFIELDOTHERDOWN";
            //Classification Name Not Found 
            public const string CLASSIFICATION_NAME_NOT_FOUND = "mClassificationNameNotFound";
            //Selected Client is not mapped with LU Client 
            public const string CLIENT_NOT_MAPPED_WITH_LU = "mClientNotMappedWithLU";
            //Could not found LU web service url 
            public const string LU_WEB_SERVICE_URL_NOT_FOUND = "mLUWebServiceUrlNotFound";
            //Pdf Control Expired
            public const string PDF_CONTROL_EXPIRED_MSG = "mPDFControlExpiredMsg";
            // Please Enter Valid Data
            public const string PLZ_ENTER_VALID_DATA = "mCommonPlzEnterValidData";

            public const string USER_PASSWORD_SEND_SUCCESS = "mUSER_PASSWORD_SEND_SUCCESS";
            public const string USER_PASSWORD_NOT_SEND_SUCCESS = "mUSER_PASSWORD_NOT_SEND_SUCCESS";

            public const string ENTER_INTEGER_VALUE = "mENTER_INTEGER_VALUE";

            //Please select only one field to move down.
            public const string SELECT_AT_LEAST_ONE_RECORD = "mSELECT_AT_LEAST_ONE_RECORD";

            //File uploaded successfully.
            public const string FILE_UPLOAD_SUCCESSFULLY = "mFILE_UPLOAD_SUCCESSFULLY";

            //File is not valid. Please upload valid file
            public const string INVALID_FILE = "mInvalidFile";

            public const string START_DATE_NOT_GREATERTHAN_TODAY = "mSTART_DATE_NOT_GREATERTHAN_TODAY";
            public const string END_DATE_NOT_GREATERTHAN_TODAY = "mEND_DATE_NOT_GREATERTHAN_TODAY";
            public const string END_DATE_NOT_GREATERTHAN_STARTDATE = "mEND_DATE_NOT_GREATERTHAN_STARTDATE";

            //select valid Date
            public const string SELECT_VALID_DATE = "mSELECT_VALID_DATE";
            //Please value in percentage. 
            public const string ENTER_VALUE_IN_PERCENTAGE = "mENTER_VALUE_IN_PERCENTAGE";
            public const string FILE_UPLOAD = "mFileUpload";

            //Pdf Control Expired
            public const string PDF_GENERATION_FAILED = "mPDF_GENERATION_FAILED";

            // Please enter valid To Birth Date
            public const string VALID_BIRTH_DATE = "mValidBirthDate";

            // Please enter valid To Birth Date
            public const string VALID_START_DUE_EXPIRE_DATE = "mVALID_START_DUE_EXPIRE_DATE";

            public const string PWD_CHANGED_SUCCESS = "mPWD_CHANGED_SUCCESS";
            public const string PWD_NOT_CHANGED_SUCCESS = "mPWD_NOT_CHANGED_SUCCESS";

            // Please enter valid Date
            public const string INVALID_DATE = "mINVALID_DATE";

            // Minimum password length 
            public const string MIN_PASSWORD_LEN = "mMIN_PASSWORD_LEN";

            // Word Verification Failed
            public const string WORD_VERIFICATION_FAILED = "mWORD_VERIFICATION_FAILED";

            // ENTER_VALID_TEST_SCORE
            public const string ENTER_VALID_TEST_SCORE = "mENTER_VALID_TEST_SCORE";

            // ENTER_VALID_TEST_SCORE
            public const string TEST_SCORE_RECORED_SAVED_SUCCESSFULLY = "mTEST_SCORE_RECORED_SAVED_SUCCESSFULLY";
        }


        public class Student
        {
            public const string SYS_MSG_STUDENT = "mSTUDENT_ERROR";
        }

        public class StudentRegistration
        {
            //Error occured in Student registration.
            public const string SYS_MSG_STUDENTREGISTRATION = "mSTUDENT_REGISTRATION_ERROR";
            //Select Apply for field.
            public const string SELECT_APPLYFOR = "mSELECT_APPLYFOR";
            //Select Degree field.
            public const string SELECT_DEGREE = "mSELECT_DEGREE";
            //Select Semester apply field.
            public const string SELECT_SEMESTER_APPLY = "mSELECT_SEMESTER_APPLY";
            //enter Firstname.
            public const string ENTER_STUD_FIRSTNAME = "mENTER_STUD_FIRSTNAME";
            //enter secondname.
            public const string ENTER_STUD_SECONDNAME = "mENTER_STUD_SECONDNAME";
            //enter grandname.
            public const string ENTER_STUD_GRANDNAME = "mENTER_STUD_GRANDNAME";
            //enter surname.
            public const string ENTER_STUD_SURNAME = "mENTER_STUD_SURNAME";
            //enter firstname in arabic.
            public const string ENTER_STUD_ARFIRSTNAME = "mENTER_STUD_ARFIRSTNAME";
            //enter secondname in arabic.
            public const string ENTER_STUD_ARSECONDNAME = "mENTER_STUD_ARSECONDNAME";
            //enter grand in arabic.
            public const string ENTER_STUD_ARGRANDNAME = "mENTER_STUD_ARGRANDNAME";
            //enter surname in arabic.
            public const string ENTER_STUD_ARSURNAME = "mENTER_STUD_ARSURNAME";
            //select nationality.
            public const string SELECT_STUD_NATIONALITY = "mSELECT_STUD_NATIONALITY";
            //select religion.
            public const string SELECT_STUD_RELIGION = "mSELECT_STUD_RELIGION";
            //select birth date.
            public const string SELECT_STUD_BIRTHDATE = "mSELECT_STUD_BIRTHDATE";
            //select identification.
            public const string SELECT_STUD_IDENTIFICATION = "mSELECT_STUD_IDENTIFICATION";
            //enter national Id.
            public const string ENTER_STUD_NATIONAID = "mENTER_STUD_NATIONAID";
            //enter Family Id.
            public const string ENTER_STUD_FAMILYID = "mENTER_STUD_FAMILYID";
            //select gender.
            public const string SELECT_STUD_GENDER = "mSELECT_STUD_GENDER";
            //select marital status.
            public const string SELECT_STUD_MARITALSTATUS = "mSELECT_STUD_MARITALSTATUS";
            //select Country.
            public const string SELECT_STUD_COUNTRY = "mSELECT_STUD_COUNTRY";
            //select City.
            public const string SELECT_STUD_CITY = "mSELECT_STUD_CITY";
            //Enter City.
            public const string ENTER_STUD_CITY = "mENTER_STUD_CITY";
            //Enter STATE.
            public const string ENTER_STUD_STATE = "mENTER_STUD_STATE";
            //Select STATE.
            public const string SELECT_STUD_STATE = "mSELECT_STUD_STATE";

            //enter MobileNo.
            public const string ENTER_STUD_MOBILENO = "mENTER_STUD_MOBILENO";
            //enter HomeNo.
            public const string ENTER_STUD_HOMENO = "mENTER_STUD_HOMENO";
            //Please enter valid parent home No.
            public const string INVALID_HOME_NO = "mINVALID_PARENT_HOMENO";
            //enter email.
            public const string ENTER_STUD_EMAIL = "mENTER_STUD_EMAIL";
            //enter parent Mobile No.
            public const string ENTER_STUDPARENT_MOBILENO = "mENTER_STUDPARENT_MOBILENO";
            //Please enter parent Home No.
            public const string ENTER_STUDPARENT_HOMENO = "mENTER_STUDPARENT_HOMENO";

            //enter Mobile country code.
            public const string ENTER_STUD_MOBILE_COUNTRYCODE = "mENTER_STUD_MOBILE_COUNTRYCODE";
            //Invalid Email Id.
            public const string INVALID_EMAIL = "mINVALID_EMAIL";
            //Invalid Parent Email Id.
            public const string INVALID_PARENT_EMAIL = "mINVALID_PARENT_EMAIL";
            //Invalid mobile country code.
            public const string INVALID_STUD_MOBILE_COUNTRYCODE = "mINVALID_STUD_MCOUNTRYCODE";
            //Invalid mobile no.
            public const string INVALID_STUD_MOBILE = "mINVALID_STUD_MOBILE ";
            //Invalid sponsor mobile country code.
            public const string INVALID_SPONSOR_MOBILE_COUNTRYCODE = "mINVALID_SPONSOR_MCOUNTRYCODE";
            //Invalid sponsor mobile.
            public const string INVALID_SPONSOR_MOBILE = "mINVALID_SPONSOR_MOBILE";
            //Invalid Parent mobile country code.
            public const string INVALID_PARENT_MOBILE_COUNTRYCODE = "mINVALID_PARENT_MCOUNTRYCODE";
            //Invalid Parent mobile.
            public const string INVALID_PARENT_MOBILE = "mINVALID_PARENT_MOBILE";
            //enter Parent firstname.
            public const string ENTER_STUD_PARENT_FIRSTNAME = "mENTER_STUD_PARENT_FIRSTNAME";
            //enter Parent secondname.
            public const string ENTER_STUD_PARENT_SECONDNAME = "mENTER_STUD_PARENT_SECONDNAME";
            //enter Parent grandname.
            public const string ENTER_STUD_PARENT_GRANDNAME = "mENTER_STUD_PARENT_GRANDNAME";
            //enter Parent surname.
            public const string ENTER_STUD_PARENT_SURNAME = "mENTER_STUD_PARENT_SURNAME";
            //enter Class name.
            public const string ENTER_STUD_CLASSNAME = "mENTER_STUD_CLASSNAME";
            //enter High school code.
            public const string ENTER_STUD_HIGH_SCHOOLCODE = "mENTER_STUD_HIGH_SCHOOLCODE";
            //enter GPA.
            public const string ENTER_STUD_GPA = "mENTER_STUD_GPA";
            //select certificate type.
            public const string SELECT_STUD_CERTIFICATETYPE = "mSELECT_STUD_CERTIFICATETYPE";
            //Invaild test score.
            public const string INVALID_TESTSCORE = "mINVALID_TESTSCORE";
            //select datetaken month.
            public const string SELECT_STUD_DATETAKEN_MONTH = "mSELECT_STUD_DATETAKEN_MONTH";
            //select datetaken year.
            public const string SELECT_STUD_DATETAKEN_YEAR = "mSELECT_STUD_DATETAKEN_YEAR";
            //error in Add physical disability.
            public const string ERROR_STUD_ADDPHYSICALDISABILITY = "mERROR_STUD_ADDPHYSICALDISABILITY";
            //enter student application Id
            public const string ENTER_STUD_APPLICATIONID = "mENTER_STUD_APPLICATIONID";


            //enter student Password
            public const string ENTER_STUD_PASSWORD = "mENTER_STUD_PASSWORD";
            //enter student confirm password
            public const string ENTER_STUD_CONFIRMPASSWORD = "mENTER_STUD_CONFIRMPASSWORD";
            //enter student password and cofirm password not match
            public const string ENTER_STUD_NOT_MATCH_PWD = "mENTER_STUD_NOTMATCHPWD";
            //enter student Username
            public const string ENTER_STUD_USERNAME = "mENTER_STUD_USERNAME";
            //Username is available
            public const string USER_NAME_AVAIL = "mUSER_NAME_AVAIL";
            //Username is not available
            public const string USER_NAME_NOT_AVAIL = "mUSER_NAME_NOT_AVAIL";
            //Email is available
            public const string USER_EMAIL_AVAIL = "mUSER_EMAIL_AVAIL";
            //Email is not available
            public const string USER_EMAIL_NOT_AVAIL = "mUSER_EMAIL_NOT_AVAIL";
            //enter student password and cofirm password not match
            public const string DO_NOT_USE_SPECIAL_CHARS = "mDO_NOT_USE_SPECIAL_CHARS";
            //application saved.
            public const string APPLICATION_SAVED = "mAPPLICATION_SAVED";
            public const string APPLICATION_NOT_FOUND = "mAPPLICATION_NOT_FOUND";
            //enter Iqama Number.
            public const string ENTER_STUD_IQAMA_NUMBER = "mENTER_STUD_IQAMA_NUMBER";
            //enter Iqama Issue Date.
            public const string ENTER_STUD_IQAMA_ISSUE_DATE = "mENTER_STUD_IQAMA_ISSUE_DATE";
            //enter Iqama Expiry Date.
            public const string ENTER_STUD_IQAMA_ISSUE_EXPIRY_DATE = "mENTER_STUD_IQAMA_ISSUE_EXPIRY_DATE";

            //enter Sponsor Mobile CountryCode.
            public const string ENTER_STUD_SPONSOR_MCOUNTRY_CODE = "mENTER_STUD_SPONSOR_MCOUNTRY_CODE";
            //enter SponsorMobile.
            public const string ENTER_STUD_SPONSOR_MOBILE = "mENTER_STUD_SPONSOR_MOBILE";
            //enter Iqama Issue Place.
            public const string ENTER_STUD_IQAMA_ISSUE_PLACE = "mENTER_STUD_IQAMA_ISSUE_PLACE";
            //enter Sponsor Name.
            public const string ENTER_STUD_SPONSOR_NAME = "mENTER_STUD_SPONSOR_NAME";
            //enter SponsorAddress
            public const string ENTER_STUD_SPONSOR_ADDRESS = "mENTER_STUD_SPONSOR_ADDRESS";
            //enter TELEPHONE Number
            public const string ENTER_TELEPHONE_NO = "mENTER_TELEPHONE_NO";
            //Provided email id is not exits in the db. please check your email ID.
            public const string FORGOT_PWD_EMAIL_NOT_EXITS = "mFORGOT_PWD_EMAIL_NOT_EXITS";
            //Select First preference
            public const string SELECT_FIRST_PREFERENCE = "mSELECT_FIRST_PREFERENCE";
            //Select second preference
            public const string SELECT_SECOND_PREFERENCE = "mSELECT_SECOND_PREFERENCE";
            //Select third preference
            public const string SELECT_THIRD_PREFERENCE = "mSELECT_THIRD_PREFERENCE";
            //Enter the valid Date
            public const string ENTER_VALID_DATE = "mENTER_VALID_DATE";
            //Enter the valid Date
            public const string ENTER_NATION_ID_IQUMA_NUM = "mENTER_NATION_ID_IQUMA_NUM";
            //Error in Add Sponsor details
            public const string ERROR_STUD_ADDSPONSOR = "mERROR_ADD_SPONSOR";
            //Error in Add Sponsor details
            public const string APPLICATION_IS_COMPLETE = "mAPPLICATIONISCOMPLETE";
        }

        public class SISDateControl
        {
            public const string SYS_MSG_DATE = "mDATE_ERROR";
            public const string SELECT_DAY = "mSELECT_DAY";
            public const string SELECT_MONTH = "mSELECT_MONTH";
            public const string SELECT_YEAR = "mSELECT_YEAR";
        }

        public class SysMessage
        {
            public const string SYS_MSG_ERROR = "mMESSAGE_ERROR";
        }

        public class LookUp
        {
            public const string SYS_LOOKUP_ERROR = "mLOOKUP_ERROR";
        }

        public class UserPageElement
        {
            public const string SELECT_PAGE = "mSelectPage";
            public const string SELECT_PAGE_ELEMENT = "mSelectPageElement";
            public const string SELECT_PAGE_ELEMENT_TEXT = "mSelectPageElementText";
            public const string USER_PAGE_UPDATED_SUCCESS = "mUserPageUpdatedSuccess";
            public const string SELECT_UPLOAD_IMAGE = "mSelectUploadImage";
            public const string INVALID_FILE_EXTENSION = "mInvalidFileExtension";
            public const string FILE_UPLOAD_SUCCESS = "mFileUploadSuccess";
            public const string BL_ERROR = "mUserPageGetError";
        }

        public class Country
        {
            public const string SYS_MSG_COUNTRY = "mCOUNTRY_ERROR";

        }

        public class CountryProvince
        {
            public const string SYS_MSG_PROVINCE = "mPROVINCE_ERROR";
        }

        public class City
        {
            public const string SYS_MSG_CITY = "mCITY_ERROR";
        }

        public class ProgramMaster
        {
            public const string SYS_MSG_PROGRAMMASTER = "mPROGRAM_ERROR";

            //Select Degree Type
            public const string SELECT_DEGREE_TYPE = "mDEGREE_TYPE";
            //enter Program Title
            public const string ENTER_PROGRAM_TITLE = "mENTER_PROGRAM_TITLE";
            //enter Program Code.
            public const string ENTER_PROGRAM_CODE = "mENTER_PROGRAM_CODE";
            //enter Program Display Name
            public const string ENTER_PROGRAM_DISPLAY_NAME = "mENTER_PROGRAM_DISPLAY_NAME";
            //Select Category
            public const string SELECT_CATEGORY = "mSELECT_CATEGORY";
            //Select Program Duration
            public const string SELECT_PROGRAM_DURATION = "mSELECT_PROGRAM_DURATION";
            //Program Application Status is approved
            public const string NOT_DELETE_PROGRAM_STATUS = "mNOT_DELETE_PROGRAM_STATUS";
            public const string SELECT_ATLEAST_ONEPROGRAM = "mSELECT_ATLEAST_ONEPROGRAM";

            //Program Name is available
            public const string PROGRAM_NAME_AVAIL = "mPROGRAM_NAME_AVAIL";
            //Program Name  is not available
            public const string PROGRAM_NAME_NOT_AVAIL = "mPROGRAM_NAME_NOT_AVAIL";
            //enter Program Name not user spacail charater
            public const string PROGRAM_DO_NOT_USE_SPECIAL_CHARS = "mPROGRAM_DO_NOT_USE_SPECIAL_CHARS";
            //Select AFFILIATION TYPE
            public const string SELECT_AFFILIATION_TYPE = "mSELECT_AFFILIATION_TYPE";
            //enter Program Display Name
            public const string PROGRAM_STATUS_NOT_APPROVED = "mPROGRAM_STATUS_IS_NOT_APPROVED";


        }

        public class ClassRoomMaster
        {
            public const string SYS_MSG_CLASSROOMMASTER = "mCLASSROOM_ERROR";
            public const string SYS_MSG_CLASSROOMTITLE = "mCLASSROOM_TITLE";
            public const string SYS_MSG_CLASSROOMCAPACITY = "mCLASSROOM_CAPACITY";
            public const string CLASSROOM_TITLE_ALREADY_EXISTS = "mCLASSROOM_TITLE_ALREADY_EXISTS";
        }

        public class AdditionalInformationMaster
        {
            public const string SYS_MSG_ADDITIONALINFO = "mADDITIONALINFO_ERROR";
        }

        public class DegreeTypeMaster
        {
            public const string SYS_MSG_DEGREE_TYPEMASTER = "mDEGREE_TYPEMASTER";
        }

        public class SemesterMaster
        {
            public const string SYS_MSG_SEMESTER_MASTER = "mSemesterMaster";
        }

        public class ProgramYears
        {
            public const string SYS_MSG_PROGRAM_YEARS = "mProgramYears";
        }

        public class CourseGroupMaster
        {
            public const string SYS_MSG_COURSEGROUPMASTER = "mPROGRAM_ERROR";
            //Please enter Course Group Title
            public const string SYS_MSG_COURSE_GROUP_TITLE = "mCOURSE_GROUP_TITLE_ERROR";
            //Please enter Code
            public const string SYS_MSG_CODE = "mCOURSE_CODE";
            //Course Group Added Successfully
            public const string SYS_MSG_COURSE_GROUP_ADDED_SUCESS = "mCOURSE_GROUP_ADDED_SUCESS";

            //Course Group updated Successfully
            public const string SYS_MSG_COURSE_GROUP_UPDATED_SUCESS = "mCOURSE_GROUP_UPDATED_SUCESS";

            //Error Occured
            public const string SYS_MSG_COURSE_GROUP_ERROR = "mCOURSE_GROUP_ERROR";

            public const string COURSEGROUP_TITLE_ALREADY_EXISTS = "mCOURSEGROUP_TITLE_ALREADY_EXISTS";
            public const string COURSEGROUP_CODE_ALREADY_EXISTS = "mCOURSEGROUP_CODE_ALREADY_EXISTS";
        }

        public class EmailTemplate
        {
            public const string DL_ERROR = "mEMAILTEMPLATEEERROR";
            public const string SELECT_TEMPLATE = "mEMAILTEMPSELTEMPLATE";
            public const string SELECT_ALREADY_APPROVE = "mEMAILTEMPSELTEMPLATEALREADYAPPROVE";
            public const string SELECT_ALREADY_SUBMITTED_FOR_APPROVE = "mEMAILTEMPSELTEMPLATEALREADYSUBMITTEDFORAPPROVE";
            public const string SELECT_SUBMITTED_FOR_APPROVE = "mEMAILTEMPSELTEMPLATESUBMITTEDFORAPPROVE";
            public const string GET_TEMPLATE_APPROVED_BEFORE_TRANSLATION = "mEMAILTEMPGETTEMPLATEAPPROVEDBEFORETRANSLATION";
            public const string DELETED_SUCCESSFULLY = "mEMAILTEMPDELETEDSUCCESSFULLY";
            public const string APPROVED_SUCCESSFULLY = "mEMAILTEMPAPPROVEDSUCCESSFULLY";
            public const string CAN_NOT_DELETED_SUCCESSFULLY = "mEMAILTEMPCANNOTDELETEDSUCCESSFULLY";
            public const string COPIED_SUCCESSFULLY = "mEMAILTEMPCOPIEDSUCCESSFULLY";
            public const string CAN_NOT_MAKE_COPY_SELECTED_TEMPLATE = "mEMAILTEMPCANNOTMAKECOPYSELECTEDTEMPLATE";
            public const string SHARED_SUCCESSFULLY = "mEMAILTEMPSHAREDSUCCESSFULLY";
            public const string ALREADY_SHARED = "mEMAILTEMPALREADYSHARED";
            public const string DEACTIVATED_SUCCESSFULLY = "mEMAILTEMPDEACTIVATEDSUCCESSFULLY";
            public const string ALREADY_DEACTIVATED = "mEMAILTEMPALREADYDEACTIVATED";
            public const string SELECT_EXCEL_FILE = "mEMAILTEMPSELECTEXCELFILE";
            public const string IMPORTED_SUCCESSFULLY = "mEMAILTEMPIMPORTEDSUCCESSFULLY";
            public const string NOT_APPROVED = "mEMAILTEMPNOTAPPROVED";
            public const string INVALID_APPROVERS_EMAIL_ID = "mEMAILTEMPINVALIDAPPROVERSEMAILID";
            public const string PRIVILEGES_EDIT_TEMPLATE = "mEMAILTEMPPRIVILEGESEDITTEMPLATE";
            public const string INVALID_APPROVERS_EMAIL_ID_UPDATED_EMAIL = "mEMAILTEMPINVALIDAPPROVERSEMAILIDUPDATEDEMAIL";

            public const string INVLD_CNTNT_PLZ_EXPT_TOVIEW_SAMP_FL_FRMT = "mInvldCntPlzExptToViewSampFlFrmt";
            public const string INVLD_MPG_PLZ_SEL_ATLST_1_FIELD = "mInvldMpgPlzSelAtlst1Field";
            public const string EMAIL_TEMP_IMP_SUCCESS = "mEmailTempImpSuccess";
            public const string EMAIL_TEMP_TRY_DIFFRNT_NAME = "mEmailTempTryDiffrntName";

            public const string APPR_EMAIL_TEMP_AND_CONFIRM_TO_CONC_PERSON = "mApprEmailTempAndConfirmToConcPerson";
            public const string ONLY_ACTIVE_EMAIL_TEMP_CAN_B_MARK_APPR = "mOnlyActiveEmailTempCanBMarkAppr";
            public const string EMAIL_TEMP_CNT_B_DELETED_ITS_IN_USE = "mEmailTempCntBDeleteditsInUse";
            public const string DEL_EMAIL_TEMP_OTH_LANG_B4_DEL_BASE_EMAIL_TEMP = "mDelEmailTempOtherLangB4DelBaseEmailTemp";

            public const string OWNER_AND_ST_ADMIN_CAN_DEL = "mOwnerAndStAdminCanDel";
            public const string BASE_EMAIL_TMP_CNT_B_DACTVTD = "mBaseEmailTmpCntBDactvtd";
            public const string BASE_EMAIL_TMP_CNT_B_DEL = "mBaseEmailTmpCntBDel";

            //Please deactivate the email template before deleting 
            public const string DEACTIVATE_BEFORE_DELETING = "mDeactivateEmailTmpBeforeDelete";
            //Please send the email template for approval before making it as approved 
            public const string SEND_FOR_APPROVAL_BEFORE_APPROVED = "mSendForApprovalBeforeApproved";

        }

        public class AutoEmailTemplateSetting
        {
            public const string AUTO_EMAIL_TEMPLATE_ERR = "mAutoEmailTemplateError";
            //1.You do not have rights to view this page
            public const string NO_VIEW_RIGHTS = "mAutoEmailTemplateNoViewRights";
            //2. Email template settings updated successfully
            public const string UPDATED_SUCCESSFULLY = "mAutoEmailTemplateUpdatedSuccessfully";

        }

        public class ProgramCurriculumMaster
        {
            public const string SYS_MSG_CURRICULUM_PLAN = "mCURRICULUM_PLAN_ERROR";
            //Please select Pragram
            public const string SYS_MSG_PLEASE_SELECT_PROGRAM = "mSELECT_PROGRAM_ERROR";
            //Please select Academic Year
            public const string SYS_MSG_PLEASE_SELECT_ACADEMIC_YEAR = "mSELECT_ACADEMIC_YEAR_ERROR";

            //Please enter duration
            public const string SYS_MSG_PLEASE_ENTER_DURATION = "mPROGRAM_CURRICULUM_ENTER_DURATION_ERROR";

            //Please enter minimum credits
            public const string SYS_MSG_PLEASE_MINIMUM_CREDITS = "mPROGRAM_CURRICULUM_ENTER_MINIMUM_CREDITS_ERROR";

            //Please enter maximum credits
            public const string SYS_MSG_PLEASE_MAXIMUM_CREDITS = "mPROGRAM_CURRICULUM_ENTER_MAXIMUM_CREDITS_ERROR";

            //Please enter numeric value
            public const string SYS_MSG_NUMERIC_VALUE = "mPROGRAM_CURRICULUM_ENTER_NUMERIC_VALUE_ERROR";

            //Please map courses
            public const string SYS_MSG_MAP_COURSES = "mPROGRAM_CURRICULUM_MAPCOURSES_ERROR";

            //Curriculum Added Successfully
            public const string SYS_MSG_PROGRAM_CURRICULUM_ADDED_SUCESS = "mPROGRAM_CURRICULUM_ADDED_SUCESS";

            //Curriculum updated Successfully
            public const string SYS_MSG_PROGRAM_CURRICULUM_UPDATED_SUCESS = "mPROGRAM_CURRICULUM_UPDATED_SUCESS";

            //Error Occured
            public const string SYS_MSG_PROGRAM_CURRICULUM_ERROR = "mPROGRAM_CURRICULUM_ERROR";

            //Maximum credit should be greater than or equal to mininmum credit
            public const string SYS_MSG_MAXIMUM_CREDITS_GREATER_THAN_MININMUM_ERROR = "mPROGRAM_CURRICULUM_MAX_GREATER_MIN_ERROR";

            //Duration should be in between 1 to 52 range
            public const string SYS_MSG_DURATION_LIMIT_RANGE = "mPROGRAM_CURRICULUM_DURATION_LIMIT_RANGE";

            //Curriculum Status updated Successfully
            public const string SYS_MSG_PROGRAM_CURRICULUM_STATUS_UPDATED_SUCESS = "mPRO_CURR_STATUS_UPDATED_SUCESS";

            //Curriculum Status updated Successfully
            public const string SYS_MSG_PROGRAM_CURRICULUM_STATUS_NOT_UPDATED_SUCESS = "mPRO_CURR_STATUS_UPDATED_NOT_SUCESS";
        }

        public class StudentProgramRegistrationDetails
        {
            public const string SYS_MSG_STUDENT_PROGRAM_REGISTRATION_DETAILS = "mSTUDENT_PROGRAM_REGISTRATION_DETAILS_ERROR";
            //sufficient seats are not available to register 
            public const string SUFFICIENT_SEATS_NOT_AVAILABLE_TO_REGISTER = "mSUFFICIENT_SEATS_NOT_AVAILABLE_TO_REGISTER";
            public const string SUFFICIENT_SECTION_STRENGTH_NOT_AVAILABLE_TO_REGISTER = "mSUFFICIENT_SECTION_STRENGTH_NOT_AVAILABLE_TO_REGISTER";
            //Please select program to register
            public const string PLEASE_SELECT_PROGRAM_TO_REGISTER_STUDENT = "mPLEASE_SELECT_PROGRAM_TO_REGISTER_STUDENT";
            //Please select student to register
            public const string PLEASE_SELECT_STUDENT_TO_REGISTER = "mPLEASE_SELECT_STUDENT_TO_REGISTER";
            public const string PLEASE_SELECT_ADVISOR_FOR_STUDENT = "mPLEASE_SELECT_ADVISOR_FOR_STUDENT";
        }

        public class EmailMessages
        {
            public const string EMAIL_ERROR = "mEMAILMSGERROR";
            public const string EMAIL_NOT_CONFIGURED = "mEMAILNOTCONFIG";
            //Send auto email succeeded.  
            public const string AUTO_EMAIL_SENT = "mAUTOEMAILMSENT";
            //Unable to process auto email.  
            public const string AUTO_EMAIL_ERROR = "mAUTOEMAILMError";
            //Schedule email succeeded.
            public const string SCHEDULE_EMAIL_SENT = "mSCHEDULEEMAILMSENT";
            //Unable to process Schedule email.  
            public const string SCHEDULE_EMAIL_ERROR = "mSCHEDULEEMAILMError";
        }

        public class ProctorsMaster
        {
            public const string SYS_MSG_PROCTOR = "mPROCTOR_ERROR";
        }


        public class ExternalProctorsMaster
        {
            public const string SYS_MSG_EXTERNAL_PROCTOR = "mEXTERNAL_PROCTOR_ERROR";
        }


        public class ProgramSeatsMaster
        {
            public const string SYS_MSG_PROGRAM_SEATS = "mPROGRAM_SEATS_ERROR";
        }

        public class AdmissionsSchedules
        {
            public const string SYS_MSG_ADMISSIONS_SCHEDULES = "mSYS_MSG_ADMISSIONS_SCHEDULES";
        }
        public class CRNMaster
        {
            public const string SYS_MSG_CRN_MASTER = "mSYS_MSG_CRN_MASTER";
            //CRN is available
            public const string CRN_NAME_AVAIL = "mCRN_NAME_AVAIL";
            //CRN is not available
            public const string CRN_NAME_NOT_AVAIL = "mCRN_NAME_NOT_AVAIL";

            //enter student password and cofirm password not match
            public const string DO_NOT_USE_SPECIAL_CHARS = "mDO_NOT_USE_SPECIAL_CHARS";
            //enter CRN Name.
            public const string ENTER_CRN_NAME = "mENTER_CRN_NAME";

            //Select Program
            public const string SELECT_PROGRAM = "mSELECT_PROGRAM";
            //Select TERM
            public const string SELECT_TERM_SEM = "mSELECT_TERM_SEM";
            //Select TERM
            public const string SELECT_TERM_YEAR = "mSELECT_TERM_YEAR";
            //Select TERM
            public const string SELECT_SEMESTER = "mSELECT_SEMESTER";
            //Select Section
            public const string SELECT_SECTION = "mSELECT_SECTION";
            //Select Academic Year
            public const string SELECT_ACADEMIC_YEAR = "mSELECT_ACADEMIC_YEAR";
            //Select CRN
            public const string SELECT_CRN = "mSELECT_CRN";

            //Select CRN
            public const string INVALID_TOTALSEATS = "mINVALID_TOTALSEATS";
        }

        public class FormApplication
        {
            public const string SYS_MSG_TITLE = "mFORMAPPLICATION_TITLE";
            public const string FORMTITLE_ALREADYEXISTS = "mFORMTITLE_ALREADYEXISTS";

        }

        public class FormCategory
        {
            public const string SYS_MSG_TITLE = "mFORMCATEGORY_TITLE";
            public const string SYS_MSG_TITLE_ALREADYEXISTS = "mFORMCATEGORY_TITLE_ALREADYEXISTS";

        }

        public class UserPage
        {
            public const string SELECT_PAGE = "mSelectPage";
            public const string SELECT_PAGE_ELEMENT = "mSelectPageElement";
            public const string SELECT_PAGE_ELEMENT_TEXT = "mSelectPageElementText";
            public const string USER_PAGE_UPDATED_SUCCESS = "mUserPageUpdatedSuccess";
            public const string SELECT_UPLOAD_IMAGE = "mSelectUploadImage";
            public const string INVALID_FILE_EXTENSION = "mInvalidFileExtension";
            public const string FILE_UPLOAD_SUCCESS = "mFileUploadSuccess";
            public const string BL_ERROR = "mUserPageGetError";
        }

        public class AssignmentMaster
        {
            public const string SYS_MSG_ASSIGNMENTMASTER = "mASSIGNMENTMASTER_ERROR";
            //Select Degree Type
            public const string SELECT_COURSE = "mSELECT_COURSE";
            //enter Program Title
            public const string ENTER_ASSIGNMENT_TITLE = "mENTER_ASSIGNMENT_TITLE";
            //ASSIGNMENT_TITLE is available
            public const string ASSIGNMENT_TITLE_AVAIL = "mASSIGNMENT_TITLE_AVAIL";
            //ASSIGNMENT_TITLE is not available
            public const string ASSIGNMENT_TITLE_NOT_AVAIL = "mASSIGNMENT_TITLE_NOT_AVAIL";
            //enter student password and cofirm password not match
            public const string ASSIGNMENT_TITLE_DO_NOT_USE_SPECIAL_CHARS = "mASSIGNMENT_TITLE_DO_NOT_USE_SPECIAL_CHARS";
        }
        public class AssignmentAllotmentMaster
        {
            public const string SYS_MSG_AllotmentMaster = "mALLOTMENTMASTER_ERROR";
            public const string SELECT_COURSE = "mSELECT_COURSE";
            public const string SELECT_CRN = "mSELECT_CRN";
            public const string SELECT_ASSIGNMENT = "mSELECT_ASSIGNMENT";
            public const string SELECT_STRATDATE = "mSELECT_STRATDATE";
            public const string SELECT_DUEDATE = "mSELECT_DUEDATE";
            public const string SELECT_EXPIRYDATE = "mSELECT_EXPIRYDATE";

        }

        public class StudentLeaves
        {
            public const string SYS_MSG_STUDENT_LEAVE = "mSYS_MSG_STUDENT_LEAVE";
            public const string SELECT_REASON = "mSELECT_REASON";
            public const string ENTER_VALID_DAYS = "mENTER_VALID_DAYS";
            public const string ENTER_NO_OF_DAYS = "mENTER_NO_OF_DAYS";
            public const string ENTER_FROM_DATE = "mENTER_FROM_DATE";
            public const string ENTER_TO_DATE = "mENTER_TO_DATE";
            public const string ENTER_TO_REASON_DESCRIPTION = "mENTER_TO_REASON_DESCRIPTION";

        }

        public class EmployeeLeaves
        {
            public const string SYS_MSG_EMPLOYEE_LEAVE = "mSYS_MSG_EMPLOYEE_LEAVE";
            public const string SELECT_REASON = "mSELECT_REASON";
            public const string ENTER_VALID_DAYS = "mENTER_VALID_DAYS";
            public const string ENTER_NO_OF_DAYS = "mENTER_NO_OF_DAYS";
            public const string ENTER_VALID_DATE = "mENTER_VALID_DATE";
            //public const string SELECT_FROM_DATE = "mENTER_FROMDATE";
            //public const string SELECT_TO_DATE = "mENTER_TODATE";
        }
        public class FeedBack
        {
            public const string INVALID_EMAIL = "mFEEDBACK_INVALID_EMAIL";

        }
        public class StudentSponsor
        {
            public const string INVALID_TELEPHONE = "mSTUDENTSPONSOR_INVALID_TELEPHONE";

        }

        public class Employee
        {
            //enter Firstname.
            public const string ENTER_EMP_FIRSTNAME = "mENTER_EMP_FIRSTNAME";
            //enter secondname.
            public const string ENTER_EMP_SECONDNAME = "mENTER_EMP_SECONDNAME";
            //enter grandname.
            public const string ENTER_EMP_GRANDNAME = "mENTER_EMP_GRANDNAME";
            //enter surname.
            public const string ENTER_EMP_SURNAME = "mENTER_EMP_SURNAME";
            //enter MobileNo.
            public const string ENTER_EMP_MOBILENO = "mENTER_EMP_MOBILENO";
            //enter HomeNo.
            public const string ENTER_EMP_HOMENO = "mENTER_EMP_HOMENO";
            //enter email.
            public const string ENTER_EMP_EMAIL = "mENTER_EMP_EMAIL";
            //Invalid Email Id.
            public const string INVALID_EMAIL = "mINVALID_EMAIL";
            //enter student Username
            public const string ENTER_EMP_USERNAME = "mENTER_EMP_USERNAME";
            //Username is available
            public const string USER_NAME_AVAIL = "mUSER_NAME_AVAIL";
            //Username is not available
            public const string USER_NAME_NOT_AVAIL = "mUSER_NAME_NOT_AVAIL";
            public const string SELECT_SIS_ROLE = "mSELECT_SIS_ROLE";

            //enter student Username
            public const string ENTER_EMP_EMPID = "mENTER_EMP_EMPID";
            //Username is available
            public const string EMPLOYEE_ID_AVAIL = "mEMPLOYEE_ID_AVAIL";
            //Username is not available
            public const string EMPLOYEE_ID_NOT_AVAIL = "mEMPLOYEE_ID_NOT_AVAIL";
            //Username is not available
            public const string DO_NOT_DELETE_EMP = "mDO_NOT_DELETE_EMP";
            //enter Valid Telephone number
            public const string ENTER_VALID_TELEPHONE_NUM = "mENTER_VALID_TELEPHONE_NUM";

        }

        public class StudentPayments
        {
            public const string PLEASE_PROVIDE_BANK = "mPLEASE_PROVIDE_BANK";
            public const string PLEASE_FILL_ATLEAST = "mPLEASE_FILL_ATLEAST";
            public const string PAYMENT_ADDED = "mPAYMENT_ADDED";
            public const string TOTAL_VAL = "mTOTAL_VAL";
        }

        public class ExamAttendence
        {
            // Please enter Date
            public const string ENTER_DATE = "mEnterDate";
        }
        public class EmployeeEducationDetails
        {
            public const string SELECT_GRADUATION_DATE = "mSELECT_GRADUATION_DATE";
            public const string ENTER_QUALIFICATION = "mENTER_QUALIFICATION";
            public const string ENTER_GRADUATION_PLACE = "mENTER_GRADUATION_PLACE";
            public const string ENTER_SPECIALIZATION = "mENTER_SPECIALIZATION";
            public const string ENTER_SPECIALIZATION_NAME = "mENTER_SPECIALIZATION_NAME";
            public const string ENTER_SPECIALIZATION_CODE = "mENTER_SPECIALIZATION_CODE";
            public const string ENTER_SPECIFIC_SPECIALIZATION = "mENTER_SPECIFIC_SPECIALIZATION";
            public const string ENTER_GRADE = "mENTER_GRADE";
            public const string INVALID_DATE = "mINVALID_DATE";
        }

        public class UserMaster
        {
            public const string ENTER_OLD_PASSWORD = "mENTER_OLD_PASSWORD";
            public const string ENTER_NEW_PASSWORD = "mENTER_NEW_PASSWORD";
            public const string ENTER_CONFIRM_PASSWORD = "mENTER_CONFIRM_PASSWORD";
            public const string MUST_BOTH_PWD_SAME = "mMUST_BOTH_PWD_SAME";
        }


        public class TempstudentPursuingEducation
        {
            public const string ERROR_GETALL = "mTempStudentPEducationGetAll ";
            public const string ERROR_UPDATE = "mTempStudentPEducationUpdate ";
            public const string ERROR_SELECT = "mTempStudentPEducationGet ";
            public const string ERROR_DELETE = "mTempStudentPEducationDelete ";
        }
        public class TempstudentPEducationDetails
        {
            public const string ERROR_GETALL = "mTempStudentPEducationDetailsGetAll ";
            public const string ERROR_UPDATE = "mTempStudentPEducationDetailsUpdate ";
            public const string ERROR_SELECT = "mTempStudentPEducationDetailsGet ";
            public const string ERROR_DELETE = "mTempStudentPEducationDetailsDelete ";
        }
        public class StudentPenalty
        {
            // Please enter Date
            public const string ENTER_PENALTYCHARGES = "mEnterPenaltyCharges";
            public const string ENTER_PENALTYTITLE = "mEnterPenaltyTitle";
            public const string ENTER_VALID_CHARGES = "mEnterValidCharges";
        }

        public class StudentTransferRequest
        {
            public const string ENTER_COMMENT = "mEnterComment";
        }
        public class ApplicationDecisionCriterias
        {
            public const string SYS_MSG_APP_DECISION_CRITERIAS = "mSYS_MSG_APP_DECISION_CRITERIAS";
        }

        public class AdmissionDecisionCriteriasMaster
        {
            public const string SYS_MSG_ADMISSION_DECISION_CRITERIAS = "mSYS_MSG_ADMISSION_DECISION_CRITERIAS";
        }

        public class ProfileGroupMaster
        {
            public const string SYS_MSG_PROFILE_GROUP_MASTER = "mSYS_MSG_PROFILE_GROUP_MASTER";
        }

        public class AdmissionDecisionSchoolCriterias
        {
            public const string SYS_MSG_ADMISSION_DECISION_SCHOOL_CRITERIAS = "mSYS_MSG_ADMISSION_DECISION_SCHOOL_CRITERIAS";
        }

        public class AdmissionDecisionTestCriterias
        {
            public const string SYS_MSG_ADMISSION_DECISION_TEST_CRITERIAS = "mSYS_MSG_ADMISSION_DECISION_TEST_CRITERIAS";
        }

        public class AdmissionDecisionGradeCritrias
        {
            public const string SYS_MSG_ADMISSION_DECISION_GRADE_CRITERIAS = "mSYS_MSG_ADMISSION_DECISION_GRADE_CRITERIAS";
        }

        public class DocumentCheckList
        {
            public const string SYS_MSG_DOC_CHECKLIST = "mSYS_MSG_DOC_CHECKLIST";
        }
        public class CatalogueMaster
        {
            public const string SYS_MSG_CATALOGUE_MASTER = "mSYS_MSG_CATALOGUE_MASTER";
        }
        public class CatalogueCourses
        {
            public const string SYS_MSG_CATALOGUE_COURSES = "mSYS_MSG_CATALOGUE_COURSES";
        }
        public class StudentPINAllocation
        {
            public const string SYS_MSG_Student_PIN_Allocation = "mSYS_MSG_Student_PIN_Allocation";
        }
        public class ClassLevelMaster
        {
            public const string ERR_GET_RECORD = "mERROR_GET_RECORD"; // error while retrieving record for class level master
        }
        public class DHLDetails
        {
            public const string SYS_MSG_DHLDetails = "mSYS_MSG_DHLDetails";
        }
        public class Probation
        {
            public const string SYS_MSG_Probation = "mSYS_MSG_Probation";
        }
        public class BreadCrumbMaster
        {
            public const string SYS_MSG_BreadCrumbMaster = "mBREADCRUMBMASTER_ERROR";

        }
    }
}
