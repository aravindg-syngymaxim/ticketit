﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TicketIt.Controllers.landingpagecontroler;
using TicketIt.Request.AdminTasks;
using TicketIt.Services.landingpage;
using TicketIt.Utility;
//using SISCoreFinanceAPI.Services.AdminTasks;
using static TicketIt.Utility.Messages;
using static System.Net.Mime.MediaTypeNames;



namespace TicketIt.Services.landingpage
{
    public interface Ilandingpage
    {
        dynamic GetCountries();
        dynamic GetTopics();
        dynamic GetTopicscontent();
        dynamic GetAdvertisements();
        dynamic GetCountryPDFs();
        dynamic GetAdvertisementsById(string countryid);
    }

    public class landingpage : Ilandingpage
    {
        private readonly connectionStrings _constr;

        public landingpage(IOptions<connectionStrings> constr)
        {
            _constr = constr.Value;
        }



        public dynamic GetCountries()
        {
            var query = @"select * from tbl_countries";
            List<countriesrequest> countriesrequestList = new List<countriesrequest>();
            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {


                    SqlDataReader sqlDat = cmd.ExecuteReader();

                    while (sqlDat.Read())
                    {
                        countriesrequest listItems = new countriesrequest();
                        listItems.CountryId = sqlDat["countryid"].ToString();
                        listItems.CountryName = sqlDat["countryname"].ToString();
                        countriesrequestList.Add(listItems);
                    }
                    sqlDat.Close();
                    return countriesrequestList;
                }
            }
        }


        public dynamic GetTopics()
        {
            var query = @"select * from tbl_Topicsmaster";
            List<Topicrequest> TopicrequestList = new List<Topicrequest>();
            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {


                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {
                        Topicrequest listItems = new Topicrequest();
                        listItems.topicid = sqlDat["topicid"].ToString();
                        listItems.topicname = sqlDat["topicname"].ToString();
                        TopicrequestList.Add(listItems);
                    }
                    sqlDat.Close();
                    return TopicrequestList;
                }
            }
        }


        public dynamic GetTopicscontent()
        {
            var query = @"SELECT DISTINCT
    tc.Topiccontentid,
    tc.Topicheading,
    tc.Topicdescription,
    tc.countryid,
    c.CountryName AS CountryName,
    tc.topicid,
    tm.TopicName AS TopicName,  
    tc.subtopicid,
    sm.SubtopicName AS SubtopicName,
    tc.semisubtopicid,
    ssm.SemiSubtopicName AS SemiSubtopicName
FROM tbl_Topicscontent tc
LEFT JOIN tbl_countries c ON tc.countryid = c.CountryId
LEFT JOIN tbl_Topicsmaster tm ON tc.topicid = tm.TopicId
LEFT JOIN tbl_subtopicsmaster sm ON tc.subtopicid = sm.SubtopicId
LEFT JOIN tbl_SemiSubtopicsMaster ssm ON tc.semisubtopicid = ssm.SemiSubtopicId
";
            List<Topiccontntrequest> TopiccontntrequestList = new List<Topiccontntrequest>();
            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {


                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {

                        Topiccontntrequest listItems = new Topiccontntrequest();
                        listItems.Topiccontentid = sqlDat["Topiccontentid"].ToString();
                        listItems.Topicheading = sqlDat["Topicheading"].ToString();
                        listItems.Topicdescription = sqlDat["Topicdescription"].ToString();
                        listItems.countryid = sqlDat["countryid"].ToString();
                        listItems.CountryName = sqlDat["CountryName"].ToString();
                        listItems.topicid = sqlDat["topicid"].ToString();
                        listItems.TopicName = sqlDat["TopicName"].ToString();
                        listItems.subtopicid = sqlDat["subtopicid"].ToString();
                        listItems.SubtopicName = sqlDat["SubtopicName"].ToString();
                        listItems.semisubtopicid = sqlDat["semisubtopicid"].ToString();
                        listItems.SemiSubtopicName = sqlDat["SemiSubtopicName"].ToString();
                        TopiccontntrequestList.Add(listItems);
                    }
                    sqlDat.Close();
                    return TopiccontntrequestList;
                }
            }
        }




        public dynamic GetAdvertisements()
        {
            var query = @"
SELECT
                        tc.advertisementid,
                        tc.add_description,
                        tc.add_pdffilename,
                        tc.countryid,
                        tc.topicid,
                        tc.created_date  ,
                        tc.modified_date,
                        tc.start_date,
                        tc.end_date,
                        tc.subtopicid,
    
                        c.CountryName AS CountryName, 
                        tm.TopicName AS TopicName, 
                        sm.SubtopicName AS SubtopicName
                    FROM tbl_Advertisements tc
                    LEFT JOIN tbl_countries c ON tc.countryid = c.CountryId
                    LEFT JOIN tbl_Topicsmaster tm ON tc.topicid = tm.TopicId
                    LEFT JOIN tbl_subtopicsmaster sm ON tc.subtopicid = sm.SubtopicId
                    ";
            List<Avertisementrequest> AvertisementrequestList = new List<Avertisementrequest>();
            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {


                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {

                        Avertisementrequest listItems = new Avertisementrequest();

                        listItems.advertisementid = sqlDat["advertisementid"].ToString();
                        listItems.add_description = sqlDat["add_description"].ToString();
                        listItems.add_pdffilename = sqlDat["add_pdffilename"].ToString();
                        listItems.countryid = sqlDat["countryid"].ToString();
                        listItems.CountryName = sqlDat["CountryName"].ToString();
                        listItems.topicid = sqlDat["topicid"].ToString();
                         listItems.created_date = sqlDat["created_date"].ToString();
                         listItems.modified_date = sqlDat["modified_date"].ToString();

                        listItems.start_date = sqlDat["start_date"].ToString();
                        listItems.end_date = sqlDat["end_date"].ToString();
                        listItems.subtopicid = sqlDat["subtopicid"].ToString();
                        listItems.CountryName = sqlDat["CountryName"].ToString();
                        listItems.TopicName = sqlDat["TopicName"].ToString();
                        listItems.SubtopicName = sqlDat["SubtopicName"].ToString();
                        
                        AvertisementrequestList.Add(listItems);
                    }
                    sqlDat.Close();
                    return AvertisementrequestList;
                }
            }
        }


        public dynamic GetAdvertisementsById(string countryid)
        {
            var query = @"
SELECT
                        tc.advertisementid,
                        tc.add_description,
                        tc.add_pdffilename,
                        tc.countryid,
                        tc.topicid,
                        tc.created_date  ,
                        tc.modified_date,
                        tc.start_date,
                        tc.end_date,
                        tc.subtopicid,
    
                        c.CountryName AS CountryName, 
                        tm.TopicName AS TopicName, 
                        sm.SubtopicName AS SubtopicName
                    FROM tbl_Advertisements tc
                    LEFT JOIN tbl_countries c ON tc.countryid = c.CountryId
                    LEFT JOIN tbl_Topicsmaster tm ON tc.topicid = tm.TopicId
                    LEFT JOIN tbl_subtopicsmaster sm ON tc.subtopicid = sm.SubtopicId
                    where tc.countryid= @countryid;
                    ";
            List<Avertisementrequest> AvertisementrequestList = new List<Avertisementrequest>();
            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {

                    cmd.Parameters.AddWithValue("@countryid", countryid);
                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {

                        Avertisementrequest listItems = new Avertisementrequest();

                        listItems.advertisementid = sqlDat["advertisementid"].ToString();
                        listItems.add_description = sqlDat["add_description"].ToString();
                        listItems.add_pdffilename = sqlDat["add_pdffilename"].ToString();
                        listItems.countryid = sqlDat["countryid"].ToString();
                        listItems.CountryName = sqlDat["CountryName"].ToString();
                        listItems.topicid = sqlDat["topicid"].ToString();
                        listItems.created_date = sqlDat["created_date"].ToString();
                        listItems.modified_date = sqlDat["modified_date"].ToString();

                        listItems.start_date = sqlDat["start_date"].ToString();
                        listItems.end_date = sqlDat["end_date"].ToString();
                        listItems.subtopicid = sqlDat["subtopicid"].ToString();
                        listItems.CountryName = sqlDat["CountryName"].ToString();
                        listItems.TopicName = sqlDat["TopicName"].ToString();
                        listItems.SubtopicName = sqlDat["SubtopicName"].ToString();

                        AvertisementrequestList.Add(listItems);
                    }
                    sqlDat.Close();
                    return AvertisementrequestList;
                }
            }
        }


        public dynamic GetCountryPDFs()
        {
            var query = @"
        SELECT c.countryid, c.countryname, cp.advertisementid
        FROM tbl_Advertisements c
        LEFT JOIN tbl_country_pdfs cp ON c.countryid = cp.countryid";

            Dictionary<string, List<string>> countryPDFs = new Dictionary<string, List<string>>();

            using (SqlConnection con = new SqlConnection(_constr.SISConnnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    SqlDataReader sqlData = cmd.ExecuteReader();

                    while (sqlData.Read())
                    {
                        string countryId = sqlData["countryid"].ToString();
                        string countryName = sqlData["countryname"].ToString();
                        string pdfFileName = sqlData["advertisementid"].ToString();

                        if (!countryPDFs.ContainsKey(countryName))
                        {
                            countryPDFs[countryName] = new List<string>();
                        }

                        countryPDFs[countryName].Add(pdfFileName);
                    }

                    sqlData.Close();
                    return countryPDFs;
                }
            }
        }

    }

}

public class countriesrequest
{
    public string CountryId { get; set; }
    public string CountryName { get; set; }
}
public class Topicrequest
{
    public string topicid { get; set; }
    public string topicname { get; set; }
}

public class Topiccontntrequest
{

    public string Topiccontentid { get; set; }
    public string Topicheading { get; set; }
    public string Topicdescription { get; set; }
    public string countryid { get; set; }
    public string topicid { get; set; }
    public string subtopicid { get; set; }
    public string semisubtopicid { get; set; }
    public string CountryName { get; set; }
    public string TopicName { get; set; }
    public string SubtopicName { get; set; }
    public string SemiSubtopicName { get; set; }
}

public class Avertisementrequest
{
    public string advertisementid { get; set; }
    public string add_description { get; set; }
    public string add_pdffilename { get; set; }
    public string countryid { get; set; }
    public string topicid { get; set; }

    public string created_date { get; set; }
    public string modified_date { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
    public string subtopicid { get; set; }
    public string CountryName { get; set; }
    public string TopicName { get; set; }
    public string SubtopicName { get; set; }
    public string SemiSubtopicName { get; set; }
}


