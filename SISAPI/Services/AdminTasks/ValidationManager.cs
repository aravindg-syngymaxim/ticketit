﻿using System.Text.RegularExpressions;
using System;

namespace SISCoreFinanceAPI.Services.AdminTasks
{
    public class ValidationManager
    {
        /// <summary>
        /// Enum datataype for the different types of Regular Expressions
        /// </summary>
        public enum DataType
        {
            /// <summary>
            /// For Numeric
            /// </summary>
            Numeric,
            /// <summary>
            /// For AlphaNumeric
            /// </summary>
            Alphanumeric,
            /// <summary>
            /// For Alpha betical
            /// </summary>
            Alphabetical,
            /// <summary>
            /// For Email ID
            /// </summary>
            EmailID,
            /// <summary>
            /// For Web Address
            /// </summary>
            WebAddress
        }

        #region ValidateString 
        /// <summary>
        /// Validate Query String Item
        /// </summary>
        /// <param name="pID"></param>
        /// <param name="pInputDataType"></param>
        /// <returns>returns true if provided string characters are with in expected valid chars range</returns>
        public static bool ValidateString(string pID, DataType pInputDataType)
        {
            string strPattern = string.Empty;
            bool bRetvalue = false;
            if (pID != string.Empty)
            {
                switch (pInputDataType)
                {
                    case DataType.Numeric:
                        strPattern = @"^[-\+]?[0-9]+$"; //strPattern = @"^[0-9]+$";
                        break;
                    case DataType.Alphanumeric:
                        strPattern = @"^[a-zA-Z0-9 `~!@#$%^&*()_=+{}®™©–‘’“”—[|\;:""',./\?\]-]*$";
                        break;
                    case DataType.Alphabetical:
                        strPattern = @"^[a-zA-Z0-9 @\""';,.]+$";
                        break;
                    case DataType.EmailID:
                        //strPattern = @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$";
                        strPattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                        break;
                    case DataType.WebAddress:
                        strPattern = @"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$";
                        //strPattern = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";                                        
                        break;
                }
                if (Regex.IsMatch(pID, strPattern))
                    bRetvalue = true;
                else
                    bRetvalue = false;
            }
            return bRetvalue;
        }
        #endregion

        #region MaskCharForJS
        /// <summary>
        /// for Scorm player to get char 
        /// </summary>
        /// <param name="pString">string to replace</param>
        /// <returns>updated string</returns>
        public static string MaskCharForJS(string pString)
        {
            if (pString != "")
            {
                return pString.Replace("'", "\'");
            }
            else
            {
                return pString;
            }
        }
        #endregion

        #region ValidateDate
        public static bool ValidateDate(string pstrDateTime)
        {
            bool bValidateDate = true;
            try
            {
                DateTime dtDOB = Convert.ToDateTime(pstrDateTime);
                DateTime dtMin = Convert.ToDateTime("1/1/1753");
                DateTime dtMax = Convert.ToDateTime("12/31/9999");
                if (!(dtDOB <= dtMax && dtDOB >= dtMin))
                {
                    bValidateDate = false;
                }
            }
            catch (Exception dtEx)
            {
                bValidateDate = false;
            }
            return bValidateDate;
        }

        #endregion

        #region ValidateMultipleEmailIds
        /// <summary>
        /// Validate Multiple Email Ids seperated by 'pstrSeperator' also supports ABC<abc@mail.com> email string
        /// </summary>
        /// <param name="pstrEmailIds">Multiple email ids</param>
        /// <param name="pstrSeperator">seperator</param>
        /// <returns>true/false</returns>
        public static bool ValidateMultipleEmailIds(string pstrEmailIds, string pstrSeperator)
        {
            string[] arrayEmails = null;
            pstrEmailIds = pstrEmailIds.Replace(Environment.NewLine, "");
            if (pstrEmailIds.Contains(","))
            {
                arrayEmails = pstrEmailIds.Split(',');
            }
            else
            {
                arrayEmails = pstrEmailIds.Split(';');
            }
            foreach (string strAdress in arrayEmails)
            {
                if (strAdress.Contains("<") && strAdress.Contains(">"))
                {
                    string strEmail = strAdress.Replace("<", "");
                    strEmail = strEmail.Replace(">", "");
                    if (!ValidationManager.ValidateString(strEmail.Trim(), ValidationManager.DataType.EmailID))
                    {
                        return false;
                    }

                }
                else
                {
                    if (!ValidationManager.ValidateString(strAdress.Trim(), ValidationManager.DataType.EmailID))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region ValidateDate
        public static bool ValidateDate(DateTime pDateTime)
        {
            try
            {
                if (DateTime.MinValue.CompareTo(pDateTime) < 0)
                    return true;
                else
                    return false;
            }
            catch (Exception dtEx)
            {
                return false;
            }
        }
        #endregion

        #region CheckEncryptedQueryString
        /// <summary>
        /// Check for spaces or other characters
        /// </summary>
        /// <returns></returns>
        public static string CheckEncryptedQueryString(string pValue)
        {
            if (!string.IsNullOrEmpty(pValue))
            {
                pValue = pValue.Replace(" ", "+");
            }
            return pValue;
        }
        #endregion
    }
}
