﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using TicketIt.Controllers.AdminTasks;
using TicketIt.Request.AdminTasks;
using TicketIt.Utility;
using SISCoreFinanceAPI.Services.AdminTasks;
using static TicketIt.Controllers.AdminTasks.StudentInfoController;
using static TicketIt.Utility.Messages;

namespace TicketIt.Services.AdminTasks
{
        public interface IAcademicCalenderService
        {
            dynamic GenerateBillingStatement(string studentId);
            dynamic GetStudentDetails(StdudentDto stdudentDto);
            dynamic Login(LoginModel stdudentDto);
            dynamic GetAllPrograms();
            dynamic GetNafethPaymentLogs(string StudentId);
            dynamic GetPaymentsData(string StudentId);
        }
        public class AcademicCalenderService: IAcademicCalenderService
        {
            private readonly connectionStrings _constr;
            public AcademicCalenderService(IOptions<connectionStrings> constr)
            {
                _constr = constr.Value;
            }
            public dynamic GenerateBillingStatement(string studentId)
            {
                try
                {

                    List<StudentInfoResponse> eventsDta = new List<StudentInfoResponse>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Sproc_GenerateBillingStatement";
                    cmd.Parameters.AddWithValue("@StudentId", studentId);

                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {
                        StudentInfoResponse listItems = new StudentInfoResponse();
                        listItems.StudentId =sqlDat["StudentId"].ToString();
                        listItems.Name1 = sqlDat["Name1"].ToString();
                        listItems.Name2 = sqlDat["Name2"].ToString();
                        listItems.Name3 = sqlDat["Name3"].ToString();
                        listItems.LocalName1 = sqlDat["LocalName1"].ToString();
                        listItems.LocalName2 = sqlDat["LocalName2"].ToString();
                        listItems.LocalName3 = sqlDat["LocalName3"].ToString();
                        listItems.Sponser = sqlDat["Sponser"].ToString();
                        listItems.TermName = sqlDat["TermName"].ToString();
                        listItems.TotalpaymentwithVat = Convert.ToDecimal(sqlDat["TotalpaymentwithVat"]);
                        listItems.RemaingByTerm =Convert.ToDecimal(sqlDat["RemaingByTerm"]);
                        eventsDta.Add(listItems);
                    }
                    sqlDat.Close();

                    return eventsDta.ToList();

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public dynamic GetStudentDetails(StdudentDto stdudentDto)
            {
                try
                {

                    List<StudentInfoResponse> eventsDta = new List<StudentInfoResponse>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Sproc_GetStudentDetailsForPayments_V2";
                    cmd.Parameters.AddWithValue("@StudentId", stdudentDto.studentId);
                    //cmd.Parameters.AddWithValue("@ProgramId", stdudentDto.program);
                    cmd.Parameters.AddWithValue("@StudentType", stdudentDto.classLevel);
                    cmd.Parameters.AddWithValue("@Name", stdudentDto.name);


                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {
                        StudentInfoResponse listItems = new StudentInfoResponse();
                        listItems.StudentId = sqlDat["StudentId"].ToString();
                        listItems.StudentName = sqlDat["StudentName"].ToString();
                        listItems.TermName = sqlDat["TermName"].ToString();
                        listItems.ProgramTitle = sqlDat["ProgramTitle"].ToString();
                        listItems.StudentType = sqlDat["StudentType"].ToString();
                        listItems.RemitterId = sqlDat["RemitterId"].ToString();
                        listItems.VABBANNumber = sqlDat["VABBANNumber"].ToString();
                        listItems.VAIBANNumber = sqlDat["VAIBANNumber"].ToString();
                        eventsDta.Add(listItems);
                    }
                    sqlDat.Close();

                    return eventsDta.ToList();

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public dynamic GetNafethPaymentLogs(string StudentId)
            {
                try
                {
                    List<Dictionary<string, object>> eventsData = new List<Dictionary<string, object>>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select N.*, T.TermName from NafethPaymentLogs N INNER JOIN Term T ON N.TermCode=T.TermCode WHERE N.StudentId='"+StudentId+"' AND Status='InForce'";
                    //cmd.Parameters.AddWithValue("@StudentId", studentId);

                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();

                    while (sqlDat.Read())
                    {
                        Dictionary<string, object> listItem = new Dictionary<string, object>();

                        for (int i = 0; i < sqlDat.FieldCount; i++)
                        {
                            string fieldName = sqlDat.GetName(i);
                            object value = sqlDat.GetValue(i);

                            // Check if the value is a reference to '3'
                            if (value is string reference && reference.StartsWith("{$ref: '3'}"))
                            {
                                listItem[fieldName] = null; // Set the value to null
                            }
                            else
                            {
                                listItem[fieldName] = value; // Keep the original value
                            }
                        }

                        eventsData.Add(listItem);
                    }
                    sqlDat.Close();

                    return eventsData.ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            public dynamic GetPaymentsData(string StudentId)
            {
                try
                {
                    List<Dictionary<string, object>> eventsData = new List<Dictionary<string, object>>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select * from PaymentsData S Inner Join Term T on S.termId=T.TermId Where StudentId='" + StudentId + "'";
                    //cmd.Parameters.AddWithValue("@StudentId", studentId);

                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();

                    while (sqlDat.Read())
                    {
                        Dictionary<string, object> listItem = new Dictionary<string, object>();

                        for (int i = 0; i < sqlDat.FieldCount; i++)
                        {
                            string fieldName = sqlDat.GetName(i);
                            object value = sqlDat.GetValue(i);

                            // Check if the value is a reference to '3'
                            if (value is string json && json.Trim() == null)
                            {
                                listItem[fieldName] = null; // Set the value to null
                            }
                            else
                            {
                                listItem[fieldName] = value; // Keep the original value
                            }
                        }

                        eventsData.Add(listItem);
                    }
                    sqlDat.Close();

                    return eventsData.ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public dynamic GetAllPrograms()
            {
                try
                {
                    List<Dictionary<string, object>> eventsData = new List<Dictionary<string, object>>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select ProgramId,ProgramTitle from ProgramMaster where IsActive=1 Order By ProgramTitle ASC";
                    //cmd.Parameters.AddWithValue("@StudentId", studentId);

                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();

                    while (sqlDat.Read())
                    {
                        Dictionary<string, object> listItem = new Dictionary<string, object>();

                        for (int i = 0; i < sqlDat.FieldCount; i++)
                        {
                            string fieldName = sqlDat.GetName(i);
                            object value = sqlDat.GetValue(i);

                            // Check if the value is a reference to '3'
                            if (value is string json && json.Trim() == null)
                            {
                                listItem[fieldName] = null; // Set the value to null
                            }
                            else
                            {
                                listItem[fieldName] = value; // Keep the original value
                            }
                        }

                        eventsData.Add(listItem);
                    }
                    sqlDat.Close();

                    return eventsData.ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public dynamic Login (LoginModel stdudentDto)
            {
                try
                {
                    string e = EncryptionManager.Encrypt(stdudentDto.Password);
                    List<StudentInfoResponse> eventsDta = new List<StudentInfoResponse>();
                    SqlConnection con = new SqlConnection(_constr.SISConnnectionString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Sproc_VelidateUserLogin_V2";
                    cmd.Parameters.AddWithValue("@UserName", stdudentDto.UserName);
                    cmd.Parameters.AddWithValue("@Password", EncryptionManager.Encrypt(stdudentDto.Password));

                    cmd.Connection = con;
                    SqlDataReader sqlDat = cmd.ExecuteReader();
                    while (sqlDat.Read())
                    {
                        StudentInfoResponse listItems = new StudentInfoResponse();
                        listItems.StudentId = sqlDat["UserId"].ToString();
                        listItems.StudentName = sqlDat["UserName"].ToString();
                        eventsDta.Add(listItems);
                    }
                    sqlDat.Close();

                    return eventsDta.ToList();

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }


        public class LoginModel
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
        public class StudentInfoResponse
        {
            public string StudentId { get; set; } 
            public string Name1 { get; set; }
            public string VAIBANNumber { get; set; }
            public string VABBANNumber { get; set; }
            public string RemitterId { get; set; }
            public string Name2 { get; set; }
            public string Name3 { get; set; }
            public string LocalName1 { get; set; }
            public string LocalName2 { get; set; }
            public string LocalName3 { get; set; }
            public string StudentName { get; set; }

            public string TermName { get; set; }
            public string ProgramTitle { get; set; }
            public string StudentType { get; set; }
            public decimal RemaingByTerm { get; set; }
            public decimal TotalpaymentwithVat { get; set; }

            public string Sponser { get; set; }
        }
    }
    
