﻿using System;

namespace TicketIt.Models
{
    public class AcademicCalenderModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPublicHoliday { get; set; }
        public string LanguageId { get; set; }
        public DateTime EventDate { get; set; }
        public string ReminderUnit { get; set; }
        public int ReminderUnitLength { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
        public string EventType { get; set; }
        public bool IsReminderRequired { get; set; }
        public string LastModifiedById { get; set; }
    }
}
