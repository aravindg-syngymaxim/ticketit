﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Models
{
    public class CourseTopic
    {
        public string TopicId { get; set; }
        public string CourseId { get; set; }
        public string TopicTitle { get; set; }
        public string TopicDescription { get; set; }
        public string LastModifiedById { get; set; }
    }
}
