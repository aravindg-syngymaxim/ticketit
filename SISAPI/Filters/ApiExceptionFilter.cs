﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using TicketIt.Exceptions;
using System.Net;

namespace TicketIt.Filters
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {

            ApiResponse apiError = null;

            var mediaException = context.Exception as GeneralException;

            if (mediaException != null)
            {
                context.Exception = null;
                apiError = new ApiResponse((int)mediaException.StatusCode, mediaException.Message);

                if (mediaException is AlreadyExistsException)
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Conflict;
                }
                else if (mediaException is UnauthorizedException)
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
                else if (mediaException is NotFoundException)
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }
                else if (mediaException is BadRequestException)
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
                else
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                var msg = context.Exception.GetBaseException().Message;

                apiError = new ApiResponse((int)HttpStatusCode.InternalServerError, "Maybe server or network is down!. Please try later!.");
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            context.Result = new JsonResult(apiError);

            base.OnException(context);
        }
    }

    public class ApiResponse
    {
        [JsonProperty("issuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("statuscode")]
        public int StatusCode { get; set; }

        [JsonProperty("statusmessage")]
        public string StatusMessage { get; set; }

        public ApiResponse(int statusCode, string message)
        {
            StatusMessage = message;
            StatusCode = statusCode;
        }
    }
}
