using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TicketIt.Filters;
using TicketIt.Services;
using TicketIt.Services.AdminTasks;

using TicketIt.Utility;
using System.Text.Json.Serialization;

using System;
using TicketIt.Services.landingpage;


namespace TicketIt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddHealthChecks();
            services.AddMemoryCache();
            var conectionStringsSection = Configuration.GetSection("connectionStrings");
            services.Configure<connectionStrings>(conectionStringsSection);
            
            services.AddScoped<IAcademicCalenderService, AcademicCalenderService>();
            services.AddScoped<Ilandingpage, landingpage>();

            services.AddSingleton<ApiExceptionFilter>();
            
            services.AddControllers().AddJsonOptions(x =>
               x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);
            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "SISAPI";

            });            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddFile("Logs/SISAPI-{Date}.txt");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStatusCodePagesWithReExecute("/error/{0}");
            app.UseExceptionHandler("/error");

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseHealthChecks("/health");
            app.UseOpenApi();

            app.UseSwaggerUi3();


            app.UseStaticFiles();
            app.UseCors(builder =>
              builder
                  .AllowAnyOrigin()
                  .AllowAnyHeader()
                  .AllowAnyMethod());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/");
                endpoints.MapControllers();
            });
        }
    }
}
