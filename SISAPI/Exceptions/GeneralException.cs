﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketIt.Exceptions
{
    public class GeneralException:Exception
    {
        public int StatusCode { get; set; }
        public GeneralException(string message, int statusCode = 500) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
