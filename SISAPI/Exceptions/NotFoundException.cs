﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TicketIt.Exceptions
{
    public class NotFoundException:GeneralException
    {
        public NotFoundException(string message, int statusCode = (int)HttpStatusCode.NotFound) : base(message, statusCode)
        {
        }
    }
}
