﻿using System.Net;

namespace TicketIt.Exceptions
{
    public class BadRequestException : GeneralException
    {
        public BadRequestException(string message, int statusCode = (int)HttpStatusCode.BadRequest) : base(message, statusCode)
        {
        }
    }
}
