﻿using System.Net;

namespace TicketIt.Exceptions
{
    public class UnauthorizedException : GeneralException
    {
        public UnauthorizedException(string message, int statusCode = (int)HttpStatusCode.Unauthorized) : base(message, statusCode)
        {
        }
    }
}
