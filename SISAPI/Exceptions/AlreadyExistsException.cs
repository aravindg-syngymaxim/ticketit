﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TicketIt.Exceptions
{
    public class AlreadyExistsException : GeneralException
    {
        public AlreadyExistsException(string message, int statusCode = (int)HttpStatusCode.Conflict) : base(message, statusCode)
        {
        }
    }
}
